# Bash

## Terms

- `metacharacter`: a character when unquoted seperating words, e.g., `space`, `tab`, `newline`, `|`, `&`, `;`, `(`, `)`, `<`, `>`
- `escape character`: A unquoted backslash `\`
  - preserves the literal value of the next character that follows
  - `exception`: `\newline` => `line continuation`
- control operator: a token performing a control function
  - `newline`, `||`, `&&`, `&`, `;`, `;;`, `;&`, `;;&`, `|`, `|&`, `(`, `)`

## Quote

- Single quotes `'`
  - preserves the literal value of each character within the quotes
  - A single quote may not occur between single quotes, even when preceded by a backslash.
- Double quotes `"`
  - preserves the literal value of all characters and prevents reinterpretation of all special characters within the quoted string, with the `exception` of
    - `$`, `` ` `` (backquote) `command expansion`
    - `\` retains its special meaning only when followed by one of the following characters: `$`, `` ` ``, `"`, `\`, or `newline`
      - backslashes that are followed by one of these characters are removed
    - `!` when history expansion is enabled
  - ANSI-C Quoting
    - `$'string'`
    - The word expands to string, with `backslash-escaped characters` replaced as specified by the ANSI C standard
  - Locale-Specific Translation
    - `$"string"`
    - the string to be translated according to the current locale

```bash
List="one two three"
for a in $List     # Splits the variable in parts at whitespace.
for a in "$List"   # Preserves whitespace in a single variable.
```

## Array

### Operation

- declaration
  - **_implicitly:_**
    - `name[subscript]=value`
    - `name=(value1 value2 ...)`
  - **_explicitly:_**
    - `declare -a name`
    - `declare -a name[subscript]` // subscript is ignored
    - `declare -A name`　// declare an associative array
- assignment `name[subscript]=value`
  - if the `subscript` is less then 0, the element at index `-1 - subscript` from the last element is assigned
  - if `subscript` is not supplied, the value is **_appended_** to the end of the array
  - for associative array, the `subscript` is required
- element reference `${name[subscript]}`
  - if the `subscript` is missed, the element of the index of `0` is referred.
  - negative subscript is allowed
  - if the `subscript` is `@` or `*`, the word is expanded to **_all members of the array `name`_**
    - differ within double quote `"`
      - `${name[*]}` expands to **_a single word_** with the value of each array member separated by **_the first character_** of the `IFS` variable
      - `${name[@]}` expands each element of **_`name`_** to a separate word.
- length `${#name[*]}` and `${#name[@]}`
  - the length of the elements contained in the array
- keys `${!name[@]}` and `${!name[*]}`
  - differ within double quote `"`
- delete element `unset name[subscript]`
  - negative subscript is allowed
  - if the subscript is supplied, delete the element at index subscript
  - if the subscript is `*` or `@` or no subscript is subscript, delete the entire array

## Simple command

- A `simple command` is a sequence of optional `variable assignments` followed by `blank-separated words` and `redirections`, and terminated by a `control operator`.
  - The first word specifies the **command** to be executed, and is passed as argument zero.
  - The remaining words are passed as arguments to the invoked command.

### options

- \--

  - signals the end of options and disables further option processing.
    - Any arguments after the -- are treated as filenames and arguments.

### Expansion when excuting a command

1. The words that the parser has marked as `variable assignments` (those preceding the command name) and `redirections` are saved for later processing
2. The words that are not variable assignments or redirections undergoes `Shell Expansions`(tilde expansion, parameter expansion, command substitution, arithmetic expansion, and quote removal). If any words remain after expansion, the first word is taken to be the name of the command and the remaining words are the arguments.
3. Redirections are performed.
4. The text after the ‘=’ in each variable assignment undergoes `Shell Expansions` before being assigned to the variable.

### Execution of a command

- If no command name found, the variable assignments affect the current shell environment
- Otherwise, the variables are added to the environment of the executed command and do not affect the current shell environment.
- If any of the assignments attempts to assign a value to a readonly variable, an error occurs, and the command exits with a non-zero status.
- If no command name found, redirections are performed, but do not affect the current shell environment
- A redirection error causes the command to exit with a non-zero status.
- If there is a command name left after expansion, execution proceeds as described below.
- Otherwise, the command exits.
- If one of the expansions contained a command substitution, the exit status of the command is the exit status of the last command substitution performed.
- If there were no command substitutions, the command exits with a status of zero.

### Search and Execution

1. the command name contains no slashes
   1. search a `shell function`
   2. search a `shell builtin`
   3. search an executable file within a `$PATH` directory
   4. search a `shell function` `command_not_found_handle`
      1. if found, execute the command with `the_original_command and arguments` as arguments, and return its `exist status` as the shell's `exist status`
      2. otherwise, print an error message and return an `exit stauts` of `127`
2. if search succeedes, or the command name has slashes, the found is executed in a **_seperate execution environment_**

   - When a simple command other than a builtin or shell function is to be executed, it is
     invoked in a separate execution environment

3. If the command was not begun **_asynchronously_**, the shell waits for the command to complete and collects its exit status.

### Globbing

- `Globbing` is a process to perform `filename expansion`
  - bash does not use the standard regular expression (RE) set
  - globbing recognizes and expands `wild card characters`
    - `*`
    - `?`
    - characters lists in square brackets `[]`
    - certain other special characters, e.g., `^` for negating the sense of a match
  - limitation
    - `*` will not match filenames that start with a dot `.`
    - `?` has a different meaning in globbing than in an RE
  - bash performs `filename expansion` on `unquoted command line arguments`
  - customization
    - the command `set -f` disables globbing
    - the options `nocaseglob` and `nullglob` to `shopt` change globbing behavior

```bash
# Filenames with embedded whitespace can cause globbing to choke.
# David Wheeler shows how to avoid many such pitfalls.
IFS="$(printf '\n\t')"   # Remove space.

#  Correct glob use:
#  Always use for-loop, prefix glob, check if exists file.
for file in ./* ; do         # Use ./* ... NEVER bare *
  if [ -e "$file" ] ; then   # Check whether file exists.
     COMMAND ... "$file" ...
  fi
done

# This example taken from David Wheeler's site, with permission.
```

### special character

A `special character` has special interpretation other than its literal meaning

- asterisk \* represents a wild card character in `globbing` and `regular expression`

## Interactive Shell

### Interactive Shell is a shell started

- without **_non-option arguments_**, unless -s is specified,
- without specifying the -c option, and
  - **-c**: Read and execute commands from the first non-option argument command_string, then exit
- whose input and error output are both connected to terminals (as determined by isatty(3)),
- or one started with the -i option.
  - **-i**: force the shell to run interactively

### How to check whether or not Bash is running interactively within a startup script

- check `$-`
- check `$PS1`

```bash
case "$-" in
*i*) echo This shell is interactive ;;
*) echo This shell is not interactive ;;
esac
if [ -z "$PS1" ]; then
  echo This shell is not interactive
else
  echo This shell is interactive
fi
```

## Builtins

### dot(.) command

- `. filename [arguments]`
- `source filename [arguments]`
  - Read and execute commands from the `filename` in `the current shell context`.
    - find filename
      - If filename **_does not_** contain a slash (`/`), the `$PATH` variable is used to find filename.
      - When **_Bash is not in posix mode_**, **_the current directory is searched_** if filename is not found in `$PATH`.
    - the positional parameters
      - If any arguments are supplied, they become the positional parameters when filename is executed
      - Otherwise the positional parameters are unchanged (use the positional parameters of the current shell context)
    - the return status
      - the exit status of the last command executed, or zero if no commands are executed.
      - If filename is not found, or cannot be read, the return status is non-zero

### Set

If no options or arguments are supplied, set

- displays the **_names_** and **_values_** of **_all shell variables_** and **_functions_**, sorted according to the current locale, in a format that may be reused as input for setting or resetting the currently-set variables.
- Read-only variables cannot be reset.
- In POSIX mode, **_only shell variables_** are listed.

```bash
-a set the export attribute of a variable or function and marked for export to the environment of subsequent commands.
```

## Shell Parameters

A parameter is an entity that stores values. It can be

- variable
  - denoted by a `name`
  - has a `value` and `attributes`
  - `variable assignment statement` : `name=[value]`
    - variable is assigned `null string` if value is not given
  - All values undergo
    - `tilde expansion`,
    - `parameter and variable expansion`,
    - `command substitution`,
    - `arithmetic expansion`, and
    - `quote removal`
  - Assignment statements may also appear as arguments to the following builtin commands (declaration commands)
    - `alias`,
    - `declare`,
    - `typeset`,
    - `export`,
    - `readonly`, and
    - `local`
  - When an assignment statement is assigning a value to `a shell variable` or `array index`
    - `+=` operator appends to or add to the variable’s previous value
    - When `+=` is applied to an array variable using compound assignment
      - the variable’s value is not unset, and
      - new values are appended to the array beginning at one greater than the array’s maximum index (for indexed arrays),
      - or added as additional key-value pairs in an associative array.
    - When applied to a string-valued variable, value is expanded and appended to the variable’s value
- number
  - if a variable has its `integer attribute` set
  - value is evaluated as an `arithmetic expression` even if the `$((...)) expansion` is not used
  - `+=` operator adds value, which is evaluated as an arithmetic expression, to the variable’s current value
  - `Word splitting` is not performed, with the exception of `$@`
  - `Filename expansion` is not performed
  - attributes are assigned by the `builtin command` `declare`
- special characters
  A parameter is
- `set` if it has been assign a value
- use the `builtin command` `unset` to unset a parameter

## List of Commands

- `;` **|** `newline` sequentially executed commands
- `c1 && c2` c2 is executed iff c1 returns **0**
- `c1 || c2` c2 is executed iff c1 returns **non-0**
- `&` asynchronously executed subshell (the parent shell return **0**)

## Redirection

### Syntax

- output
  - `:>filename`: equal to `touch filename`
  - `>filename`: equal to `touch filename`
  - `&>filename`: Redirect both stdout and stderr to file "filename."
  - `M>N`: File descriptor "M" is redirect to file "N" where "M" is 1, if not explicitly set
  - `M>&N`: File descriptor "M" is redirect to another file descriptor "N" where "M" is 1, if not explicitly set
  - `exec >filename`: redirects stdout to a designated file. This sends all command output that would normally go to stdout to that file
  - `exec N > filename` affects the entire script or current shell. Redirection in the PID of the script or shell from that point on has changed
  - `N > filename` affects only the newly-forked process, not the entire script or shell
- input
  - `0< FILENAME` | `< FILENAME`: Accept input from a file
  - `0< &N` : Accept input from a file descriptor
  - `exec <filename`: redirects stdin to a file. From that point on, all stdin comes from that file, rather than its normal source (usually keyboard input)
  - `exec 6<&0` # Link file descriptor #6 with stdin and Saves stdin.
  - `exec < data-file` # stdin replaced by file "data-file"
  - `exec 0<&6 6<&-` Now restore stdin from fd #6, where it had been saved, and close fd #6 ( `6<&-`) to free it for other processes to use
- open file
  - `[j]<>filename`: Open file "filename" for reading and writing and assign file descriptor "j" to it
- close file
  - `n<&-` : Close input file descriptor n
  - `n>&-` : Close output file descriptor n

### Here Document

```bash
[n] <<[-]word
  here-Document
delimiter
```

`Here document` is a redirection which

- instructs the shell to read input from the current source until a line containing only `word` (`with no trailing blanks`) is seen.
- All of the lines read up to that point are then used as the standard input (or `file descriptor n` if n is specified) for a command.

- No `parameter expansion` and `variable expansion`, `command substitution`, `arithmetic expansion`, or `filename expansion` is performed on word.
- If any part of word is quoted, the `delimiter` is `the result of quote removal on word`, and the lines in the here-document are not expanded.
- If word is unquoted,

  - all lines of the here-document are subjected to `parameter expansion`, `command substitution`, and `arithmetic expansion`,
  - `the character sequence \newline is ignored`, and
  - `` \ must be used to quote the characters \, $, and ` ``.

- If the redirection operator is `<<-`, then `all leading tab characters are stripped from input lines and the line containing delimiter.`
  - This allows here-documents within shell scripts to be indented in a natural fashion.

```bash
# print multiple lines into a file
cat << EOF > file
contents
EOF
```

### Here strings

`[n] <<< word`

- The word undergoes `brace expansion`, `tilde expansion`, `parameter and variable expansion`, `command substitution`, arithmetic expansion, and quote removal.
- Pathname expansion and word splitting are not performed.
- The result is supplied as a single string, with a newline appended, to the command on its standard input (or file descriptor n if n is specified)

### File Descriptor

- `0` standard input
- `1` standard output
- `2` standard error

#### read lines from a file

```bash
while read line
do
  operation on line
done < file
```

## Built-in

### shift

```bash
shift [n]
```

- Shift the positional parameters to the left by n.
- The positional parameters from n+1 … $# are renamed to$1 … \$#-n.
- Parameters represented by the numbers $# to$#-n+1 are unset.
- n must be a non-negative number less than or equal to \$#.
- If n is zero or greater than \$#, the positional parameters are not changed.
- If n is not supplied, it is assumed to be 1.
- The return status is zero unless n is greater than \$# or less than zero, non-zero otherwise.

### set

- -e
  - Exit immediately if a pipeline (see Pipelines) returns a non-zero status. A pipeline may consist of
    - a single simple command (see Simple Commands),
    - a list (see Lists),
    - or a compound command (see Compound Commands)
  - The shell does not exit
    - if **_the command that fails_** is
      - part of the command list immediately following a while or until keyword,
      - part of the test in an if statement,
      - part of any command executed in a **&&** or **||** list except the command following
        - the final && or ||,
        - any command in a pipeline but the last,
    - or if the command's return status is being inverted with !.
  - If a compound command other than a subshell returns a non-zero status because a command failed while -e was being ignored, the shell does not exit.
  - A trap on ERR, if set, is executed before the shell exits.
- \--

  - If no arguments follow this option, then the positional parameters are unset.
  - Otherwise, the positional parameters are set to the arguments, even if some of them begin with a '-'.

- \-
  - Signal the end of options, cause all remaining arguments to be assigned to the positional parameters.
  - The -x and -v options are turned off.
  - If there are no arguments, the positional parameters remain unchanged.

This option applies to the shell environment and each subshell environment separately (see Command Execution Environment), and may cause subshells to exit before executing all the commands in the subshell.

If a compound command or shell function executes in **_a context where -e is being ignored_**, none of the commands executed within the compound command or function body will be affected by the -e setting, even if -e is set and a command returns a failure status. If a compound command or shell function sets -e while executing in a context where -e is ignored, that setting will not have any effect until the compound command or the command containing the function call completes.

## Expansion

### command substitution

- `$(command)` or `` `command` ``
- executing command in **a subshell environment** and replacing the command substitution with **the standard output of the command**, with **any trailing newlines deleted**
  - **Embedded newlines are not deleted**, but they may be removed during word splitting.
- The command substitution `$(cat file)` can be replaced by the equivalent but faster `$(< file)`.
- If the substitution appears within **double quotes**, `word splitting` and `filename expansion` are not performed on the results.

### Difference between [[]] and []

`[[]]` serves an enhanced version of `[]` with the following differences

1. when comparing strings, [[]] features `shell globbing`
   1. only general shell globbing is allowed, i.e., bash-specific globbing is not allowed
   2. globbing does not work if the right string is quoted
2. `word splitting` is prevented; may omit quotes around string variables
3. `expanding filenames` is prevented
4. can use && and ||
5. can use =~ for `regex pattern`

### Test

```bash
-n # not empty string, can be omitted
-z # is empty string
=  # string equality
!=  # string inequality
>, < # string comparing
=~ # match regex pattern, only in [[]]

### File test
-e # file exists // -a has same function, but is deprecated
-f # is a regular file
-d # is a directory
-s # is not zero size
```

### Special parameters

```bash
$@ # positional parameters
$* # positional parameters
$n # the nth position parameter, starting from 1.
$0 # the bash name or the function name
$? # the result of previous command
$# # the length of positional parameters
$- # the current option flags as specified upon invocation, by the set builtin command, or those set by the shell itself
```

### Difference between `$*` and `$@`

- `$*` expands to a list of words, each corresponding to a positional parameter
  - these words go through `word splitting` and `pathname expansion`
- `"$*"` expand to a word by joining every positional parameter with **_the first character of the IFS_**
  - if `IFS` is unset, join with space
  - if `IFS` is null, join with nothing
- `"$@"` expand to a list of words
- When there are no positional parameters, `"$@"` and `$@` expand to nothing (i.e., they are removed)

### Variable expansion

```bash
${#var} # the length of a variable
${!var} # return the value of the variable whose name is the value of var; ${${var}}
${!Prefix*} | ${!Prefix@} # return all the defined variable names beginning with Prefix
${var^} # uppercase the first character
${var^^} # uppercase all characters
${var,} # lowercase the first character
${var,,} # lowercase all characters
${var~} # reverse the case of the first character
${var~~} # reverse the case of all characters
${var:-default} # return default if var is unset or empty
${var-default} # return default if var is unset
${var:=assignment} # return assignment and assign to var if var is unset or empty
${var=assignment} # return assignment and assign to var if var is unset
${var:+alternate} # return empty if var is unset or empty; return alternate otherwise
${var+alternate} # return empty if var is unset; return alternate otherwise
${var:?error} # return an error message if var is unset or empty
${var?error} # return an error message if var is unset
```

**NOTE:** var can be **array[@]**

### String replace

```bash
${var/Pattern/Replace} // replace the first pattern match pattern with the replace string
${var//Pattern/Replace} // replace all pattern match with the replace string
${var/Pattern} // delete the first pattern match pattern
${var//Pattern} // delete all pattern match
```

**NOTE:**

- A `#` (hashmark) in pattern will indicate that your expression is matched against the beginning portion of the string,
- A `%` (percent-sign) in pattern will do it for the end portion.
- if var is `array[@]`, perform corresponding action on each item of the array

### String trim

```bash
${var#Pattern} // trim shortest front end
${var##Pattern} // trim longest front end
${var%Pattern} // trim shortest back end
${var%%Pattern} // trim longest back end
```

**NOTE**: if var is array[@], perform corresponding action on each item of the array

### Substring Expansion

```bash
${parameter:offset}
${parameter:offset:length}
```

- It expands to **_up to length characters_** of the value of parameter starting at the character specified by offset.
- If parameter is

  - **_'@'_**
    - the result is **length positional parameters beginning at offset**.
    - A negative offset is taken relative to one greater than the greatest positional parameter, so an offset of -1 evaluates to the last positional parameter.
    - It is an expansion error if length evaluates to a number less than zero.
  - **an indexed array subscripted by '@' or '\*'**,
    - the result is the length members of the array beginning with \${parameter[offset]}.
    - A negative offset is taken relative to one greater than the maximum index of the specified array.
    - It is an expansion error if length evaluates to a number less than zero.
  - or **_an associative array name_**
    - Substring expansion applied to an associative array produces undefined results.

- Substring indexing is zero-based unless the positional parameters are used, in which case the indexing starts at 1 by default. If offset is 0, and the positional parameters are used, \$@ is prefixed to the list.
- If offset evaluates to a number less than zero, the value is used as an offset in characters from the end of the value of parameter.
- If length is omitted, it expands to the substring of the value of parameter starting at the character specified by offset and extending to the end of the value. length and offset are **arithmetic expressions** (see Shell Arithmetic).
- If length evaluates to a number less than zero, it is interpreted as an offset in characters from the end of the value of parameter rather than a number of characters, and the expansion is the characters between offset and that result.
- Note that **a negative offset must be separated from the colon by at least one space to avoid being confused with the ':-' expansion**.

## Tools

### String process

- `tr`: translate, squeeze, delete character in a string

### Image Process

- `identify filename` get information of an image

### AWK

#### Overview

- AWK is an interpreted programming language.
- It is very powerful and specially designed for text processing.
- Its name is derived from the family names of its authors
  - Alfred Aho,
  - Peter Weinberger,
  - and Brian Kernighan.

#### Workflow

![Workflow of awk](awk_workflow.jpg)

- Read
  AWK reads a line from the input stream (file, pipe, or stdin) and stores it in memory.
- Execute
  All AWK commands are applied sequentially on the input. By default AWK execute commands on every line. We can restrict this by providing patterns.
- Repeat
  This process repeats until the file reaches its end.

#### Syntax of awk

```bash
BEGIN {awk-commands}
```

- The BEGIN block gets executed at program start-up.
- It executes only once.
- This is good place to initialize variables.
- BEGIN is an AWK keyword and hence it must be in upper-case.
- Please note that this block is optional.

```bash
/pattern/ {awk-commands}
```

- The body block applies AWK commands on every input line.
- By default, AWK executes commands on every line.
- We can restrict this by providing patterns.
- Note that there are no keywords for the Body block.

```bash
END {awk-commands}
```

- The END block executes at the end of the program.
- END is an AWK keyword and hence it must be in upper-case.
- Please note that this block is optional.

### cat

cat concatenate files and print on the standard output

```bash
-b, --number-nonblank    number nonempty output lines, overrides -n
-E, --show-ends          display $ at end of each line
-n, --number             number all output lines
-s, --squeeze-blank      suppress repeated empty output lines
-T, --show-tabs          display TAB characters as ^I
-v, --show-nonprinting   use ^ and M- notation, except for LFD and TAB
```

### find

find files in a path

```bash
find PATH OPTIONS
-name regex
-type f|c|b|d|l(symbolic link) | p (pipe) | s (socket)
-exec command '{}' \; // {} a placeholder for the find match results
                      // command is terminated with a semicolon (;)
                      // +  add next command;
```

```bash

xargs option command
-0 Input items are terminated by a null character instead of by  whitespace,
-a file read items from file rather than standard input
-d item delimiter
```

### sed

Stream EDitor. It is a simple yet powerful utility that parses the text and transforms it seamlessly

- developed during 1973–74 by Lee E. McMahon of Bell Labs

#### Workflow of sed

![workflow](sed_workflow.jpg)

- **Read**: SED reads a line from the input stream (file, pipe, or stdin) and stores it in its internal buffer called pattern buffer.

- **Execute**: All SED commands are applied sequentially on the pattern buffer.

  - By default, SED commands are applied on all lines (globally) unless line addressing is specified.

- **Display**: Send the (modified) contents to the output stream. After sending the data, the pattern buffer will be empty.

##### Notice

- **Pattern buffer** is a private, in-memory, volatile storage area used by the SED.
- By default, all SED commands are applied on the pattern buffer, hence the input file remains unchanged.
- **hold buffer** which is also private, in- memory, volatile storage area.
  - Data can be stored in a hold buffer for later retrieval.
  - At the end of each cycle, SED removes the contents of the pattern buffer but the contents of the hold buffer remains persistent between SED cycles.
  - However SED commands cannot be directly executed on hold buffer, hence SED allows data movement between the hold buffer and the pattern buffer.
  - Initially both pattern and hold buffers are empty.

```bash
sed [-n] [-e] 'command(s)' files // -n Default printing of pattern buffer
                                 // -e editing command
sed [-n] -f scriptfile files     // -f commands from a scriptfile
-i edit in file
```

### tar

```bash
tar -C to_dir -xvf Extract file
  -f use archive file
  -v verbose output
  -C change to directory
  --strip-component n: strip n leading components from file
```

### read

```bash
read -s -p "Password: " VARIABLE
```

- -s: Do not display password on screen. It causes input coming from a terminal/keyboard to not be echoed
- -p: "Password: ": Display text message
- VARIABLE: Bash variable in which your password stored

### getopt

getopt is used to break up (parse) options in command lines for easy parsing by shell procedures, and to check for legal options.

```bash
getopt [options] [--] optstring parameters
getopt [options] -o|--options optstring [options] [--] parameters
```

The parameters of getopt can be divided into two parts:

1. options which modify the way getopt will parse (options and -o|--options optstring), and
2. the parameters which are to be parsed (parameters in the SYNOPSIS).
3. This part will start at the first non-option parameter that is not an option argument, or
4. after the first occurrence of ‘--’.
   - If no ‘-o’ or ‘--options’ option is found in the first part, the first parameter of the second part is used as the short options string.

## Ubuntu

### Theme location

```bash
/usr/share/themes
```

## Q&A

### what is ^M

^M is \r (Control-M, or carriage return).

- windows uses \r\n for its line endings
- Linux uses only \n, hence you see the ^M at the end of the line
- remove ^M

```bash
sed -i -e 's/\r//' filename
```
