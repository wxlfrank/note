# Java

## Environment

1. JDK ([Java Development Kit](https://www.oracle.com/technetwork/java/javase/downloads/index.html))
   1. Edition
      1. SE (Standard Edition)
      2. EE (Enterprise Edition)
      3. ME (Micro Edition)
   2. Platform
      1. x86 (32-bit)
      2. x64 (64-bit)

### Hello World Program

```java
public class HelloWorld {
  public static void main(String[] args) {
    System.out.println("Hello, World!");
  }
}
```

- Java is case _**sensitive**_
- Everything in a Java program _**must**_ be inside a class
- Names _**must**_ begin with _**a letter**_ plus _**any combination of letters and digits**_.
  - You _**cannot**_ use a `Java reserved word`
  - `CamelCase` Convention
    - names are nouns that start with an uppercase letter.
    - If a name consists of multiple words, use an initial uppercase letter in each of the words
- The file name for the source code _**must**_ be the same as the name of `the public class`, with the extension _**.java**_

### Compile and Run

- The `javac` program is _**the Java compiler**_
  - It compiles _**a java file**_ into `bytecodes` placed in _**a java class file**_
- The `java` program launches `the Java virtual machine`.
  - It executes the bytecodes in the class file.

### Java Virtual Machine

A runtime instance of the Java Virtual Machine is to run one Java application.

- When a Java application starts, a `runtime instance` is born.
- When the application completes, the instance dies.
- If you start three Java applications at the same time, on the same computer, using the same `concrete implementation`, you will get three Java Virtual Machine instances.
  - Each Java application runs inside its own Java Virtual Machine.

A Java Virtual Machine instance starts running its solitary application by invoking the `main()` method of some initial class.

- The `main()` method must be `public`, `static`, `return void`, and accept `one parameter: a String array`.
- Any class with such a main() method can be used as the `starting point` for a Java application.
- You must in some `implementation-dependent way` give a Java Virtual Machine `the name of the initial class` that has the main() method that will start the entire application.

#### The life cycle of a runtime instance

Inside the Java Virtual Machine, threads come in two flavors: `daemon` and `non-daemon`.

- `A daemon thread` is ordinarily a thread `used by the virtual machine itself`, e.g., garbage collection.
- The application can mark any threads it creates as daemon threads.
  - The initial thread of an application--the one that begins at main()--is a non-daemon thread.
  - A Java application continues to execute (the virtual machine instance continues to live) as long as any non-daemon threads are still running.
    - When all non-daemon threads of a Java application terminate, the virtual machine instance will exit.
    - If permitted by `the security manager`, the application can also cause its own demise by invoking `the exit() method of class Runtime or System`.

#### Architecture

A Java Virtual Machine has

![JVM Architecture](images/java-jvm-architecture.png)

- `a class loader subsystem`:

  - a mechanism for
    - `Loading`: finding and importing the binrary data for a type (classes and interfaces) given fully qualified names.
    - `Linking`:
      - `Verification`: verify the correctness of imported classes
      - `Preparation`: allocate and initialize memory for class variables
      - `Resolution`: assist in the resolution of symbolic references
    - `Initialization`: invoking Java code that initializes class variables to their proper starting values.
  - two kinds of loaders
    - `The primordial class loader` is a part of the virtual machine implementation
      - knows how to load `trusted classes`, including the classes of the Java API.
      - Given a fully qualified type name, the primordial class loader must in some way attempt to locate a file with the type's simple name plus ".class".
        - The primordial loader looks in each directory, in the order the directories appear in the `CLASSPATH`, until it finds a file with the appropriate name: `the type's simple name plus ".class"`.
        - The path name of the subdirectory is built from the package name of the type, e.g., `java.lang` --> `java\lang` directory
    - `class loader objects` are part of the running Java application.
      - class loader objects are regular Java objects whose class descends from `java.lang.ClassLoader`.
  - Classes loaded by different class loaders are placed into `separate name spaces` inside the Java Virtual Machine.

```java
//a new type to be imported into the method area
protected final Class defineClass(byte data[], int offset, int length);
// name representing a fully qualified name of a type
// it is requesting the virtual machine to load the named type via its primordial class loader.
protected final Class findSystemClass(String name);
// c is a reference to a Class instance
// cause c to be linked and initialized (if it hasn't already been linked and initialized)
protected final void resolveClass(Class c);
```

- `an execution engine`: a mechanism responsible for executing the instructions contained in the methods of loaded classes.
- `runtime data areas`: the memory it needs to execute a program to store many things, e.g., `bytecodes` and other information it extracts from loaded class files, `objects the program instantiates`, `parameters to methods`, `return values`, `local variables`, and `intermediate results of computations`.
  - Some runtime data areas are shared among all of an applicationís threads and others are unique to individual threads.
  - Each instance of the Java Virtual Machine has `one method area` and `one heap`.
    - These areas are `shared by all threads running inside the virtual machine`.
    - When the virtual machine loads a class file, it parses `information about a type` from the binary data contained in the class file.
      - The fully qualified name of the type
      - The fully qualified name of the type's direct superclass (unless the type is an interface or class java.lang.Object, neither of which have a superclass)
      - Whether or not the type is a class or an interface
      - The type's modifiers ( some subset of public, abstract, final)
      - An ordered list of the fully qualified names of any direct superinterfaces
      - The constant pool for the type
        - A constant pool is `an ordered set of constants` used by the type, including literals (string, integer, and floating point constants) and symbolic references to types, fields, and methods
        - Entries in the constant pool are referenced by index
      - Field information
        - name
        - type
        - modifiers (some subset of public, private, protected, static, final, volatile, transient)
      - Method information
        - name
        - return type (or void)
        - number and types (in order) of parameters
        - modifiers (some subset of public, private, protected, static, final, synchronized, native, abstract)
        - for method not abstract or native
          - bytecodes
          - The sizes of the operand stack and local variables sections of the method's stack frame
          - An exception table
      - All class (static) variables declared in the type, except constants
        - Every type that uses a final class variable gets a copy of the constant value in its own constant pool
        - whereas non-final class variables are stored as part of the data for the type that declares them, final class variables are stored as part of the data for any type that uses them
      - A reference to class ClassLoader
        - For those types loaded via a class loader object, the virtual machine must store a reference to the class loader object that loaded the type
      - A reference to class Class
        - An instance of class java.lang.Class is created by the Java Virtual Machine for every type it loads
      - Method table
        - A method table is an array of direct references to all the instance methods that may be invoked on a class instance, including instance methods inherited from superclasses
        - A method table allows a virtual machine to quickly locate an instance method invoked on an object.
    - It places `this type information` into `the method area`.
    - Whenever a class instance or array is created in a running Java application, the memory for the new object is allocated from a single `heap`.
  - As each new thread comes into existence, it gets its own `pc register` (program counter) and `Java stack`.
    - If the thread is executing `a non-native Java method`, the value of `the pc register` indicates `the next instruction to execute`.
    - A thread's `Java stack` stores the state of non-native Java method invocations for the thread.
      - The state of a Java method invocation includes its `local variables`, `the parameters with which it was invoked`, `its return value` (if any), and `intermediate calculations`.
    - The state of `native method invocations` is stored in an implementation-dependent way in `native method stacks`, as well as possibly in `registers` or `other implementation-dependent memory areas`.
  - The `Java stack` is composed of `stack frames` (or `frames`).
    - A stack frame contains `the state of one Java method invocation`.
    - When a thread invokes a method, the Java Virtual Machine pushes a new frame onto that threadís Java stack.
    - When the method completes, the virtual machine pops and discards the frame for that method.
    - The Java Virtual Machine has `no registers to hold intermediate data values`.
      - The instruction set uses `the Java stack` for storage of intermediate data values.

![Java Runtime Data Areas exclusive to each thread](images/java-jvm-runtime-data-areas-to-thread.gif)

### Data Type in JVM

- Primitive
  - byte, short, int, long, char, float, double
  - When a compiler translates Java source code into bytecodes, it uses `ints` or `bytes` to represent `booleans`
    - false is represented by integer zero and true by any non-zero integer
    - Operations involving boolean values use ints
    - Arrays of boolean are accessed as arrays of `byte`
  - the `returnValue` type is used to implement `finally` clauses of Java programs.
- Reference
  - `class type`, `interface type`, and `array type`
  - All three types have values that are references to dynamically created objects.
    - The interface type's values are references to class instances that implement an interface
- `The basic unit of size for data values` in the Java Virtual Machine is the `word`
  - The word size must be large enough to hold a value of type byte, short, int, char, float, returnValue, or reference.
  - Two words must be large enough to hold a value of type long or double.
  - The word size is often chosen to be the size of a native pointer on the host platform.
  - It is only an internal attribute of a virtual machine implementation

## Data

Java is _**a strongly typed**_ language

### Integer

## Class

### Field

A field is automatically set to a default value

1. All data fields are initialized to their default values (0, false, or null).
2. All `field initializers` and `initialization blocks` are executed, in the order in which they occur in the class declaration.
3. If the first line of the constructor calls a second constructor, then the body of the second constructor is executed.
4. The body of the constructor is executed.

## Inner class

An inner class is a class that is defined inside another class.
It can

- access the data from the scope in which they are defined
  - including the data that is private.
- be hidden from other classes in the same package
- a nested class can be declared **private**, **public**, **protected**, or **package private**.

### Syntax

```java
public class Container{
  Type outField;
  static boolean access$0(Container); // compiler adds to enable InnerClass instance to access fields of Container instance
  public class InnerClass{ // NOT mean every Container instance has an InnerClass instance
    Type innerField;
    public void method(){
      outField; // An inner class method gets to access both its own data fields and those of the outer object creating it.
    }
    final Container this$0; // compiler adds
    public Container$InnerClass(Container);  // compiler adds
  }
}
```

NOTE:

- Any static fields declared in an inner class must be final.
  - One expects a unique instance of a static field, but there is a separate instance of the inner class for each outer object. If the field was not final, it might not be unique.

Inner classes are translated into regular class files with \$ (dollar signs) delimiting outer and inner class names, and the virtual machine has no knowledge about them.

#### Container reference

An inner class instance has a container reference to a container instance.

- The container reference is set in the constructor.
  - The compiler modifies all inner class constructors, adding a parameter for the container reference
  - When an inner class instance of constructed, the compiler passes **this** to the constructor

```java
Container.this
ContainerInstance.new InnerClass() // inside Container class
Container.new InnerClass() // Outside of Container class
```

**SYNTAX:** **_Container.this_**

### Local Inner Class

Local Inner Class is defined in method

- Local classes are never declared with an access specifier (that is, public or private).
  - Their scope is always restricted to the block in which they are declared.
- They are completely hidden from the outside world
- They can also access **final** local variables (**before Java SE 8**)
  - concurrent consideration

#### Syntax

```java
Class Container{
  void method(Type parameter){
    class LocalInnerClass{
      parameter;//access local variable of outer method
      Container$LocalInnerClass(Container, Type); // compiler adds
      final Type val$parameter; // compiler adds
      final Container this$0; // compiler adds
    }
  }
}
```

### Anonymous Inner Class

#### Syntax

```java
Class Container{
  void method(Type parameter){
    LocalInnerClass innerInstance = new LocalInnerClass(){
      parameter; //access local variable of outer method
      Container$LocalInnerClass(Container, Type); // compiler adds
      final Type val$parameter; // compiler adds
      final Container this$0; // compiler adds
    }
  };
}
```

- An anonymous inner class **cannot have constructors**
  - because the name of a constructor must be the same as the name of a class, and the class has no name.
  * In particular, whenever an inner class implements an interface, it cannot have any construction parameters.

##### Double brace initialization

The trick use anonymous inner class and **object construction block**

```java
ArrayList<String> friends = new ArrayList<>(); friends.add("Harry");
friends.add("Tony");
invite(friends);
// Alternative
invite(new ArrayList<String>() {{ add("Harry"); add("Tony"); }});


new Object(){}.getClass().getEnclosingClass() // gets class of static method
```

### Static inner Class

A static inner class is exactly like any other inner class, **except that an object of a static inner class does not have a reference to the outer class object that generated it**.

- **only inner classes can be declared static**.
- Inner classes that are declared inside an interface are automatically static and public.
- Unlike regular inner classes, **static inner classes can have static fields and methods**.
- **a static nested class is behaviorally a top-level class that has been nested in another top-level class for packaging convenience**

### run application use command

```bash
classpath=''
localpath=$(pwd)
mrp="${HOME}/.m2/repository" ## maven repository path
function lines2array {
	TEMPIFS=${IFS}
	IFS=$'\n'
	LineArray=($1)
	IFS=${TEMPIFS}
}
function addLocal {
	classpath="${classpath}${classpath:+:}${localpath}/$1"
}
function addMavenJar {
	path="${1//./\/}/${2}/${3}/${2}-${3}.jar"
	classpath="${classpath}${classpath:+:}${mrp}/$path"
}
addLocal 'target/classes'
addLocal 'target/test-classes'
addMavenJar 'org.hamcrest' 'hamcrest-core' '1.3'
addMavenJar 'junit' 'junit' '4.12'
addMavenJar 'org.eclipse.persistence' 'org.eclipse.persistence.core' '2.7.1'
cmd="java -cp ${classpath} -D jaxb.debug=true org.junit.runner.JUnitCore jsldReader.TestXMLReader"
$cmd
```

## Enum

`Enumerated Type` is actually a class

- They have exactly the instances defined
- compare with `==`
- All enumerated types are subclasses of the class Enum.
  - `toString`: returns the name of an enumerated constant
  - `valueOf`: return an enumerated constant from a string
  - `values`: return all the enumerated constants
  - `ordinal`: return the position of an enumerated constant

```java
public enum Size { SMALL, MEDIUM, LARGE, EXTRA_LARGE };
```

## Internationalization

`Def:` the process of designing an application so that it can be adapted to various languages and regions without engineering changes.

- also called `i18n`
- `localized data`
- textual elements are stored outside the source code and retrived dynamically
  - adding a new language requires no recompilation
- culturally-dependent data, `date` and `currency`

`Localization` is the process of adapting software for a specific region or language by adding locale-specific components and translating text

- also called `i10n`
- translating the user interface elements and documentation, changing display of numbers, dates, currency

1. Create the Properties Files

   - A `properties file` stores information about the characteristics of a program or environment.

2. Define the Locale
   - The `Locale` object identifies a particular language and country
3. Create a ResourceBundle
   - `ResourceBundle` objects contain locale-specific objects.
4. Fetch the Text from the ResourceBundle

## Servlet

1. ServletContext

- one context per "web application" per Java Virtual Machine. (A "`web application`" is `a collection of servlets and content` installed under a specific subset of `the server's URL namespace` such as /catalog and possibly installed via a .war file.)
- Defines a set of methods that a servlet uses to communicate with its `servlet container`
  - `getContextPath()` Return the main path associated with this context.
  - `getContext(String uripath)` Returns a ServletContext object that corresponds to a specified URL on the server (must be begin with "/").
    - This method allows servlets to gain access to the context for various parts of the server, and as needed obtain `RequestDispatcher` objects from the context.

Important differences between attributes and parameters in JSP/servlets are:

1. Parameters are `read only`, attributes are `read/write objects`.
2. Parameters are `String objects`, attributes can be `objects of any type`.
3. Parameters may come into our application from the `client request`, or may be `configured` (init) through `deployment descriptor` (web.xml) elements or `their corresponding annotations`. Attributes are objects that are attached to various scopes (binding an object to a scope: `page` (`PageContext`), `request` (`HttpServletRequest`), `session` (`HttpSession`), `application` (`ServletContext`)) and can be modified, retrieved or removed.

## Annotation

Annotations

- a form of metadata, provide data about a program that is not part of the program itself.
- have no direct effect on the operation of the code they annotate.
- usages:
  - **Information for the compiler** — Annotations can be used by the compiler to detect errors or suppress warnings.
  - **Compile-time and deployment-time processing** — Software tools can process annotation information to generate code, XML files, and so forth.
  - **Runtime processing** — Some annotations are available to be examined at runtime.
- format
  - `@annotate(named elements | unnamed elements)`
  - named elements := name = value
  - unnamed elements := value
- example

  - `@Documented` annotation indicates that whenever the specified annotation is used those elements should be documented using the Javadoc tool.
  - `@Retention` annotation specifies how the marked annotation is stored:
    - **RetentionPolicy.SOURCE** – The marked annotation is retained only in the source level and is ignored by the compiler.
    - **RetentionPolicy.CLASS** – The marked annotation is retained by the compiler at compile time, but is ignored by the Java Virtual Machine (JVM). **(DEFAULT)**
    - **RetentionPolicy.RUNTIME** – The marked annotation is retained by the JVM so it can be used by the runtime environment.
  - `@Target` annotation marks another annotation to restrict what kind of Java elements the annotation can be applied to.

    - **ElementType.ANNOTATION_TYPE** can be applied to an annotation type.
    - **ElementType.CONSTRUCTOR** can be applied to a constructor.
    - **ElementType.FIELD** can be applied to a field or property.
    - **ElementType.LOCAL_VARIABLE** can be applied to a local variable.
    - **ElementType.METHOD** can be applied to a method-level annotation.
    - **ElementType.PACKAGE** can be applied to a package declaration.
    - **ElementType.PARAMETER** can be applied to the parameters of a method.
    - **ElementType.TYPE** can be applied to any element of a class.

  - `@Inherited` annotation indicates that the annotation type can be inherited from the super class. (This is not true by default.) When the user queries the annotation type and the class has no annotation for this type, the class' superclass is queried for the annotation type. This annotation applies only to class declarations.

  - `@Repeatable` annotation, **introduced in Java SE 8**, indicates that the marked annotation can be applied more than once to the same declaration or type use.
