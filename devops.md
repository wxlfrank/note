# DevOps

<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

## Kubernetes

Applications are

- **Monolithic applications** consist of components that are all tightly coupled together and have to be developed, deployed, and managed as one entity
  - Changes to one part of the application require a redeployment of the whole application
  - To deal with increasing loads on the system
    - **vertically** scale the servers (also known as `scaling up`) by adding more CPUs, memory, and other server components,
      - expensive
      - upper limit
    - or scale the whole system **horizontally**, by setting up additional servers and running multiple copies (or replicas) of an application (`scaling out`)
      - require big changes in the application code
      - If any part of a monolithic application (**relational database**) isn’t scalable, the whole application becomes unscalable
- **microservices** which are components can be **developed**, **deployed**, **updated**, **scaled** individually
  - components runs as dependent processes
  - communicated with each other through interfaces (**HTTP** or **AMQP**)
  - scaled up on a **per-service basis**, you have the option of scaling only those services that require more resources,
  - chanlenges
    - automatic **scheduling** of those components to our servers,
    - automatic **configuration**, **supervision**, and **failure-handling**
    - hard to debug and trace execution calls, because they span multiple processes and machines --> solutions (`Zipkin`).
    - Provide a consistent environment to application development and production
  - solutions: **Kubernetes**
    - for developers: deploy their applications
    - for operation team: automatically monitoring and rescheduling those apps in the event of a hardware failure
    - The focus for **system administrators** (sysadmins) shifts from **supervising individual apps** to **mostly supervising and managing Kubernetes and the rest of the infrastructure**

Kubernetes

- abstracts away the hardware infrastructure
- exposes your whole **datacenter** (**cluster** containing thousands of **computer nodes**) as a single enormous computational resource
- allows you to deploy and run your software components without having to know about the actual servers underneath.

When deploying a multi-component application through Kubernetes, it

- selects a server for each component,
- deploys it, and
- enables it to easily find and communicate with all the other components of your application.

**Kubernetes system** is composed of

- each **cluster** has **a Kubernetes System**
- a **master node**, which hosts `the Kubernetes Control Plane` that controls and manages the whole Kubernetes system. It contains
  - **The Kubernetes API Server**, which you and the other Control Plane components communicate with
  - **The Scheduler**, which schedules your apps (assigns a worker node to each deploy- able component of your application)
  - **The Controller Manager**, which performs **cluster-level functions**, such as replicating components, keeping track of worker nodes, handling node failures
  - **etcd**, a reliable **distributed data store** that persistently stores **the cluster configuration**.
- a number of **worker node**s, where application components are deployed to. The task of **running**, **monitoring**, and **providing services** to your applications is done by
  - Docker, rkt, or another **container runtime**, which runs your containers
  - **The Kubelet**, which talks to **the API server** and manages containers on its node
  - **The Kubernetes Service Proxy (kube-proxy)**, which load-balances network traffic between application components
- **Kubernetes can be thought of as an operating system for a cluster**.

### How to run an application in Kubernetes

1. package the application up into one or more container images
   1. building Docker image is performed in **the Docker daemon**
      1. the contents of the whole directory are uploaded to the Docker daemon and the image is built there
   2. An image is composed of **multiple layers**
   3. When building an image, a new layer is created for **each individual command** in the Dockerfile.
2. push those images to **an image registry**
3. post **a description of your app** to the Kubernetes API server.
   1. the container image or images that contain your application components, which are grouped into three sets (called _**pods**_, **containers in the same pod need to run co-located and should not be isolated from each other**)
   2. how those components are related to each other
   3. which components need to be run **co-located** (**together on the same node**) and which don’t.
   4. For each component, you can also specify how many copies (or **replicas**) you want to run
   5. which of those components provide a **service** to either **internal** or **external clients** and should be **exposed through a single IP address** and made discoverable to the other components.

### How **the API server** process your application description

1. **the Scheduler** schedules the specified groups of containers onto **the available worker nodes** based on **computational resources required by each group** and **the unallocated resources on each node**
2. **The Kubelet** on those worker nodes then instructs the **Container Runtime (Docker, for example)** to pull the required container images and run the containers.
   1. each Docker container has **a hexadecimal number**, which is the **ID** of the Docker container.

### How do clients easily find containers that provide a specific service

1. you can use **environment variables** or **look up the service IP through good old DNS** to tell Kubernetes which containers provide the same service and Kubernetes will expose all of them at a single static IP address and expose that address to **all applications runnning in the cluster**.

## Practice

### login GCP

```bash
gcloud auth login
```

get common information

```bash
gcloud compute machine-types list
```

### project

```bash
gcloud config set project ${projectid}
gcloud projects list // list available projects
gcloud compute project-info describe
```

### Region

```bash
gcloud compute regions describe
```

### cluster

```bash
### create
gcloud container clusters create kubia --num-nodes 3 --machine-type f1-micro --zone europe-north1-a
### read
gcloud container clusters list
gcloud container clusters describe
gcloud container clusters get-credentials
### update
gcloud container clusters update $NAME
gcloud container clusters upgrade $NAME // Upgrade the Kubernetes version of an existing container cluster
gcloud container clusters resize --size=$SIZE $NAME

### delete
gcloud container clusters delete $NAME
```

after creation, interaction is performed by kubectl

```bash
kubectl get nodes
kubectl describe node

kubectl describe pod
```

#### pod

- A **pod** is a group of one or more tightly related **containers** that will always run together on the same worker **node** and in the same Linux namespace(s)
- each pod gets its own IP address, but this address is internal to the cluster and isn’t accessible from outside of it.
- each pod behaves like a separate independent machine
with its own **IP address** and **hostname**.
- To **make the pod accessible from the outside**, you’ll expose it through a **Service** object.

```bash
### create
kubectl run kubia --image=luksa/kubia --port=8080 --generator=run/v1
kubectl create -f yaml|json
### read
kubectl get pods -o wide --show-labels
kubectl logs pod-name // get logs
### update
kubectl delete pod $NAME -l labels
```

#### namespace

```bash
### create
kubectl create namespace $NAME
### read
kubectl get namespace
### delete
kubectl delete namespace $NAME

```

#### Service

A Kubernetes Service is a resource you create to make a **single**, **constant** point of entry to a group of pods providing the same service

- Each service has **an IP address and port** that **never change** while the service exists.

```bash
### create
kubectl expose rc kubia --type=LoadBalancer --name kubia-http
### read
kubectl get services
```

- A pod may disappear at any time
- When it happens, a missing pod is replaced with a new one by **the ReplicationController**
- This new pod gets a different IP address from the pod it’s replacing. 
- This is where services come in to solve the problem of ever-changing pod IP addresses, as well as exposing multiple pods at a single constant IP and port pair.
- When a service is created, it gets a static IP, which **never changes during the lifetime of the service**.
- Services represent a static location for a group of one or more pods that all provide the same service. 

