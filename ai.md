# AI

## agents

- **reflex agent**
  - base their actions on a direct mapping from states to actions
  - not suitable for environment where this mapping would be too large
- **goal-based agent**
  - consider future actions and the desirability of their outcomes
  - **problem-solving agent**
    - use **atomic** representations: states of the world are considered as wholes, with no internal structure visible to the problem- solving algorithms
    - general-purpose search algorithms
      - uninformed search algorithm: given no information about the `problem` other than its `definition`
      - Informed search algorithm
  - **planning agent**
    - use advanced **factored** or **structured** representations

## terms

- performance measure
- Goal formulation: set a goal based on the current situation and the agent’s performance
measure,
- Problem formulation: decides what actions
  and states to consider, given a goal.
