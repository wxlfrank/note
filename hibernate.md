<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

# Hibernate

## Mapping

### Association

#### unidirectional many-to-one association

```java
@Entity
public class Item {
    @Id
    @GeneratedValue(generator = "ID_GENERATOR")
    protected Long id;
    public Long getId() {
        return id;
    }
}
@Entity
public class Bid {
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "ITEM_ID", nullable = false)
    protected Item item;
} //
```

- `@ManyToOne`
  - `fetch=FetchType.EAGER`: means the associated Item is loaded whenever the Bid is loaded
- `@JoinColumn`
  - `name`: default `the target entity name + _ + its identifier property`

## `join on tables where condition`

combine tables first then filter with condition

1. association is not null -> can query with main entity
2. query without condition -> can query with main entity
3. otherwise -> must query seperately

## EntityManager

`EntityManger` is a persistence manager API, which provides services for

- basic CRUD (create, read, update, delete) operations,
- query execution, and
- controlling the persistence context
- created by a **shared** `EntityManagerFactory`
  - Most applications have only one shared EntityManagerFactory
  - use for a single unit of work in a single thread, and
  - **inexpensive** to create.
- the `EntityManager` doesn’t obtain a JDBC Connection from the pool until SQL statements have to be executed.
  - look up or query data
  - it flushes changes `detected` by the persistence context to the database.
- Hibernate joins the in-progress system transaction when an EntityManager is created and waits for the transaction to commit
  - Hibernate is `notified (by the JTA engine)` of the commit, it performs `dirty checking` of
    the persistence context and synchronizes with the database.
  - force dirty checking synchronization manually by calling `EntityManager#flush()` at any time during a transaction
- `each client request` will be processed with one persistence context and system transaction in a multithreaded environment.
