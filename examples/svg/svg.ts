export class Sizable {
  constructor(public width: number, public height: number) {}
}

export class SVG extends Sizable {
  public xmlns: URL = new URL('http://www.w3.org/2000/svg');
  public version = '1.1';
  public viewBox: number[] = null;
}
