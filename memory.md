# Memory allocation and Program Layout for Different Programming language

## Term Declaration

When you declare a variable of a type, enough memory is allocation locally to store data of that type.

- The allocation is local, occurring within the scope of the function
- when that function returns the memory is deallocated.

`Memory allocation` refers to the process by which the program makes "space" for the storage of data

`Stack allocation` referes to the way that programs track execution across functions is based on a stack

- The stack model (last-in-first-out) matches closely the model of function calls and returns during program execution.
  - The act of calling a function is the same as pushing the called function execution onto the top of the stack
    - the computer is actually allocating memory for the function's local variables
  - once that function completes, the results are returned popping the function off the stack.
    - the function and its allocated memory is popped off the stack
  - Each function is contained within a structure on the stack called a `stack frame`. A stack frame contains
    - all `the allocated memory` from `variable declaration`
    - a pointer to `the execution point` of `the calling function`, the so called `return pointer`.

`The global memory region` for a program is called the `heap`, which is `a fragmented data structure` where new allocation try and fit within unallocated regions.

- Whenever a program needs to `allocate memory globally` or `in a dynamic way`, that memory is allocated on the heap, which is `shared across the entire program` irrespective of function calls.

`Memory leak`: memory is allocated but not deallocated
`dangling pointer`: a pointer value once referenced allocated memory, but that memory has seen been dealocated.

### Library

A library is a file that contains compiled code and data.

- `Static Libraries` are linked into a compiled executable (or another library).
  - After the compilation, the new artifact contains the static library’s content.
- `Shared Libraries` are loaded by the executable (or other shared library) at runtime.

## [C]([https://link](https://www.usna.edu/Users/cs/aviv/classes/ic221/s16/lec/08/lec.html))

the programer is responsible for memory management

- `number_of_bytes sizeof(type_name)`
- allocation: `pointer_to_a_memory_region_on_heap malloc(number_of_bytes)`
- deallocation: `free(pointer_to_a_memory_region_on_heap)`


### Program Layout

a program as a `memory profile`.

- All information about a program, including the actual binary code and variables all are within the memory layout of a program.
- When executing, `the Operating System` will manage that `memory layout`, and `a snapshot of that memory` and `the current execution point` basically defines a program.
  - This allows the operating system to swap in and out programs as needed.

![alt](images/memory-c-memory-layout.png)

- The stack `base pointer` is the current top of the stack
  - as functions are called and returned, it will shift appropriately.
- The `break point` refers to the top of the programs data segment, which contains the heap.
  - As the heap fills up, requirement more space, the break is set to higher addresses.
- `memory mapping` is the process of loading data directly from files into memory (e.g., when you read and write files)
- the loading of `dynamic shared libraries`
  - When you make a call to a function like printf() or malloc(), the code for those functions exist in shared libraries, the standard C library, to be precise.
  - Your program must run that code, but the operating system doesn't want to have load more code into memory than needed.
  - Instead, the O.S. loads shared libraries dynamically, and when it does so, it maps the necessary code into the middle address spaces.

## Dynamic Linking and Resolution

### Java

- Each individual class file harbor `symbolic references` to one another and to the class files of the Java API.
- When you run your program, `the Java Virtual Machine` loads your program's classes and interfaces and hooks them together in a process of `dynamic linking`
- A class file keeps all its `symbolic references` in `the constant pool`
  - Each class file has a constant pool
    - the constant pool is organized as a sequence of items. 
    - Each item has `a unique index`, much like an array element. 
    - A symbolic reference is _**one kind of item**_ that may appear in the constant pool. 
    - Java Virtual Machine instructions that use a symbolic reference specify the index in the constant pool where the symbolic reference resides.
  - each class or interface loaded by the Java Virtual Machine has _**an internal version**_ of its constant pool
    - The internal constant pool is `an implementation-specific data structure` that _**maps to the constant pool in the class file**_
  - if a particular symbolic reference is to be used, it must be `resolved`.

`Resolution` is the process of finding the entity identified by the symbolic reference and replacing `the symbolic reference` with `a direct reference`

- also called `constant pool resolution`