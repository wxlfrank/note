# Devops

## Build

Building code from one form to another
- compilation of source code to native code or virtual machine bytecode, depending on **production platform**
- Linting of code
  - check the code for errors
  - generate **code quality measures** by means of **static code analysis**
- unit testing
- generate artifacts suitable for deployment


