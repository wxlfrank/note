# Git

---

## Git Config

### Config location

| Level  | Parameter         | Windows                                                                                                | Linux                                |
| ------ | ----------------- | ------------------------------------------------------------------------------------------------------ | ------------------------------------ |
| System | --system          | C:\Documents and Settings\All Users\Application Data\Git\config (Windows XP) C:\ProgramData\Git\config | /etc/gitconfig                       |
| Global | --global          | C:\Users\\\$USER\.gitconfig                                                                            | ~/.config/git/config or ~/.gitconfig |
| Local  | --local (default) | .git/config                                                                                            | .git/config                          |

### Credential system

- not to cache at all (default)
- The "cache" mode keeps credentials in memory for a certain period of time (default: 15 minutes).
- The "store" mode saves the credentials to a plain-text file on disk, and they never expire.
- "osxkeychain" mode, which caches credentials in the secure keychain that’s attached to your system account (on Mac)

#### core.autocrlf

- `true` — this converts LF endings into CRLF when you **check out code**
- `input`- convert CRLF to LF on **commit** but **not the other way around**

```bash
git config --global credential.helper ${model}
git credential fill // get a credential
git credential approve // store a credential
git credential reject// erase a credential
```

---

## Git Term

### Git file change

- Added (A)
- Copied (C)
- Deleted (D)
- Modified (M)
- Renamed (R)
- have their type (i.e. regular file, symlink, submodule, …​) changed (T)
- Unmerged (U)
- Unknown (X)
- pairing Broken (B)

---

## Git Branch

### How git store Data

**Git store data as a series of _snapshots_, not _changesets_**

A commit makes git store a commit object that

- contains a pointer to the snapshot of the content you staged
- contains the author’s name and email address, the message that you typed
- pointers to the commit or commits that directly came before this commit (its parent or parents):
  - zero parents for the initial commit,
  - one parent for a normal commit, and
  - multiple parents for a commit that results from a merge of two or more branches.

![Commit Object](images/git-commit-object.png)
![Commit Object](images/git-commit-object2.png)
![Commit Object](images/git-commit-object3.png)

**A branch in Git is simply a lightweight movable pointer (called branch pointer) to one of these commits.**

- The default branch is **"master"**
- A special pointer **"HEAD"** indicates which branch the working directory is on

### Create a branch

```bash
git branch ${branch-name} // create a new branch but not switch to it
git checkout -b ${branch-name} // create a new branch and switch to it
```

#### Show which branch the HEAD is pointing to

```bash
git branch -v (filter: --merged | --no-merged)
git log --oneline --decorate
git log --oneline --decorate --graph --all // showing where your branch pointers are                                        // show your history has diverged
```

### Switch a branch

```bash
git checkout ${branch-name}
```

The command

- moves **the HEAD pointer** back to the specified branch
- reverts the files in working directory back to the snapshot the specified branch points to
  - Switching branches in Git changes files in working directory
    - For an older branch, the working directory will be reverted to the snapshot after the lastest commit.
    - If Git cannot do it cleanly, it will not let you switch at all.
    - It’s best to have a clean working state when you switch branches ## TODO Stashing and Cleaning

### Delete a branch

```bash
git branch -d ${branch-name} // fail when the branch does not merge
git branch -D ${branch-name} // force deletion
```

---

## Git Merge

### Three-way Merge

Using the two snapshots pointed to by the branch tips and the common ancestor of the two, instead of just moving the branch pointer forward, Git creates a new snapshot that results from this three-way merge and automatically creates a new commit that points to it.

```bash
git checkout ${branch-into}
git merge ${branch-in}
```

![three way merge](images/git-three-way-merge.png)
![three way merge result](images/git-three-way-merge-result.png)

---

## Git Rebase

Rebasing take all the changes that were committed on one branch and replay them on another one.

```bash
git checkout ${branch-in}
git rebase ${branch-into}
git checkout ${branch-into}
git merge ${branch-in} // make a fast-forward merge
git rebase --onot ${real-branch-into} ${branch-into} ${branch-in}
```

Working Procedure

1. go to the common ancestor of the two branches (the one you’re on and the one you’re rebasing onto)
2. get the diff introduced by each commit of the branch you’re on and save those diffs to temporary files
3. reset the current branch to the same commit as the branch you are rebasing onto, and finally applying each change in turn.

![Git Rebase](images/git-rebase.png)

---

## Git Remote

Remote repositories are versions of your project that are hosted on the Internet or network somewhere.

### Show Remote Repositories

```bash
git remote
git remote -v // show urls of remote repositories
              // corresponding to .git/config#[remote]
```

### Add remote repositories

```bash
git remote add ${shortname} ${url}
```

### Fetch and pull from remote repositories

```bash
git fetch ${remote}
```

- **function:** pull down all the data from the remote repositories
  - **NOTE** it only downloads the data to the local repository
  - **NOTE** it **DOES NOT** automatically merge data to your working space
- **Affect:** local git has references to all the branches from remote repositories
  - **NOTE** cloning a repositories adds a remote repository under the shortname **"origin"**

```bash
git pull ${remote}
git pull ## if your current branch is set up to track a remote branch TODO
```

### Push to remote repositories

```bash
git push ${remote} ${branch}
git push ${remote} HEAD:${branch}
```

### Inspect a remote repository

```bash
git remote show ${remote}
```

### Rename and remove remote repositories

```bash
git remote rename ${old-name} ${new-name}
git remote remove ${remotename}
```

**NOTE:** it changes all the remote-tracking branch names, e.g., _pb/master-->paul/master_

### Manage Remote branches

- Remote references are references (pointers) to your remote repositories, including branches, tags

```bash
## Show a full list of remote references
git ls-remote ${remote}
git remote show ${remote}
```

- Remote-tracking branches are references to the state of remote branches.
  - They’re local references that you can’t move;
    - Git moves them for you whenever you do any network communication, to make sure they accurately represent the state of the remote repository.
  - Think of them as bookmarks, to remind you where the branches in your remote repositories were the last time you connected to them.
  - Remote-tracking branches take the form **${remote}/${branch}**

```bash
## update remote tracking branch pointers
git fetch ${remote}
## push current branch to remote repository branch
git push ${remote} ${branch}
git push ${remote} ${local-branch}:${remote-branch}
```

**NOTE:**

- when you do a fetch that brings down new remote-tracking branches, you don’t automatically have local, editable copies of them.
- you only have a remote-tracking branch pointer that you cannot modify

### define branches as tracked

**Tracking branches** are local branches that have a direct relationship to a remote branch.

- Checking out a local branch from a remote
  - a tracking branch automatically is created
  - the branch it tracks is called an **"upstream branch"**

```bash
git checkout -b ${branch} ${remote}/${branch}
git checkout --track ${remote}/${branch}
git branch -u ${remote}/${branch} // If you already have a local branch and
            // want to set it to a remote branch you just pulled down,
            // or want to change the upstream branch you’re tracking
            //@{upstream} or @{u} reference its upstream branch
git branch -vv //see what tracking branches you have set up
```

## Reset

### The Three Trees

**Note:**`tree collection of files, not specifically the data structure`

| Tree    | Role                              |
| ------- | --------------------------------- |
| HEAD    | Last commit snapshot, next parent |
| Index   | Proposed next commit snapshot     |
| Sandbox | Working Directory                 |

```bash
# two commands inspect head
git cat-file -p HEAD
git ls-tree -r HEAD
# one command inspect index
git ls-files -s
```

![Three Tree Operation](images/git-three-tree.png)

### Git Reset

1. Move HEAD (`git reset --soft HEAD~`)
2. Update the Index (`git reset [--mixed] HEAD~`)
3. Update the working directory (`git reset --hard HEAD~`)

![Reset Three steps](images/git-reset-three-step.png)

### Git Reset a file

![Reset a file](images/git-reset-a-file.png)

### Squash with Git Reset

1. move the HEAD branch back to an older commit (`git reset --soft HEAD~2`)
2. commit (`git commit`)

### Difference with Git Checkout

![Difference with Checkout](images/git-difference-with-checkout.png)

| Column A                                    | HEAD | Index | Workdir | WD Safe? |
| ------------------------------------------- | ---- | ----- | ------- | -------- |
| Commit Level                                |
| `reset --soft [commit]`                     | REF  | NO    | NO      | YES      |
| `reset [commit]`                            | REF  | NO    | YES     | YES      |
| `reset --hard [commit]` `checkout <commit>` | REF  | YES   | YES     | NO       |
| File Level                                  |
| `reset [commit] <paths>`                    | NO   | YES   | NO      | YES      |
| `checkout [commit] <paths>`                 | NO   | YES   | YES     | NO       |

---

## Git Internals

> Git is a content-addressable filesystem
>
> - the core of Git is a simple key-value data store
> - you can insert any kind of content into a Git repository, for which Git will hand you back a unique key you can use later to retrieve that content

### Basic concepts

#### blob

```bash
git hash-object
# take the content and merely return the unique key that would be used to store it in Git database
# options
# -w: write the content to the database
# --stdin: get the content to be processed from stdin; otherwise, a filename argument is expected
# return 40-character checksum hash in SHA-1 hash
```

##### how Git stores the content initially

- Each content is stored a single file as **blob**
  - named with the SHA-1 checksum of the content and its header
    - The subdirectory is named with the first 2 characters of the SHA-1, and the filename is the remaining 38 characters.

```bash
git cat-file
# inspect git objects
# options
# -p: first figure out the type of content, then display it appropriately
# -t: tell the git object type
```

### tree

> storing the filename and also allows you to store a group of files together

| Git system | UNIX filesystem        |
| ---------- | ---------------------- |
| tree       | directory              |
| blob       | indoes or file content |

A single tree object contains one or more entries, each of which is

- the SHA-1 hash of a blob
- or subtree with its associated
  - **mode**
    - for blob
      - 100644: normal file
      - 100755: executable file
      - 120000: symbolic link
    - for directories
      - 040000
    - for submodules
  - **type**
  - **filename**

```bash
git cat-file -p master^{tree}
# master^{tree} syntax specifies the tree object that is pointed to by the last commit on your master branch
```

```bash
git update-index
# update the index in the staging area
# options
# --add: non-file into stagint area
# --cacheinfo: add content directly into database

git update-index --add --cacheinfo 100644 \
  83baae61804e65cc73a7201a7252750c76066a30 test.txt
```

```bash
git write-tree
# write the staging area out to a tree object
# options
# -w: automatically creates a tree object from the state of the index if that tree doesn’t yet exist

```

### commit

A commit object specify

- a top-level tree for the snapshot to be committed
- the parent commits if any
- the author/committer information
- commit message

```bash
git commit-tree

# create a commit object with a tree as the top-level tree

```

## Git Reference

> a normal reference points to a commit

```bash
git update-ref
# update a reference

```

### HEAD

HEAD file is a symbolic reference to the branch you’re currently on

- symbolic reference, unlike a normal reference, contains a pointer to another reference

### Git Tag

Tag contains a tagger, a date, a message, and a pointer

- the pointer points a commit, rather a tree, as a commit object does
- the pointer never moves
- types:
  - annotated
    - writes a reference to point to it rather than directly to the commit
    - can tag any kind of object
  - lightweight

### Remote Reference

> Remote Reference refers to remote branch information

- read-only
- bookmarks to the last known state of where those branches were on those servers.
