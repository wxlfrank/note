# Linux

<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />


## Process

A **process** is

- an instance of a program in execution
- an entity to which **system resources** (CPU time, memory, etc.) are allocated
- creation
  - almost identical to its parent
  - receives a (logical) copy of **the parent’s address space**
  - executes **the same code as the parent**, beginning at **the next instruction following the process creation system call**
  - have **separate copies of the data (stack and heap)**

### Multithreaded applications in linux

- a process is composed of several **user threads** (or **simply threads**), each of which represents **an execution flow of the process**
- The multiple execution flows of a multithreaded application were **created**, **handled**, and **scheduled** entirely **in User Mode**, usually by means of a POSIX-compliant `pthread` library

### lightweight processes

- share some resources with synchronization


### Thread group

a **thread group** is basically a set of lightweight processes that implement **a multithreaded application** and act as a whole with regards to some system calls


## System Login History

- history file --> **/var/log/wtmp**
- change log rotate config --> edit **/etc/logrotate.conf**
