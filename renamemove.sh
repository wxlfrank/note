#

## use extend glob

shopt -s extglob
# c=$(pwd)
# prefix=${c##*\/}
prefix=$1
if [ -d "${prefix}" ]; then
    loc="$(pwd)"
    cd "${prefix}"
	[ -f "note.md" ] && mv "note.md" "${loc}/${prefix}.md"
	for file in *.+(svg|png); do
		mv "$file" "${loc}/images/${prefix}-${file}"
	done 2>/dev/null
	if [ -d "images" ]; then
		for file in images/*; do
			mv "$file" "${loc}/images/${prefix}-${file##*\/}"
		done
	fi
    cd "${loc}"
	rm -rf ${prefix}
fi
