# Maven
## Build Lifecycle
Maven is based around the central concept of a build lifecycle.
- A Build Lifecycle is made Up of Phases
  - a build phase represents a stage or a step in the lifecycle
  - These lifecycle phases are executed sequentially to complete the lifecycle.
- A build phase can also have zero or more goals bound to it.
  - If a build phase has no goals bound to it, that build phase will not execute.
  - If it has one or more goals bound to it, it will execute all those goals

```bash
# bash
mvn phase*
```
- A Build Phase is made Up of Plugin Goals
A **plugin goal** represents a specific task (finer than a build phase) which contributes to the building and managing of a project.
  - It may be bound to zero or more build phases.
    - A goal not bound to any build phase could be executed outside of the build lifecycle by direct invocation.
    - If a goal is bound to one or more build phases, that goal will be called in all those phases.
  - The order of execution depends on the order in which the goal(s) and the build phase(s) are invoked.

```
mvn phase* plguin:goals*   
```

### Setting Up Your Project to Use the Build Lifecycle
##### Packaging
- Set the packaging for your project via the equally named POM element <packaging>.
  - Some of the valid packaging values are **jar**, **war**, **ear** and **pom**.
  - If no packaging value has been specified, it will default to jar.
- Each packaging contains a list of goals to bind to a particular phase.

#### Plugins
- **configure plugins in your project to add goals to phases.**
  - **Plugins are artifacts that provide goals to Maven**.
  - a plugin may have one or more goals wherein each goal represents a capability of that plugin.
  - plugins can contain information that indicates which lifecycle phase to bind a goal to.
    - Note that adding the plugin on its own is not enough information - you must also specify the goals you want to run as part of your build.
  - The goals that are configured will be added to the goals already bound to the lifecycle from the packaging selected.
    - If more than one goal is bound to a particular phase, the order used is that those from the packaging are executed first, followed by those configured in the POM.
  - Note that you can use the **<executions\>** element to gain more control over the order of particular goals.
    - **<executions\>** is where you can run the same goal multiple times with different configuration if needed.
    - Separate executions can also be given an ID so that during inheritance or the application of profiles you can control whether goal configuration is merged or turned into an additional execution.
```xml
<!--
  echo a myproperty during clean
  mvn -D myproperty=value clean
-->
<plugin>
				<groupId>org.apache.maven.plugins</groupId>
				<artifactId>maven-antrun-plugin</artifactId>
				<version>1.8</version>
				<executions>
					<execution>
						<phase>clean</phase>
						<configuration>
							<tasks>
                <echo>${myproperty}</echo>
							</tasks>
						</configuration>
						<goals>
							<goal>run</goal>
						</goals>
					</execution>
				</executions>
      </plugin>
```


## Properties
Maven properties are value placeholder, like properties in Ant.
- Their values are accessible anywhere within a POM by using the notation ${X}, where X is the property.
- Or they can be used by plugins as default values
- They come in five different styles:
  1. **env.X**: Prefixing a variable with "env." will return the shell's environment variable.
```bash
${env.PATH} contains the PATH environment variable.
```
**Note**:
    - While environment variables themselves are case-insensitive on Windows, lookup of properties is case-sensitive.
    - In other words, while the Windows shell returns the same value for %PATH% and %Path%, Maven distinguishes between ${env.PATH} and ${env.Path}.
    - As of Maven 2.1.0, the names of environment variables are normalized to all upper-case for the sake of reliability.
  2. **project.x**: A dot (.) notated path in the POM will contain the corresponding element's value.
```bash
<project><version>1.0</version></project> is accessible via ${project.version}.
```
  3. **settings.x**: A dot (.) notated path in the settings.xml will contain the corresponding element's value.
```bash
<settings><offline>false</offline></settings> is accessible via ${settings.offline}.
```
  4. **Java System Properties**: All properties accessible via java.lang.System.getProperties() are available as POM properties, such as ${java.home}.
  5. **x**: Set within a <properties /> element in the POM. The value of <properties><someVar>value</someVar></properties> may be used as ${someVar}.
