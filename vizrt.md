# Vizrt

## Introduction

### Challenge

1. build successful media (device adaptable)
2. efficient workflow

### Techniques

Two engines:

1. `Viz One`: Media Managment
2. `Viz Engine`: visualization

## History

- 1997 Pilot Broadcast Systems AS
- 1999 Peak Broadcast Systems (fusion of Pilot and Peak Software Technologies GmbH, Austria)
- 2000 Vizrt (fusion with RT-SET (Real Time Synthesized Entertainment Technology) Ltd, Israel)
- 2005 acquire Curious Software (London)
- 2005 list on Oslo Stock exchange (removed in 2014)
- 2005 acquire 19% Adactus AS (acquire 100% in 2010)
- 2006 acquire Ardendo, Sweden
- 2008 acquire Escenic (content managment software, sold since 2013, 75.5% are sold until 2014)
- 2012 acquire LiberoVision, Swiss --> `Viz Libero`
- 2013 acquire Mosart Medialab (the Mosart control room automation system)

## Products

four groups

1. Broadcast
   1. Viz Pilot
      - `template based` system for journalists to create, manage, and deliver high volumes of top-quality content
      - interfaces
        - Pilot Director: locally installed client with `playlist`
        - Pilot News: Active-X based newsroom plugin
        - Pilot Edge: `HTML` newsroom interface or plugin
   2. Viz One
      - efficiently manages all your media
2. Digital
3. Sports
4. Infrastructure and Platform
   1. Viz Engine
      - real-time graphics rendering engine (renders animated 3D scenes in real-time, producing high-end animations in HD, 4K and beyond)
      - video servers
      - the graphics and video compositing platforms
   2. Viz Artist
      - enables designers to build complete `virtual sets` as well as complex 3D animations

## Viz Pilot System

Viz Pilot is mainly used for `control room`, `newsroom` and `template design`.

### Domain concepts

- scence
  - build in `Viz Artist`
  - a single scene
  - `a transition logic scene`
    - mutiple `front scene` (the layered scenes)
    - a `background scene` (the controlling scene): logic that enables an operator to toggle the states of multiple front scene, typically also containing back plates and transition effects (pre-configured or customized) for each state of the front scenes
  - `control plugin`: a graphic scene containing all objects that can be controlled from a template, e.g., text, back plates, images, colors, etc
    - `control object`: every scene with control plugins **must** have one instance of the control object at the root level of a scene tree.
      - reads the information from all other control plugins
- template
  - build in `Template Wizard` based on scene (contain reference to scenes)
  - `component`: used to create text fields, buttons, drop-lists, database connections etc
- data element
  - build in `Director` or `Viz Pilot News`
  - a template that is filled with data, containing reference to its template
  - added to a `playlist` for `playout` on-air
- playlist
  - a list of `stories` containing a set of `data elements`

### Components

- Viz Pilot Database
  - two options
  1.  Oracle Database (old)
  2.  Graphic Hub (new, free, Vizrt Owned Product)
      1. store files (images, fonts, audio, geometry, videos and scenes), optimized to deliver graphics as fast as possible
      2. connection tool: `Graphic Hub Terminal`
  - optional services
    - Thumbnail Generator
    - Crop Service
- Media Sequencer
  - store all `playlist` and shows making it available to ALL `control applications`
  - communication with Viz Engine and other systems
- Viz Pilot News
- Director
  1.  dependency
      1. Media Sequencer (`playout`)
      2. the Viz Pilot Database (`templates` and `content`)
      3. A Viz Engine with a connection to a Graphic Hub for `content creation` and `graphics playout` is also required for playout.
      4. connection to a newsroom system for retrieving `playlist`
      5. connection to Viz One for the search and transfer of video clips to Viz Engine when playout of video clips
  2.  functions
      - managing graphics and video playout of both `MOS (Media Object Server Communications Protocol) rundowns` and any other `playlist`
- Template Wizard

### Service Providers

1. the Pilot Data Server: an application server for accessing
   - Viz Pilot’s database (Graphic Hub)
   - services
     - in Viz Pilot workflows
       - Crop Service
       - Template Tagging
       - Update Service
       - Person Search
       - search on Viz One
       - Thumbnail Generator
       - Timeline Editor
     - for integrating with Viz Story, Viz Pilot Edge and Template Builder.
2. Object Store (image database)
3. Thumbnail Generator
4. The Preview Server
   1. used when the Viz Engine must provide frames for snapshot or thumbnail generation
   2. used by Viz Pilot News to fetch previews of overlay graphics for the Timeline Editor

## Viz Pilot Edge

### workflow

1. Viz Artist
   1. enables designers to build complete virtual sets as well as complex 3D animations
   2. build scene
2. Template Builder --> template

### Document

1. [Overview of Pilot Workflow](https://docs.google.com/presentation/d/19QkhivtzAQV0aZ8aRfjVB433rjCc_I88sZ6gwZZQN0w/edit?usp=sharing)
2. [Product Document](https://docs.google.com/presentation/d/19QkhivtzAQV0aZ8aRfjVB433rjCc_I88sZ6gwZZQN0w/edit?usp=sharing)

## Development

### Environment Setup

- `Java 1.8`
- `Git`
- `Gradle`
- `Eclipse`
- `Server`: `bgo-eddie-vm` including
  - `PDS`
  - `Graphic Hub`
  - `Media Sequence`

### Build Product

1. how to build a product
   1. `./gradlew PilotEdge:dsw`
   2. `http://localhost:8888/PilotEdge.html?pilot=bgo-eddie-vm`
2. how to depoly a product

### Git Workflow

1. pick a issue/ticket
2. create

## Administration

1. [Employee Handbook](https://drive.google.com/file/d/17KT2kJQv856xM_IEZcGYVmLbGx8HMQmw/view)

## Work Environment Setup (Mac)

1. Virtual machine
   1. [VMWare Fusion](https://www.vmware.com/products/fusion/fusion-evaluation.html)
2. VizArtist
   1. use [DeployTool](https://gitlab.vizrt.com/deploytool/deploytool)

## Backend

### PDS

#### Template

- `GET /templates/{id} atom:entry`
  - get a template for a given template id, information include
    - id
    - title
    - update date
    - links:
      - `elements`: data elements basing on the template
      - `thumbnailfeed`: thumbnail of the template
      - `alternate`: model of the template
      - `self`: reflexive link
      - `edit`: reflexive link also
        - **TODO**: two reflexive link
      - `masterTemplate`: the masterTemplate of the template
        - **TODO** any definition of the masterTemplate?
      - `saveDataElement`: the link to save a data element basing on the template
      - `related`: the changelog of the template
    - thumbnail
    - atom-ext:pilotbid
    - atom-ext:templatekind { PilotGraphicTriopage | PilotGraphicPayload | VideoWallPreset}
- `GET /templates/{id}/thumbnails`
  - get the thumbnail of a given template id
  - **deprecated, service returing error**
  - **TODO**: why it is still here if it is deprecated
- `GET /templates/{id}/thumb`
  - get the thumnail of a given template id
- `GET /templates/{id}/master_template element`
  - get the master template of a given template id
- `GET /templates/{id}/bgfx_master application/vnd.vizrt.bgfx.mastertemplate+xml`
  - get the master template of a given template id
  - **TODO**: what is the difference with the previous one?
- `GET /templates/{id}/model vdf:model`
  - get the model of a given template id including
    - model
    - `bgfx:generatedmodel`
      - **TODO**: need a doc for this
    - mastertemplate (type:element)
- `GET /changelog/templates/{id} atom:feed`
  - get the changelog of ta given template id

#### Data Element

- `GET /dataelements/{id} atom:entry`
  - get a data element
  - containing referred `template`, `payload`, `mosxml`
- `GET /dataelements?template={template_id} atom:feed`
  - get all the data elements created basing on a given template
- `GET /dataelements/{id}/payload vdf:payload`
  - get the payload of a given data element id
- `GET /dataelements/{id}/thumbnails`
  - get the thumbnail of a given data element
  - deprecated, use preview server to retrieve thumbnail for a data element [snapshot](#snapshot)
- `GET /dataelements/{id}/mosxml application/vnd.vizrt.mosobj+xml`
  - get the MOS XML of a given data element

#### Scenes

- `GET /scene_info/{id} element?`
  - get the scene info of a given scene id

### Preview Server

#### snapshot

- `GET /api/preview/snapshot?s={request}&p={payload}`
  - `request`
    - `type`: vizrt.snapshotrequest
    - properties of snapshot, e.g.,
      - position of the snapshot within the timeline of the element
      - properties of returned image
  - `payload`
    - url to retrieve payload or the payload itself
    - VDF representation of the element, which can be obtain through the Meida Sequence API

### Graphic Hub

#### Open Search

- `GET /opensearchdescription application/opensearchdescription+xml`
- `GET /opensearchdescription/{id} application/opensearchdescription+xml`
  - get the open search description of the server, including
    - Url: the url to perform open search
    - vizmedia:media: the media types supported, e.g.,
      - image
      - video
      - audio
    - vizcategory:categories: the resources categories, e.g,
      - image
      - video_clip
      - audio_clip
      - scene
      - geometric
      - material
      - material advanced
      - base font
      - font
      - shared memory map
      - folder
      - project

#### Resource (Scene)

- `GET /file/{id} atom:enty`
- `GET /file/{id}/{containing_folder_id} atom:enty`
  - get the resource description of a given resource id, including
    - author
    - title
    - id
    - update timestamp
    - publish timestamp
    - resource category
      - **TODO**: why two categories, e.g., SCENE and asset
    - links
      - `up`: containing folder
      - `self`: reflexive link
      - `describedby`: payload of the resource
      - `alternate`: payload of the resource
        - **TODO**: are describedby and alternate the same?
        - **TODO**: if not, when are describedby and alternate different?
      - `history`: history
      - `addons`: addons
    - `bgfx:scene`: \<id> what is this used for
      - specific for scene?
    - `vosmt:media`: the mediatype of the resource
- `GET /folder/{id} atom:entry`
  - get the folder description of a given folder id, including
    - author
    - title
    - id
    - update timestamp
    - category
    - links
      - `search`: open search link
      - `up`: containing folder link
      - `self`: reflexive link
      - `down`: children folder contained
      - `describedby`: paylod of the folder
      - `alternate`: paylod of the folder
        - **TODO**: are describedby and alternate the same?
        - **TODO**: if not, when are describedby and alternate different?
      - `addon atom:feed`
        - **TODO**: what is this
      - `related`: children contained (excluding folders?)
- `GET /folders/{id} atom:feed`
  - get the children folders of a given folder id
- `GET /files/{id} atom:feed`
  - get the children resource, excluding children folders?, of a given folder id
- `GET /metadata/folder/{id} vdf:payload`
  - get the metadata (payload) of the given folder id
  - **TODO**: can be atom:feed (according Rolf doc)
- `GET /metadata/file/{id} vdf:payload`
  - get the metadata (payload) of the given resource id
  - **TODO**: can be atom:feed (according Rolf doc)
- `GET /addons/{id} atom:feed`
  - **TODO**: what is addons?
- `GET /histories/{id} atom:feed`
  - **TODO**: the history of a given resource id?

## Digital Video

> digit images displayed in rapid succession
>
> - duration: time length
> - frame size: WxH
> - color depth: 24 normally
> - frame rate: FPS
> - compression factor: FC
> - BPP = CD/CF

### `frame`

- an orthogonal bitmap digital image
- a raster of pixels
- `frame rate` (**FPS**)
  - file: 24fps
  - NTSC: 30/1.001 &#x2248; 29.97fps
  - PAL: 25fps
  - frame collection
    - interlaced: more frame, smoother
    - progressive scan: less frame, sharper
- `bit rate` (**BPP**, bit per pixel)
  - affects _video quality_ and _video size_

#### `keyframe`

- animation:
  - defines the starting and ending points of any smooth transition
    - A sequence of keyframes defines which movement the viewer will see, whereas the position of the keyframes on the film, video, or animation defines the timing of the movement
    - Because only two or three keyframes over the span of a second do not create the illusion of movement, the remaining frames are filled with inbetweens
- compression:
  - incremental storage
  - keyframe is for big frame change, support random access
- editing:
  - general: parameter changes start/end
  - vizrt: specifies parameter that affect how media is presented and how that presentation varies across the time span of the timeline segment that the media occupies.
    - Available adaptation parameters include
      - audio volume adjustment
      - interest area

### DASH

#### Adaptive bitrate streaming

> a technique used in streaming multimedia over computer networks.
>
> - old way: use streaming protocols such as RTP with RTSP,
> - new way: exclusively based on HTTP.
> - workflow
>   - detect a user's bandwidth and CPU capacity in real time and adjusting the quality of the media stream accordingly.
>   - requires the use of an encoder which can encode a **single source media** (video or audio) at **multiple bit rates**.
>   - The player client switches between streaming the different encodings depending on available resources
> - expectation: very little buffering, fast start time and a good experience for both high-end and low-end connections.

## Backend Implementation

### Old Data Concepts

- Model
- MasterTemplate (GeneratedModel)
  - check whether a template is old : `!HasTemplateControlObject(templateId)`

### New Data Concepts

- ModelElement
- GeneratedModel (MasterTemplate)

### Conversion between Old and New Data Concetps

#### Old --> New

- reader = VdfSerializer.CreateXml(model).CreateReader()
- ModelElement.Load(reader)

#### New --> Old

- reader = modelElemet.CreateReader()
- VdfSerializer.ReadModel(reader)

## Refinement

1. avoid unnecessary request
   1. double request
   2. when value in field editor is changes, the value unrelated to the current variant should be avoid sending snapshot
2. UI improvement
   1. Title has no visualization on preview, should be reflected
   2. warning for fieldedtior should be shown with the title of the field, e.g., #12 field lower third title
   3. Error report: when snapshot is not available because of absence of scene, just show the scene is not accesible.
   4. Concept Manager
      1. concept creation should be possible here
      2. it is possible to delete empty concept; no template is linked to the concept
