# PDS

## Data flow

### how to get templates

1. `controller` of type `TemplateController` method `GetTemplates` 
2. `backend` of type `IDataServerBackend` method `GetTemplates` with `TemplateQueryParams` constructed from Request
   1. implementation of `IDataServerBackend` is `DataServerBackend`
   2. `server` of type `IPilotServer` method `GetTemplateCollection` with `TemplateQueryParams`
      1. implementation of `IPilotServer` is `PilotServer`
      2. `provider` of type `IPilotProvider` methods `GetTemplates` with a list of parameters from `TemplateQueryParams`
         1. implementation of `IPilotProvider` is `PilotOracleDBProvider`
         2. `pilotConnection` of type `IManagedDatabaseConnection` method `GetTemplatesWithXmlTags` with a list of parameters