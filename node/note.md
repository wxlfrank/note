How do I update each dependency in package.json to the latest version?
```bash
npm i -g npm-check-updates
ncu -u
npm install
```
