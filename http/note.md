# HTTP

## Details


## Header Fields
Each header field
- consists of a case-insensitive field name
- followed by a colon (":"),
- optional leading whitespace, the field value, and optional trailing whitespace.


## curl
-c, --cookie-jar filename // write cookie to a file
-b, --cookie filename // read cookie from a file
-d, --data data // HTTP POST data
-H, --header header | @filename
-i, --include // include protocol response headers in the output
-o, --output filename // output to a file instead of stdout:q
