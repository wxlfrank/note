# Gradle

<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

## Build Lifecycle
The core of Gradle is a language for dependency based programming
- to define tasks and dependencies between tasks
- these tasks are executed in the order of their dependencies
- each task is executed only once
- these tasks form a Directed Acycle Graph (DAG)
  - tools to build such a DAG
- Gradle builds the complete dependency graph before any task is executed
- build scripts configure this dependency graph.
  - they are strictly speaking build configuration scripts.

### Build phases
A Gradle build has three distinct phases
1. Initialization
  - determines which projects are going to take part in the build
  - creates a Project instance for each of these projects
2. Configuration means executing the build.gradle file of a project
  - project objects are configured
  - The build scripts of all projects which are part of the build are executed
  - e.g. downloading all plugins that were declared using 'apply plugin'
  - when a single task, from a single project is requested, all projects of multi-project build are configured first
  - **Configuration on demand** (since version 1.4) mode attempts to configure only projects that are relevant for requested tasks
    - the configuration time of a large multi-project build can be reduced
  - In Gradle build scripts are optional.
    - for a single project build, a project without a build script doesn’t make much sense.
    - For multiproject builds the situation is different.
3. Execution
  - determines the subset of the tasks which are created and configured during the configuration phase to be executed.
    - Gradle always evaluates every project of the multi-project build and creates all existing task objects.
  - The subset is determined by the task name arguments passed to the gradle command and the current directory.
  - Gradle then executes each of the selected tasks.
    - Execute all tasks down the hierarchy which have this task name.
  - If nothing else is defined, Gradle executes the task in alphanumeric order

### two types of dependencies
1. configuration dependencies
  1. submodules depend on root module
2. execution dependencies
  1. root module depends on submodules
  - A project dependency is a special form of an execution dependency.
    - Project dependencies model dependencies between modules
    - It causes the other project to be built first and adds the jar with the classes of the other project to the classpath.
    - It also adds the dependencies of the other project to the classpath.
  - dependency of  an output produced by another task
    1. produce the output,
    2. mark it as an "outgoing" artifact
      - or add it to the output of the main source set which you can depend on in the consuming project.
### Project and task paths
A project path has the following pattern:
1. It starts with an optional colon, which denotes the root project.
  - The root project is the only project in a path that is not specified by its name.
2. The rest of a project path is a colon-separated sequence of project names, where the next project is a subproject of the previous project.

The path of a task is simply its project path plus the task name, like “:bluewhale:hello”.
  - Within a project you can address a task of the same project just by its name.
  - This is interpreted as a relative path.

### Configuration injection
The capability to configure a project build from any build script is called **cross project configuration**. Gradle implements this via **configuration injection**.
```groovy
Closure cl = { task -> println "I'm $task.project.name" }
task('hello').doLast(cl)
project(':bluewhale') {
    task('hello').doLast(cl)
}
```

The Project API provides
- a method **project()**, which takes a path as an argument and returns the Project object for this path.
- a property **allprojects** which returns a list with the current project and all its subprojects underneath it
  - If you call allprojects with a closure, the statements of the closure are delegated to the projects associated with allprojects
- a property **subprojects** for accessing the subprojects only
- configure() method takes a list as an argument and applies the configuration to the projects in this list.
```groovy
allprojects {
    task hello {
        doLast { task ->
            println "I'm $task.project.name"
        }
    }
}
subprojects {
    hello {
        doLast {
            println "- I depend on water"
        }
    }
}
configure(subprojects.findAll {it.name != 'tropicalFish'}) {
    hello {
        doLast {
            println '- I love to spend time in the arctic waters.'
        }
    }
}
```
may only construct a task once in a project, but you may add any number of code blocks providing additional configuration.
- uses the “task” keyword, constructs the task and provides it’s base configuration
- doesn’t use the “task” keyword, as it is further configuring the existing task

#### properties
Configure build environment, listed in order of highest to lowest precedence (first one wins)
1. command-line flags, e.g., -Dname=value
2. system properties stored in a gradle.properties
3. gradle properties stored in gradle.properties
4. environment variables, e.g., GRADLE_OPTS sourced by the environment that executes Gradle
you can also configure a given **project build** using Project properties such as -PreleaseType=final.

### Settings file
- Default name: settings.gradle
- Gradle looks for a settings.gradle file in the following way:
  1. It looks in a directory called **master** which has the same nesting level as the current dir.
  2. If not found yet, it searches parent directories.
  3. If not found yet, the build is executed as a single project build.
  4. If a settings.gradle file is found, Gradle checks if the current project is part of the multiproject hierarchy defined in the found settings.gradle file.
    1. If not, the build is executed as a single project build.
    2. Otherwise a multiproject build is executed.
- The settings file is executed during the initialization phase
- -u command line option to tell Gradle not to look in the parent hierarchy for a settings.gradle file
  - current project is then always built as a single project build.
  - If the current project contains a settings.gradle file, the -u option has no meaning
- mandatory for a multiproject build
  - in the root project of the multiproject hierarchy.
  - because it defines which projects are taking part in the multi-project build
- optional for a single-project build
- **define the included projects**
- **add libraries to build script classpath**

### Multi-project builds
A multi-project build is a build where you build more than one project during a single execution of Gradle.
- Multi-project builds are always represented by a tree with a single root.
  - Each element in the tree represents a project.
    - A project has a path which denotes the position of the project in the multi-project build tree.
    - In most cases the project path is consistent with the physical location of the project in the file system.
      - this behavior is configurable.
  - The project tree is created in the settings.gradle file.
    - By default the location of the settings file is also the location of the root project.
      - But you can redefine the location of the root project in the settings file.
- Cross project configuration
  - the same configuration affects several subprojects

#### Hierarchy layouts
```groovy
include 'project1', 'project2:child'
```
- The **include method** takes project paths as arguments.
  - The project path is assumed to be the relative physical file system path.
    - a path 'services:api' is mapped by default to a folder 'services/api' (relative from the project root)
  - only need to specify the leaves of the tree
    - the inclusion of the path 'services:hotels:api' will result in creating 3 projects: 'services', 'services:hotels' and 'services:hotels:api'

#### Flat layouts
```groovy
includeFlat 'project1' 'project2'
```
- The **includeFlat method** takes directory names as an argument.
  - These directories need to exist as siblings of the root project directory.
  - The location of these directories are considered as child projects of the root project in the multi-project tree.

#### Multi-project three
The multi-project tree created in the settings file is made up of so called **project descriptors**
- Access and change project descriptors

```groovy
println rootProject.name
println project(':projectA').name
rootProject.name = 'main'
project(':projectA').projectDir = new File(settingsDir, '../my-project-a')
project(':projectA').buildFileName = 'projectA.gradle'
```

### Lifecycle notifications
Receive notifications as the build progresses through its lifecycle
1. implement a particular listener interface
2. provide a closure to execute when the notification is fired

#### before and after a project is evaluated
```groovy
allprojects {
    afterEvaluate { project ->
        if (project.hasTests) {
            println "Adding test task to $project"
            project.task('test') {
                doLast {
                    println "Running tests for $project"
                }
            }
        }
    }
}
```

#### tasks is added to a project
```groovy
tasks.whenTaskAdded { task ->
    task.ext.srcDir = 'src/main/java'
}

task a

println "source dir is $a.srcDir"
```

#### task DAG is ready
#### before and after task is executed
```groovy
task ok

task broken(dependsOn: ok) {
    doLast {
        throw new RuntimeException('broken')
    }
}

gradle.taskGraph.beforeTask { Task task ->
    println "executing $task ..."
}

gradle.taskGraph.afterTask { Task task, TaskState state ->
    if (state.failure) {
        println "FAILED"
    }
    else {
        println "done"
    }
}
```

```groovy
gradle --stop
gradle --status
```

## Gradle Wrapper

The Wrapper is a script that invokes a declared version of Gradle, downloading it beforehand if necessary
![alt](images/gradle-wrapper-workflow.png)

- Standardizes a project on a given Gradle version, leading to more reliable and robust builds.
- Provisioning a new Gradle version to different users and execution environment (e.g. IDEs or Continuous Integration servers) is as simple as changing the Wrapper definition.

- `gradle-wrapper.jar`
  - The Wrapper JAR file containing code for **downloading the Gradle distribution**.
- `gradle-wrapper.properties`
  - A properties file responsible for configuring the Wrapper runtime behavior e.g. the Gradle version compatible with this version.
- `gradlew`, `gradlew.bat`
  - A shell script and a Windows batch script for executing the build with the Wrapper.

### Add the Gradle Wrapper

- `gradle wrapper`
  - `--gradle-version`
  - `--distribution-type`: **bin**, **all**
  - `--gradle-distribution-url`
    - obselete `--gradle-version` and `--distribution-type`
  - `--gradle-distribution-sha256-sum`


## Directories and files

### Gradle user home directory

The `Gradle user home directory` (**$USER_HOME/.gradle** by default) is used to store

- **global configuration properties** 
- **initialization scripts** 
- **caches** and **log** files
  - Gradle automatically cleans its user home directory (since 4.10)

```javascript
├── 4.4.1
│   ├── fileChanges
│   ├── fileHashes
│   └── taskHistory
├── buildOutputCleanup
│   ├── buildOutputCleanup.lock
│   ├── cache.properties
│   └── outputFiles.bin
├── caches // Global cache directory
│   ├── 4.3 // Version-specific caches (e.g. to support incremental builds)
│   ├── 4.4.1
│   ├── 4.8.1
│   ├── 5.2.1
│   ├── build-cache-1
│   ├── jars-3 // Shared caches (e.g. for artifacts of dependencies)
│   ├── journal-1
│   ├── modules-2
│   ├── transforms-1
│   └── transforms-2
├── daemon // Registry and logs of the Gradle Daemon
│   ├── 4.10.2
│   ├── 4.3
│   ├── 4.4.1
│   ├── 4.5.1
│   ├── 4.8.1
│   └── 5.2.1
├── native
│   ├── 25
│   ├── 28
│   └── jansi
├── workers
└── wrapper
    └── dists // Distributions downloaded by the Gradle Wrapper
└── init.d // Global initialization scripts
    └── my-setup.gradle
├── gradle.properties //Global Gradle configuration properties
```

### Project root directory

```javascript
├── build // The build directory of this project into which Gradle generates all build artifacts.
│   ├── classes
│   ├── reports
│   ├── resources
│   ├── test-results
│   └── tmp
├── build.gradle // The project’s Gradle build script
├── gradle.properties // Project-specific Gradle configuration properties
├── .gradle // Project-specific cache directory generated by Gradle
│   ├── 4.4.1 // Version-specific caches (e.g. to support incremental builds)
│   ├── 4.8.1
│   ├── buildOutputCleanup
│   └── vcsWorkingDirs
├── gradle
│   └── wrapper // Contains the JAR file and configuration of the Gradle Wrapper
├── gradlew // Scripts for executing builds using the Gradle Wrapper
├── gradlew.bat // Scripts for executing builds using the Gradle Wrapper
├── settings.gradle // The project’s settings file
└── src // source files of your project
    ├── main
    └── test
```

## Properties

### System Properties

- pass a `system property` to the `JVM` which runs Gradle
- syntax: `-D property=value`
  - use the prefix **systemProp** in **gradle.properties** 
    - In **a multi project build**, “systemProp.” properties set in any project **except the root** will be **ignored**

### Environment variables

- used for **the environment that executes Gradle**


### Precedence

1. command-line options
2. system properties
3. environment variables

## Plugins
A plugin is an extension to Gradle which configures your project, typically by adding some pre-configured tasks which together do something useful.

### Java Plugin
Java plugin adds some tasks to your project which will
- compile and unit test your Java source code, and
- bundle it into a JAR file.
- add pre-defined configurations to your project
- The Java plugin is convention based.
  - the plugin defines default values for many aspects of the project
    - source code: *src/main/java*
    - test source code: *src/test/java*
    - any files under *src/main/resources* will be included in the JAR file as resources, and
    - any files under *src/test/resources* will be included in the classpath used to run the tests.
    - All output files are created under the *build* directory, with the JAR file ending up in the *build/libs* directory

### Apply a plugin
```groovy
apply plugin: 'java'
```
This applies the Java plugin to your project, which adds a number of tasks to your project.
- use ***gradle tasks*** to list the tasks of the project

### Dependencies
![](dependency-management-resolution.png)
A build script developer can declare dependencies for different ***scopes***
- just for compilation of source code or
- for executing tests.

Gradle takes your **dependency declarations** and **repository definitions** and attempts to download all of your dependencies by a process called ***dependency resolution***.
1. Given a required dependency, Gradle attempts to resolve the dependency by searching for the module the dependency points at.
  - Each repository is inspected in order.
  - Depending on the type of repository, Gradle looks for metadata files describing the module (.module, .pom or ivy.xml file) or directly for artifact files.
  - If the dependency is declared as a dynamic version (like 1.+), Gradle will resolve this to the highest available concrete version (like 1.2) in the repository.
    - For Maven repositories, this is done using the maven-metadata.xml file, while for Ivy repositories this is done by directory listing.
  - If the module metadata is a POM file that has a parent POM declared, Gradle will recursively attempt to resolve each of the parent modules for the POM.
2. Once each repository has been inspected for the module, Gradle will choose the 'best' one to use
  1. For a dynamic version, a 'higher' concrete version is preferred over a 'lower' version.
  2. Modules declared by a module metadata file (.module, .pom or ivy.xml file) are preferred over modules that have **an artifact file** only.
  3. Modules from earlier repositories are preferred over modules in later repositories.
  4. When the dependency is declared by a concrete version and a module metadata file is found in a repository, there is no need to continue searching later repositories and the remainder of the process is short-circuited.
3. All of the artifacts for the module are then requested from the same repository that was chosen in the process above.

Once resolved, the resolution mechanism stores the underlying files of a dependency in a local cache, also referred to as the **dependency cache**.

#### Configuration
the scope of a dependency is called a configuration
![](dependency-management-configurations.png)
A custom configuration is useful for separating the scope of dependencies needed for a dedicated purpose.

A configuration can extend other configurations to form an inheritance hierarchy.
- Child configurations inherit the whole set of dependencies declared for any of its superconfigurations.

#### Customize project by change properties
use **gradle properties** to list the properties of a project.
- This will allow you to see the properties added by the Java plugin, and their default values.
