let selectTable = document.getElementById('selectTable');
let selectList = document.getElementById('selectList');
let downloadMP3 = document.getElementById('downloadMP3');

// chrome.storage.sync.get('color', function (data) {
//     selectTable.style.backgroundColor = data.color;
//     selectTable.setAttribute('value', data.color);
// });

function tr2row(tr) {
    var cells = [...tr.children];
    var makdn = '';
    if (cells.length > 0) {
        makdn += '|';
    }
    for (var i = 0; i < cells.length; i++) {
        makdn += cells[i].innerText.trim() + '|';
    }
    if (cells.length > 0) {
        makdn += '<br>';
    }
    return makdn;
}

function table2makdn(table) {
    var trs = [...table.getElementsByTagName('tr')];
    var result = '';
    console.log(trs[0].children);
    var array = Array(trs[0].children.length + 1).fill('|').join('---') + '<br>';
    result += tr2row(trs[0]);
    result += array;
    trs.slice(1).forEach(tr => {
        result += tr2row(tr);
    })
    return result;
}

function list2makdn(list, prefix) {
    var type = list.tagName;
    var isUntyped = type === 'UL';
    var makdn = '';
    var lis = [...list.children];
    lis.forEach((l, i) => {
        makdn += (prefix != null ? prefix : '') + (isUntyped ? '- ' : (i + 1) + '. ') + l.innerText.trim() + '<br>';
    })
    return makdn;
}

var map = {
    table: table2makdn,
    ul: list2makdn,
    ol: list2makdn
};

function perform(action, selector, fn) {
    chrome.tabs.query({
        active: true,
        currentWindow: true
    }, function (tabs) {
        chrome.tabs.sendMessage(tabs[0].id, {
            action: action,
            parameters: [selector]
        }, function (response) {
            if (response != null) {
                var div = document.createElement('div');
                div.innerHTML = response.result;
                fn && fn(div, response.baseUrl, selector);
            }
        });
    });
}

function toMarkdown(div, url, selector) {
    div.innerHTML = map[selector](div.firstElementChild);
    selectTable.parentNode.append(div);
}

function download(div, base) {
    [...div.children].forEach(v => {
        chrome.downloads.download({
            url: "" + new URL(v.getAttribute('href'), base)
        });
    })
}

downloadMP3.onclick = function () {
    perform('query', 'a[href$=".mp3"]', download);
}

selectTable.onclick = function (element) {
    perform('findCloset', 'table', toMarkdown);
};
selectList.onclick = function (params) {
    perform('findCloset', 'ul', toMarkdown);
    perform('findCloset', 'ol', toMarkdown);
}
document.body.onload = function (params) {
    chrome.runtime.getBackgroundPage(bgp => {
        chrome.tabs.query({
            active: true,
            currentWindow: true
        }, function (tabs) {
            chrome.tabs.executeScript(
                tabs[0].id, {
                    file: 'content.js'
                });
        });
    });
};