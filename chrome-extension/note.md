<link rel="stylesheet" type="text/css" media="all" href="../markdown.css" />

# Chrome Extension

## Background script

`Background script` is the extension's event handler;

- it contains listeners for browser events that are important to the extension.
- It lies dormant until an event is fired then performs the instructed logic.
- An effective background script is only loaded when it is needed and unloaded when it goes idle.

```javascript
chrome.runtime.getBackgroundPage(function callback(Window backgroundPage){})
```

![Architecture](images/popuparc.png)

## UI Elements

Extensions have UI forms:

- `browser_action`
  - `chrome.browserAction` put icons
    - in the main Google Chrome toolbar,
    - to the right of the address bar.
  - can also have a tooltip, a badge, and a popup
  - used to activate the extension on all pages
- `page action`
  - `chrome.pageAction` to put icons
    - in the main Google Chrome toolbar,
    - to the right of the address bar.
  - represents actions that can be taken on the current page, but that aren't applicable to all pages. Page actions
  - appears grayed out when inactive.
  - used to activate the extension on select pages
  - Add `declared rules` to the `background script` with the `declarativeContent API` within the `runtime.onInstalled` listener event.
    - `chrome.declarativeContent API` takes actions depending on the content of a page, without requiring permission to read the page's content.
- `context menus`
- `omnibox`
- `keyboard shortcut`

## Content script

`Function`: Extensions that read or write to web pages utilize a `content script` which

- contains JavaScript that executes in the contexts of a page that has been loaded into the browser.
- read and modify the DOM of web pages the browser visits.

![Content Script Architecture](images/contentscriptarc.png)

`Content scripts` can communicate with their parent extension by

- exchanging `messages` and
- storing values using the `storage API`.

![Content Script Architecture](images/messagingarc.png)

## chrome.storage

chrome.storage API stores, retrieves, and tracks changes to user data.

## events

### onInstalled

Fired

- when the extension is first installed,
- when the extension is updated to a new version, and
- when Chrome is updated to a new version.

```javascript
chrome.runtime.onInstalled.addListener(function callback)
```
