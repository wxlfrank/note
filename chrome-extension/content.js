var utils = {
    findCloset: function (selector) {
        var selection = window.getSelection();
        var table = selection.anchorNode.parentElement.closest(selector)
        return table;
    },
    query: function (selector) {
        return document.querySelectorAll(selector);
    }
};
chrome.runtime.onMessage.addListener(function (request, sender, sendResponse) {
    var result = utils[request.action].apply(null, request.parameters);
    if (result != null) {
        sendResponse({
            baseUrl: document.baseURI,
            result: result instanceof NodeList ? [...result].map(v => v.outerHTML) : result.outerHTML
        });
    } else
        sendResponse(null);
    return true;
})