# Docker

## Docker Overview
Docker is an open platform for developing, shipping, and running applications.
- package and run an application in a loosely isolated environment called **a container**
- containers are lightweight because they run directly within the host machine's kernel.

Docker consists of:
- The Docker Engine - a lightweight and powerful open source containerization technology combined with **a work flow** for building and containerizing applications.
- Docker Hub - a SaaS service for sharing and managing application stacks.
### Docker Engine
![](images/engine-components-flow.png)

Docker Engine is a client-server application with these major components:
- A server which is a type of long-running program called a daemon process (the ***dockerd*** command).
  - listens for Docker API requests
  - manages Docker objects such as images, containers, networks, and volumes
  - also communicate with other daemons to manage Docker services.
- A REST API which specifies interfaces that programs can use to talk to the daemon and instruct it what to do.
- A command line interface (CLI) client (the ***docker*** command) which manages docker objects, such as images, containers and services.
- The CLI uses the Docker REST API to control or interact with the Docker daemon through scripting or direct CLI commands.

#### images
An image is a read-only template with instructions for creating a Docker container.
- use a **Dockerfile** to build your own image (docker build, docker image build)
  - Dockerfile use instructions needed to create the image and run it.
  - Each instruction creates a layer in the image.
  - When you change the Dockerfile and rebuild the image, only those layers which have changed are rebuilt.
```bash
# build a image
docker build PATH|URL
-t tag the build image
--build-arg variable=value # supply variables which can be used 
                    # in the form of ${variable} in Dockerfile
```

#### containers
A container is a runnable instance of an image.
- create (docker create), start (docker start), stop (docker stop), move (?), or delete (docker rm) a container using the Docker API or CLI.
  - A container is defined by its image as well as any configuration options you provide to it when you create or start it.
  - When a container is removed, any changes to its state that are not stored in persistent storage disappear.
- connect a container to one or more networks
- attach storage to it
- create a new image based on its current state.
- By default, a container is relatively well isolated from other containers and its host machine.
  - control how isolated a container’s network, storage, or other underlying subsystems are from other containers or from the host machine.

#### services
Services allow you to scale containers across multiple Docker daemons, which all work together as a swarm with multiple *managers* and *workers*.
- Each member of a swarm is a Docker daemon, and the daemons all communicate using the Docker API.
- A service is created by deploying an application image when **Docker Engine** is in ***swarm mode***. When creating, you can
  - specify which image to use
  - specify which commands to execute inside running containers
  - define options for the service including
    - the port where ***the swarm*** makes the service available outside the swarm
    - ***an overlay network*** for the service to connect to other services in the swarm
    - CPU and memory limits and reservations
    - ***a rolling update policy***
    - the number of replicas of the image to run the swarm
- **A service is the image for a microservice within the context of somer larger application.**


![](docker.svg)



### Docker registries
A Docker registry stores Docker images.
- **Docker Hub** and **Docker Cloud** are public registries that anyone can use
  - Docker is configured to look for images on **Docker Hub by default**.
- You can even run your own private registry.
- If you use **Docker Datacenter (DDC)**, it includes **Docker Trusted Registry (DTR)**.

## Docker architecture
Docker uses a client-server architecture.
- The Docker client talks to the Docker daemon, which does the heavy lifting of building, running, and distributing your Docker containers.
- The Docker client and daemon can run on the same system, or you can connect a Docker client to a remote Docker daemon.
- The Docker client and daemon communicate using a REST API, over UNIX sockets or a network interface.
![Docker architecture](images/architecture.svg)
**Docker host** is the machine that Docker Engine is installed.

## Technologies
Docker is written in Go and takes advantage of several features of the Linux kernel to deliver its functionality.
- **namespaces**: provide the isolated workspace called the container.
  - When you run a container, Docker creates a set of namespaces for that container.
    - **The pid namespace**: Process isolation (PID: Process ID).
    - **The net namespace**: Managing network interfaces (NET: Networking).
    - **The ipc namespace**: Managing access to IPC resources (IPC: InterProcess Communication).
    - **The mnt namespace**: Managing filesystem mount points (MNT: Mount).
    - **The uts namespace**: Isolating kernel and version identifiers. (UTS: Unix Timesharing System).
- **control groups (cgroups)**
  - limit an application to a specific set of resources
  - allow Docker Engine to share available hardware resources to containers and optionally enforce limits and constraints.
- **Union file systems (UnionFS)** are file systems that operate by creating layers, making them very lightweight and fast.
  - Docker Engine uses UnionFS to provide the building blocks for containers.
  - Docker Engine can use multiple UnionFS variants, including AUFS, btrfs, vfs, and DeviceMapper.
- **Container format**
  - Docker Engine combines the namespaces, control groups, and UnionFS into a wrapper called a container format.
  - The default container format is libcontainer

## Docker swarm
Current versions of Docker include swarm mode for natively managing ***a cluster of Docker Engines called a swarm***.
- A swarm consists of **multiple Docker hosts** which
  - run in **swarm mode**
  - act as
    - **managers (to manage membership and delegation)** and
    - **workers (which run swarm services)**.
      - When you create a service, you define its optimal state (
        - number of replicas,
        - network and storage resources available to it,
        - ports the service exposes to the outside world, and more).
      - Docker works to maintain that desired state.
- A given Docker host can be a manager, a worker, or perform both roles.
- A **task is a running container** which is part of a swarm service and managed by a swarm manager, as opposed to a **standalone container**.
  - One of the key advantages of swarm services over standalone containers is that **you can modify a service’s configuration, including the networks and volumes it is connected to, without the need to manually restart the service**.
  - Docker will update the configuration, stop the service tasks with the out of date configuration, and create new ones matching the desired configuration.
  - A key difference between standalone containers and swarm services is that **only swarm managers can manage a swarm, while standalone containers can be started on any daemon.**
### Nodes
**A node is an instance of the Docker engine participating in the swarm.**
#### Manager Node & Worker Node
- To deploy your application to a swarm, you submit a service definition to a manager node.
- The manager node dispatches units of work called tasks to worker nodes.
- Manager nodes perform the orchestration and cluster management functions required to maintain the desired state of the swarm.
- Manager nodes elect a single leader to conduct orchestration tasks.
- Worker nodes receive and execute tasks dispatched from manager nodes.
- By default manager nodes also run services as worker nodes, but you can configure them to run manager tasks exclusively and be manager-only nodes.
- An agent runs on each worker node and reports on the tasks assigned to it.
The worker node notifies the manager node of the current state of its assigned tasks so that the manager can maintain the desired state of each worker.
#### Services and Tasks
A service is the definition of the tasks to execute on the manager or worker nodes.
- It is the central structure of the swarm system and the primary root of user interaction with the swarm.
- When you create a service, you specify which container image to use and which commands to execute inside running containers.
- In the **replicated services model**, the swarm manager distributes a specific number of replica tasks among the nodes based upon the scale you set in the desired state.
- For **global services**, the swarm runs one task for the service on every available node in the cluster.
- A task carries a Docker container and the commands to run inside the container.
  - It is the atomic scheduling unit of swarm.
- Manager nodes assign tasks to worker nodes according to the number of replicas set in the service scale.
- Once a task is assigned to a node, it cannot move to another node. It can only run on the assigned node or fail.
#### Load balancing
The swarm manager uses **ingress load balancing** to ***expose the services you want to make available externally to the swarm.***
- The swarm manager can automatically assign the service a **PublishedPort (in the 30000-32767 range)** or you can configure a PublishedPort for the service.

- External components, such as cloud load balancers, **can access the service on the PublishedPort of any node in the cluster whether or not the node is currently running the task for the service**.
  - All nodes in the swarm route ingress connections to a running task instance.

- Swarm mode has **an internal DNS component that automatically assigns each service in the swarm a DNS entry**.
- The swarm manager uses **internal load balancing to distribute requests among services within the cluster based upon the DNS name of the service**.
## Docker Network
### Objective
- Docker containers wrap a piece of software in a complete filesystem that contains everything needed to run: code, runtime, system tools, system libraries
- By default, containers isolate applications from one another and the underlying infrastructure, while providing an added layer of protection for the application.
- applications communicate with each other, the host or an external network
- allow for proper connectivity while maintaining application portability, service discovery, load balancing, security, performance, and scalability

### The container Network Model (CNM)
The Docker networking architecture is built on a set of interfaces called the Container Networking Model (CNM).
- The philosophy of CNM is to provide application portability across diverse infrastructures.
- This model strikes a balance to
  - achieve application portability and also
  - takes advantage of special features and capabilities of the infrastructure.
![](images/cnm.png)

#### Sandbox
 - **A Sandbox contains the configuration of a container's network stack.** This includes
    - management of the container's interfaces,
    - routing table, and
    - DNS settings.
  - An implementation of a Sandbox could be
    - a Linux Network Namespace,
    - a FreeBSD Jail,
    - or other similar concept.
  - A Sandbox may contain many endpoints from multiple networks.

#### Endpoint
- An Endpoint joins a Sandbox to a Network.
- The Endpoint construct exists so the actual connection to the network can be abstracted away from the application.
  - This helps maintain portability so that a service can use different types of network drivers without being concerned with how it's connected to that network.

### Network
- The CNM does not specify a Network in terms of the OSI model.
- An implementation of a Network could be
  - a Linux bridge, a VLAN, etc.
- A Network is a collection of endpoints that have connectivity between them.
- Endpoints that are not connected to a network do not have connectivity on a network.

### CNM Driver Interfaces
![](images/cnm-api.png)

The Container Networking Model provides **two pluggable and open interfaces**

#### Network Drivers
Docker Network Drivers provide the actual implementation that makes networks work.
- They are pluggable so that different drivers can be used and interchanged easily to support different use cases.
- Multiple network drivers can be used on a given Docker Engine or Cluster concurrently, but **each Docker network is only instantiated through a single network driver**.
- There are two broad types of CNM network drivers:
  - Native Network Driversare **a native part of the Docker Engine and are provided by Docker**.
    - There are multiple drivers to choose from that support different capabilities like overlay networks or local bridges.
  - Remote Network Drivers are network drivers created by the community and other vendors.
    - These drivers can be used to provide integration with incumbent software and hardware.
    - Users can also create their own drivers in cases where they desire specific functionality that is not supported by an existing network driver.

#### IPAM Drivers
Docker has **a native IP Address Management Driver that provides default subnets or IP addresses for networks and endpoints if they are not specified**.
- IP addressing can also be manually assigned through network, container, and service create commands.
- Remote IPAM drivers also exist and provide integration to existing IPAM tools.

Docker Native Network Drivers include
- **Host**	With the host driver, a container uses the networking stack of the host.
  - There is no namespace separation, and
  - all interfaces on the host can be used directly by the container
- **Bridge**	The bridge driver creates **a Linux bridge** on the host that is managed by Docker.
  - By default containers on a bridge can communicate with each other.
  - External access to containers can also be configured through the bridge driver
- **Overlay**	The overlay driver creates **an overlay network** that supports multi-host networks out of the box.
  - It uses a combination of local Linux bridges and VXLAN to overlay container-to-container communications over physical network infrastructure
- **MACVLAN**	The macvlan driver uses the MACVLAN bridge mode to establish a connection between container interfaces and a parent host interface (or sub-interfaces).
  - It can be used to provide IP addresses to containers that are routable on the physical network.
  - Additionally VLANs can be trunked to the macvlan driver to enforce Layer 2 container segmentation
- **None**	The none driver gives a container its own networking stack and network namespace but does not configure interfaces inside the container.
 - Without additional configuration, the container is completely isolated from the host networking stack

The network scope is the domain of the driver which can be the local or swarm scope.
- Local scope drivers provide connectivity and network services (such as DNS or IPAM) within the scope of the host.
- Swarm scope drivers provide connectivity and network services across a swarm cluster.
- Swarm scope networks have the same network ID across the entire cluster while local scope networks have a unique network ID on each host.
