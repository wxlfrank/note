# Markdown

## how to include backtick in code

Wrapping inline code blocks in double backticks instead of single backticks allow a backtick to be used.

```markdown
`` ` ``
```