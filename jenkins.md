# Jenkins

## Local Docker Running
> Docker: [jenkins/jenkins](https://hub.docker.com/r/jenkins/jenkins/)

```bash
mkdir data
cat > data/log.properties <<EOF
handlers=java.util.logging.ConsoleHandler
jenkins.level=FINEST
java.util.logging.ConsoleHandler.level=FINEST
EOF
docker run --name myjenkins -p 8080:8080 -p 50000:50000 --env JAVA_OPTS="-Djava.util.logging.config.file=/var/jenkins_home/log.properties" -v `pwd`/data:/var/jenkins_home jenkins/jenkins:lts
```

## Jenkinsfile

- fileName: `Jenkinsfile`

### Main concept

- pipeline
  - agent
    - `checkout scm`:
      - `scm` is a special variable which instructs the `checkout` step to clone the specific revision which triggered this Pipeline run
    - allocates an executor and workspace for the Pipeline
  - stages
    - stage
      - standard stages: `Build`, `Test`, `Deploy`
      - steps
        - Globle variables
          - use without `def`
          - must first initialize
          - e.g., `Utils = load 'file_path'`
        - [environment variables](https://www.jenkins.io/doc/book/pipeline/jenkinsfile/#using-environment-variables)
          - only string type is allowed
          - BRANCH_NAME
          - JOB_NAME
        - clean workspace: `cleanWs()`
        - print to log: `echo 'message'`

## functions

- findFiles
  - must install `Pipeline Utility Steps`
  - similar functions: `readFile`, `writeFile`
- static function is not allowed
  
## library

### Load code directly

#### define library

- define function
- `return this;`

#### load and use library

- `def lib = load 'library location'`
- `lib.function(parameters)`