# Javascript

## Special Feature

### Destruc

## [Destructuring assignment](https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Operators/Destructuring_assignment)

- Array Destructuring
  - `[v1, v2, ...] = array`
  - `[v1, v2=default_value, ...] = array`
  - `[v1, ...v2] = array`
- Object Destructuring
  - `{v1, v2, ...} = object`
  - `{v1, v2=default_value, ...} = object`
  - `{v1:rename_v1, v2, ...} = object`
  - `{v1, ...v2} = object`

Objects in JavaScript are maps (dictionaries) from strings to values.

### Property

- A `(key, value) entry` in an object is called a `property`.

  - `The key of a property` is always `a text string`.
  - `The value of a property` can be any JavaScript value, including a function.
    - `Methods` are properties whose values are functions.

#### Three kinds of properties

1. Properties (or named data properties)
   1. Normal properties are storage locations for property values
2. Accessors (or named accessor properties)
   1. accessors allow you to compute the values of properties
3. Internal properties
   1. Exist only in the ECMAScript language specification.
   2. They are not directly acces‐ sible from JavaScript,
      1. there might be indirect ways of accessing them.
   3. The specification writes the keys of internal properties in brackets.
      1. `[[Prototype]]` holds the prototype of an object and is readable via `Object.getPrototypeOf()`.

#### Operation on properties

1. get `obj.property`
   1. call method `obj.method()`
2. set `obj.property = value`
3. delete `delete obj.property`
   1. after `obj.property = undefined`, the property still exists in the object
   2. `delete` affects only the direct properties of an object; its prototypes are not touched
   3. `delete` returns `false` if the property is an own property, but cannot be deleted. It returns true in all other cases.

### Object constructor

| Value                       | Result                              |
| --------------------------- | ----------------------------------- |
| (Called with no parameters) | {}                                  |
| undefined                   | {}                                  |
| null                        | {}                                  |
| A boolean bool              | new Boolean(bool)                   |
| A number num                | new Number(num)                     |
| A string str                | new String(str)                     |
| An object obj               | obj (unchanged, nothing to convert) |

### `this`

#### `this` in functions

1. `this === window` in sloppy mode
2. `this === undefined` in strict mode

#### `this` in methods

`this` refers to the object on which the method has been invoked

#### set `this`

1. `Function.prototype.call(thisValue, arg1?, arg2?, ...)`
2. `Function.prototype.apply(thisValue, argArray)`
3. `Function.prototype.bind(thisValue, arg1?, ..., argN?)`

### Prototype

![Prototype Chain](images/javascript-prototype-chain.png)

#### Inheritance

Every object can have another object as its `prototype`. Then the former object inherits all of its prototype’s properties.

- Whenever you access a property via obj, JavaScript starts the search for it in that object and continues with its prototype, the prototype’s prototype, and so on.
- In a prototype chain, a property in an object overrides a property with the same key in a “later” object: the former property is found first.
- setting a property affects only the first object in a prototype chain, whereas
- getting a property considers the complete chain .

#### Operations

- set object prototype `Object.create(proto, propDescObj?)`
- get object prototype `Object.getPrototypeOf(obj)`
- check whether one object is a prototype of another `Object.prototype.isPrototypeOf(obj)`
- Listing Own Property Keys
  - `Object.getOwnPropertyNames(obj)` returns the keys of all own properties of obj
  - `Object.keys(obj)` returns the keys of all enumerable own properties of obj
- Listing All Property Keys
  - `for («variable» in «object»)`
- Checking Whether a Property Exists
  - `propKey in obj` Returns true if obj has a property whose key is propKey. Inherited properties are included in this test.
  - `Object.prototype.hasOwnProperty(propKey)` Returns true if the receiver (this) has an own (noninherited) property whose key is propKey.

### Accessors

ECMAScript 5 lets you write methods whose invocations look like you are getting or setting a property. That means that a property is virtual and not storage space. (Getters and setters are inherited from prototypes)

1. define via an object literal

```javascript
var obj = {
  get foo() {
    return "getter";
  },
  set foo(value) {
    console.log("setter: " + value);
  }
};
```

２. define via property descriptor

```javascript
var obj = Object.create(
    Object.prototype, { // object with property descriptors
    foo: { // property descriptor
        get: function () {
            return 'getter';
        set: function (value) {
            console.log('setter: '+value);
        }
    }
}
```

### Constructor

A constructor is a function that is invoked via the `new` operator.

```javascript
function Person(name) {
  this.name = name;
}
//The object in Person.prototype becomes the prototype of all instances of Person.
Person.prototype.describe = function() {
  return "Person named " + this.name;
};
```

![Constructor Prototype](images/javascript-constructor-prototype.png)

Each constructor **C** has `a prototype property` that refers to an object **P**.

- **P** is the prototype of all the instances of **C**
  - given an instance of **C**, **i**, `Object.getPrototypeOf(i) === C.prototype`
- `C.prototype.constructor === C`
- The `instanceof` operator: `value instanceof Constr` determines whether value has been created by the constructor `Constr` or a `subconstructor`. It does so by checking whether `Constr.prototype` is in `the prototype chain of value`.
  - instanceof is always false for primitive values:

### Computed property names

Starting with ECMAScript 2015, the object initializer syntax also supports `computed property names`. That allows you to put an expression in brackets `[]`, that will be computed and used as the property name.

## Array

- `splice`: `splice(start, delete_items_count, add_items)`
  - `delete elements`
  - `add elements`
  - operation on original array
  - return deleted elements
- `slice`: `slice(start, end)`
  - select sub-array
  - return an copy of selection

### Spread syntax

`Spread syntax` allows an iterable such as an array expression or string to be expanded in places where

- zero or more arguments (for function calls) or elements (for array literals) are expected, or
- an object expression to be expanded in places where zero or more key-value pairs (for object literals) are expected.

```javascript
myFunction(...iterableObj);
[...iterableObj, "4", "five", 6];
let objClone = { ...obj };
```

- Arrays are maps from indices to values
- `Array.isArray(obj)` check if `obj` is an array

#### holes

- indices smaller than the length that are missing in the array
- operation ignore holes
  - forEach
  - every
  - some
  - map (preserve holes)
  - filter (remove holes)
  - for-in loop
- operation consider holes
  - join
  - sort
  - apply

## Event

### Attach event listener

1. general way
   1. `EventTarget.addEventListener(event_type:string, listener)`
2. specific `on-event` handlers
   1. the event handler is specified as an HTML attribute: `on${event_type}=listener(parameter)` where parameter can be
      1. the `this` keyword inside the handler is set to the DOM element on which the handler is registered
      2. `event` for all event handlers, except `onerror`. (event is accessible in the handler without needing to specify in the parameter list)
      3. `event`, `source`, `lineno`, `colno`, and `error` for the onerror event handler. Note that the event parameter actually contains the error message as string
   2. in Javascript: `domelement.on${event_type}=function (parameter){};`
3. for non-element objects, such as `window`, `document`, `XMLHttpReqeust`, etc,
   1. `obj.on${event_type}=function (parameter){};`
4. The return value from the handler determines if the event is canceled.

### load event

- `window.onload` (same as `$(document).ready(fn)` and `$(fn)` in jQuery) fires when the document's window is ready for presentation and
- `document.body.onload` fires when the body (`there is no document.onload in DOM API`) (built from the markup code within the document) is completed.
- `DOMContentLoaded` event is fired when the initial HTML document has been completely loaded and parsed, without waiting for stylesheets, images, and subframes to finish loading.
- A very different event `load` should be used only to detect a fully-loaded page.

#### Lifecycle events summary

- `DOMContentLoaded` event triggers on document when DOM is ready. We can apply JavaScript to elements at this stage.
  - All `inline scripts` and `scripts with defer` are already executed.
  - `Async scripts` may execute both before and after the event, depends on when they load.
  - `Images` and `other resources` may also still continue loading.
- `load` event on window triggers when the page and all resources are loaded. We rarely use it, because there’s usually no need to wait for so long.
- `beforeunload` event on window triggers when the user wants to leave the page. If it returns a string, the browser shows a question whether the user really wants to leave or not.
- `unload` event on window triggers when the user is finally leaving, in the handler we can only do simple things that do not involve delays or asking a user. Because of that limitation, it’s rarely used.
- `document.readyState` is the current state of the document, changes can be tracked in the readystatechange event:
  - `loading` – the document is loading.
  - `interactive` – the document is parsed, happens at about the same time as DOMContentLoaded, but before it.
  - `complete` – the document and resources are loaded, happens at about the same time as window.onload, but before it.

## DOM Structure

<p style="font-size:30px">
EventTarget&#8701;Node&#8701;Element&#8701;HTMLElement</p>

### Node

- The `textContent` property returns the text with spacing, but without inner element tags.

### Element

- The `innerHTM`L property returns the text, including all spacing and inner element tags.
- The `outerHTML` attribute of the Element DOM interface gets the serialized HTML fragment describing the element including its descendants.
- The `closest()` method returns the closest ancestor of the current element (or the current element itself) which matches the selectors given in parameter.

### HTMLElement

- The `innerText` property returns just the text, without spacing and inner element tags.

### HTMLCollection

Loop through HTMLCollection

```javascript
[].forEach.call(hc, visitor);
Array.prototype.forEach.call(hc, visitor);
```

## Attribute && DOM property

### Attribute

1. Attributes are **defined by HTML**, all definitions inside HTML tag are attributes.
2. The **type** of attributes is **always string**.

```html
<div id="test" class="button" custom-attr="1"></div>
```

```javascript
document.getElementById("test").attributes;
// return: [custom-attr="hello", class="button", id="test"]
```

All attributes are accessible by using the following methods:

- **elem.hasAttribute(name)** – checks for existence.
- **elem.getAttribute(name)** – gets the value.
- **elem.setAttribute(name, value)** – sets the value.
- **elem.removeAttribute(name)** – removes the attribute.
- **elem.attributes** - read all attributes as a collection of objects that belong to a built-in `Attr` class, with `name` and `value` properties.

### Property

```javascript
document.getElementById("test").foo = 1; // set property: foo to a number: 1
document.getElementById("test").foo; // get property, return number: 1
$("#test").prop("foo"); // read property using jQuery, return number: 1
$("#test").prop("foo", {
  age: 23,
  name: "John"
}); // set property foo to an object using jQuery
document.getElementById("test").foo.age; // return number: 23
document.getElementById("test").foo.name; // return string: "John"
```

1. Properties **belong to DOM**, the nature of DOM is an object in JavaScript
2. Non-custom attributes have 1:1 mapping onto properties, like: id, class, title, etc.
3. Non-custom property (attribute) changes when corresponding attribute (property) changes in most cases.
4. **Attribute which has a default value doesn't change** when corresponding property changes.

## Module

### export

- named export (0..\*)
  - export let, var, const, function, class
  - export {name1, name2, ...}
  - export {v1 as name1, ...}
  - export const {name1, name2: type} = o; // by use of destructured assignments
  - export \* from ...
  - export \* as name1 from ...
  - export {name1, name2, ...} from ...
  - export {import1 as name1, ...} from ...
- default export (0..1)
  - export default let, var, const, function, class
  - export {name1 as default, name2, ...}
  - export {default} from ...

### import

- import defaultExport from "module-name";
- import \* as name from "module-name";
- import { export1 as alias1, ... } from "module-name";
- import { export1 , export2, ... } from "module-name";
- import defaultExport, { export1 [ , [...] ] } from "module-name";
- import defaultExport, \* as name from "module-name";
- import "module-name";
- var promise = import("module-name");
- let module = await import('module-name');

### Two Environments

- browser
  - `<script type="module" src="main.mjs"></script>`
- node

#### Node Module systems

- CommonJS
  - used by node
  - four environment variable: **module**, **exports**, **require**, `global`
  - `module.exports` define extern interface
  - `require` load module (synchronously)

```javascript
// 定义模块math.js
var basicNum = 0;
function add(a, b) {
  return a + b;
}
module.exports = { //在这里写上需要向外暴露的函数、变量
  add: add,
  basicNum: basicNum
}

// 引用自定义的模块时，参数包含路径，可省略.js
var math = require('./math');
math.add(2, 5);

// 引用核心模块时，不需要带路径
var http = require('http');
http.createService(...).listen(3000);


```

- RequireJS (AMD (Asynchronous Module Definition)-based)
  - `require.config` specify module path
  - `define` specify module
  - `require` load moudle (asynchronously)

```javascript
/** 网页中引入require.js及main.js **/
<script src="js/require.js" data-main="js/main"></script>;

/** main.js 入口文件/主模块 **/
// 首先用config()指定各模块路径和引用名
require.config({
  baseUrl: "js/lib",
  paths: {
    jquery: "jquery.min", //实际路径为js/lib/jquery.min.js
    underscore: "underscore.min"
  }
});
// 执行基本操作
require(["jquery", "underscore"], function($, _) {
  // some code here
});

// 定义math.js模块
define(function() {
  var basicNum = 0;
  var add = function(x, y) {
    return x + y;
  };
  return {
    add: add,
    basicNum: basicNum
  };
});
// 定义一个依赖underscore.js的模块
define(["underscore"], function(_) {
  var classify = function(list) {
    _.countBy(list, function(num) {
      return num > 30 ? "old" : "young";
    });
  };
  return {
    classify: classify
  };
});

// 引用模块，将模块放在[]内
require(["jquery", "math"], function($, math) {
  var sum = math.add(10, 20);
  $("#sum").html(sum);
});
```

- CMD (CommonJS Module Definition)
  - difference with AMD
  - AMD 推崇依赖前置、提前执行
    - RequireJS 从 2.0 开始，也改成可以延迟执行
  - CMD 推崇依赖就近、延迟执行

```javascript
/** AMD写法 **/
define(["a", "b", "c", "d", "e", "f"], function(a, b, c, d, e, f) {
  // 等于在最前面声明并初始化了要用到的所有模块
  a.doSomething();
  if (false) {
    // 即便没用到某个模块 b，但 b 还是提前执行了
    b.doSomething();
  }
});

/** CMD写法 **/
define(function(require, exports, module) {
  var a = require("./a"); //在需要时申明
  a.doSomething();
  if (false) {
    var b = require("./b");
    b.doSomething();
  }
});

/** sea.js **/
// 定义模块 math.js
define(function(require, exports, module) {
  var $ = require("jquery.js");
  var add = function(a, b) {
    return a + b;
  };
  exports.add = add;
});
// 加载模块
seajs.use(["math.js"], function(math) {
  var sum = math.add(1 + 2);
});
```

- Webpack
  - webpack is a `static module bundler` for modern JavaScript applications. When webpack processes your application, it internally builds a dependency graph which maps every module your project needs and generates one or more bundles.
  - concepts
    - `entry`: An entry point indicates which module webpack should use to begin building out its internal dependency graph
      - By default its value is **./src/index.js**
    - `output` tells webpack where to emit the bundles it creates and how to name these files.
      - `./dist/main.js` for the main output file and to the `./dist` folder for any other generated file.
    - `Loaders` allow webpack to process other types of files (rather than javascript and JSON files) and convert them into valid modules that can be consumed by your application and added to the dependency graph.
      1. The `test` property identifies which file or files should be transformed.
      2. The `use` property indicates which loader should be used to do the transforming
    - `plugins` can be leveraged to perform a wider range of tasks like bundle optimization, asset management and injection of environment variables.
    - `mode`: `*development*`, `production` or `none`, enable webpack's built-in optimizations that correspond to each environment
    - `target`: `node`, `web`
- Babel
  - Babel is a tool that helps you write code in the latest version of JavaScript.
  - When your supported environments don't support certain features natively, Babel will help you compile those features down to a supported version.
- webpack-dev-server
  - `contentBase`: where to serve content from
  - `publicPath`: determine where the bundles should be served from, and takes precedence
