# Apache

## Prerequisite

- `URL`: address on the Web
  - `protocol`
  - `servername` (`hostname`)
  - `URL-path`
  - `query string`
- `request`
- `respond`
  - `status code`
  - `response body`

## IP and hostname

- `IP address`
  - `DNS`: resolve a servername to an IP address
  - More than one servername may point to the same IP address
  - more than one IP address can be attached to the same physical server.
  - `hosts file`
    - ***/etc/hosts*** or
    - ***C:\Windows\system32\drivers\etc\hosts***
  - `virtual hosts`

## Configuration
The server is configured by placing `configuration directives` in `configuration files`.
### Files
- `location`:***/usr/local/apache2/conf***
- `the main configuration file`:***httpd.conf***
- The configuration is frequently broken into multiple smaller files, for ease of management
- Changes to the main configuration files are only recognized by httpd when it is started or restarted.
#### MIME document file
The server reads a file containing `mime document types`;
the filename is set by the `TypesConfig` directive, and is `mime.types` by default.
### directives
A directive is a keyword followed by one or more `arguments` that set its value.
- Directives placed in the main configuration files apply to the entire server.
- you can scope your directives by placing them in `<Directory>`, `<DirectoryMatch>`, `<Files>`, `<FilesMatch>`, `<Location>`, and `<LocationMatch>` sections to change the configuration for only a part of the server
  -  These sections limit the application of the directives which they enclose to `particular filesystem locations` or `URLs`.
  - They can also be nested, allowing for very fine grained configuration
  - Directives can also be scoped by placing them inside `<VirtualHost>` sections, so that they will only apply to requests for a particular website.
#### global setting
Directives in the configuration file, outside of any `<Directory>`, `<Location>`, `<VirtualHost>` or other section.
#### local setting
Directives inside section, e.g, `<Directory>`
#### Custom setting
configuration files in `.htaccess` files located in the content directories
- `.htaccess` files are primarily for people who do not have access to the main server configuration file(s)
- the name `.htaccess` can be customized in the `AccessFileName` directive
- Directives placed in .htaccess files apply to the directory where you place the file, and all sub-directories.
-  `.htaccess` files are read on every request
-  The server administrator can control what directives may be placed in .htaccess files by configuring the `AllowOverride` directive in the main configuration files.

## Directives
- `Include file-path|directory-path|wildcard`: load other configuration file
  - `file-path`: The file path specified may be *an absolute path*, or may be *relative* to the `ServerRoot` directory.
  - `directory-path`:  Apache httpd will read all files in that directory and any subdirectory.
  - The Include directive will fail with an error if a wildcard expression does not match any file.
  - The `IncludeOptional` directive can be used if non-matching wildcards should be ignored.
- `ServerRoot directory-path`: sets the directory in which the server lives
  - `default`: */usr/local/apache*
  - The default location of ServerRoot may be modified by using the `--prefix` argument to `configure`
  - use `httpd -d serverroot` to configure
- `Typesconfig file-path` directive sets the location of the media types configuration file.
  - File-path is relative to the ServerRoot
  - This file sets the default list of mappings from filename extensions to content types
  - `default`: *conf/mime.types*
  - can be overridden by `AddType` directives as needed
  - You should not edit the mime.types file, because it may be replaced when you upgrade your server.
- `AddType media-type extension+`
#### content negotiation
Content negotiation is the mechanism that is used for serving different representations of a resource at the same URI, so that the user agent can specify which is best suited for the user
- two mechanisms
  1. Specific HTTP headers by the client (`server-driven negotiation` or `proactive negotiation`), which is the standard way of negotiating a specific kind of resource.
  2. The `300 (Multiple Choices)` or `406 (Not Acceptable)` HTTP response codes by the server (`agent-driven negotiation` or `reactive negotiation`), that are used as `fallback mechanisms`.
- The HTTP/1.1 standard defines list of the standard headers that start server-driven negotiation
  - `Accept`: comma-separated lists of MIME types  that the agent is willing to process, each combined with a `quality factor`, a parameter indicating the relative degree of preference between the different MIME types.
  - `Accept-Charset`: indicates to the server what kinds of character encodings are understood by the user-agent
  - `Accept-Encoding`: defines the acceptable content-encoding (supported compressions).
    - The value is a `q-factor list` (e.g.: br, gzip;q=0.8) that indicates the priority of the encoding values.
    - The default value identity is at the lowest priority (unless otherwise declared).
  - `Accept-Language`: indicate the language preference of the user. It is a list of values with `quality factors` (like: "de, en;q=0.7"). A default value is often set according the language of the graphical interface of the user agent

- `<Directory directory-path> ... </Directory>`

## syntax

### Directive
- one directive per line
  - The backslash "\" may be used as the last character on a line to indicate that the directive continues onto the next line.
  There must be no other characters or white space between the backslash and the end of the line.
- Arguments to directives are separated by whitespace.
  - If an argument contains spaces, you must enclose that argument in quotes.
- Directives in the configuration files are case-insensitive, but arguments to directives are `often` case sensitive.
### comment
- Lines that begin with the hash character "#" are considered comments, and are ignored.
  - Comments may not be included on the same line as a configuration directive.??
- White space occurring before a directive is ignored
  - you may indent directives for clarity.
- Blank lines are also ignored.
### Variable
```xml
<IfDefine [!]parameter-name>
  Define parameter-name [parameter-value]
</IfDefine>
```
- The values of variables defined with the `Define`
  - `Define` is equivalent to passing the `-D` argument to `httpd`
- shell environment variables can be used in configuration file lines using the syntax ${VAR}.
  - Only shell environment variables defined before the server is started can be used in expansions. Environment variables defined in the configuration file itself, for example with `SetEnv`, take effect too late to be used for expansions in the configuration file.
- Variables defined with `Define` take precedence over shell environment variables.
- Variable names may not contain colon ":" characters, to avoid clashes with `RewriteMap`'s syntax.
  -  If "VAR" is found, the value of that variable is substituted into that spot in the configuration file line, and processing continues as if that text were found directly in the configuration file.
  - otherwise, the characters ${VAR} are left unchanged, and a warning is logged

### restriction
The maximum length of a line in normal configuration files, after `variable substitution` and `joining any continued lines`, is approximately `16 MiB`.
  - In .htaccess files, the maximum length is 8190 characters
### Test syntax
- `apachectl configtest`
- command line with `-t` option
- use `mod_info`'s `-DDUMP_CONFIG` to dump the configuration with
  - all included files and environment variables resolved and
  - all comments and non-matching `<IfDefine>` and `<IfModule>` sections removed.

### Module
`httpd` is a modular server shipped with only the most basic functionality
- Extended features are available through modules which can be loaded into httpd
- By default, a base set of modules is included in the server at compile-time.
- If the server is compiled to use `dynamically loaded modules`, then modules can be compiled separately and added at any time using the `LoadModule` directive.
- Otherwise, httpd must be recompiled to add or remove modules.
- Configuration directives may be included conditional on a presence of a particular module by enclosing them in an `<IfModule>` block.
- use `-l` to show what modules are compiled into the server
- use `-M` to show what modules are loaded dynamically

## Web site content
### static content
- e.g. HTML files, image files, CSS files, and other files
- `DocumentRoot` directive specifies where in your filesystem you should place these files.
  - This directive is either set globally, or per virtual host
  - Typically, a document called `index.html` will be served ***when a directory is requested without a file name being specified***.
### dynamic content
- Dynamic content is anything that is generated at request time, and may change from one request to another.
- Various `handlers` are available to generate content
- `Third-party modules`, e.g., mod_php,  may be used to write code that does a variety of things.
## Log files
The location of the error log is defined by the `ErrorLog` directive, which may be set globally, or per virtual host.
Entries in the error log tell you
  - what went wrong, and when.
  - how to fix it.
Each error log message contains `an error code`, which you can search for online for even more detailed descriptions of how to address the problem.
You can also configure your error log to contain `a log ID` which you can then correlate to an access log entry,
- `transaction`
- `error condition`
