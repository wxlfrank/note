# Spring

<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />
## Spring properties

Various properties can be specified

- inside your **application.properties** file,
- inside your **application.yml** file, or
- as **command line switches**.

## Test

- @SpringBootTest annotation tells Spring Boot to look for a main configuration class (one with @SpringBootApplication for instance), and use that to start a Spring application context
  - the application context is cached in between tests
  - starting the application once
  - control the cache using the @DirtiesContext annotation.
  - use of webEnvironment=RANDOM_PORT to start the server with a random port and injection of the port with @LocalServerPort
  - Spring Boot has provided a **TestRestTemplate**???
- @Autowired annotation is interpreted by the Spring and the controller is injected before the test methods are run

### Alternative

Another useful approach is to not start the server at all, but test only the layer below that

- Spring handles the incoming HTTP request and hands it off to your controller.
- Almost the full stack is used, and your code will be called exactly the same way as if it was processing a real HTTP request, but without the cost of starting the server
- Use Spring’s **MockMvc**
- injected **MockMvc** by using the @AutoConfigureMockMvc annotation

### Spring Boot

@SpringBootApplication combines

- @Configuration Designates a class as a configuration class using Spring’s Java-based configuration
- @ComponentScan Enables component-scanning
  - the web controller classes and other components will be
    - automatically discovered
    - registered as beans in the Spring application context.
- @EnableAutoConfiguration enables the magic of Spring Boot auto-configuration

#### application.properties

the application.properties file comes in handy for

- fine-grained configuration of the stuff that Spring Boot automatically configures
- specify properties used by application code.

### Cross Site Request Forgery (CSRF)

Cross site means one of the following units are different: protocol, host and port

By default the **CookieCsrfTokenRepository** will

- write to a cookie named **XSRF-TOKEN** and
- read it from
  - a header named **X-XSRF-TOKEN** or
  - the HTTP parameter **\_csrf**

**CookieHttpOnly** determines if the browser should allow the cookie to be accessed by client-side javascript.

- The default is true, which means the cookie will only be passed to http requests and is not made available to script on the page.

## Spring properties

Various properties can be specified

- inside your **application.properties** file,
- inside your **application.yml** file, or
- as **command line switches**.

## session tracking

HTTP is a **_stateless protocol_**

### How session tracking works in Java

1. a browser requests a JSP/servlet from the web server, which passes the request to the servlet engine
2. the servlet engine checks if the request includes an ID for the Java session

- if not, the servlet engine creates
  - a unique ID for the session
  - a **_session object_** that stores the data for the session

3. the web server uses the session ID to relate each browser request to the session object, even though the server drops the HTTP connection after returning each response

Two solution store session ID

1. By default API uses a **cookie** to store the session ID within the browser

- this is an extension of HTTP protocol
- when the next request is made, the cookie is added to the request

2. **URL encoding** to rewite the URL in order to include the session ID

- a security hole that allow session hijacking
- cause a site to malfunction if the user bookmakrs a page including a session ID

Two types of cookies

1. **persistent cookies** stored on user PC
2. **per-session cookies** deleted when the session ends

```java
request.getSession() // return the HttpSession object associated with the request. Created one if no session associated.
session.setAttribute(name, object)
session.getAttribute(name)
session.removeAttribute(name)
session.getAttributeNames()
session.getId() // return a string for the unique Java session identifier that the servlet engine generates for each session
session.isNew() // return true if the client does not know about the session or if the client chooses not to join the session (if the session relies upon cookies and the browser does not accept cookies)
session.setMaxInactiveInterval(seconds) // default 30 minutes, -1 for a session that wont end until the client closes
session.invalidate() // invalidates the session and unbinds any objects bound to it
```

### thread-safe access

```java
final Object lock = request.getSession().getId().intern();
synchronized(lock){
 actions
}
```

### Cookie

A cookie is a name/value pair stored in a browser

- server generates a cookie while client save the cookie and sends it back the server every time it accesses a page from the server

```java
Cookie(name, String_value)
cookie.setMaxAge(maxAgeInSeconds)
cookie.setPath(path) // "/" if allow the entire application to access the cookie
cookie.getName()
cookie.getValue()

response.addCookie(cookie)
request.getCookies()
```

### Cors

#### Enable Cors

- Method specific

```Java
@CrossOrigin(origins = "http://localhost:9000")
```

- Glolabl setting

```java
@Bean
   public WebMvcConfigurer corsConfigurer() {
       return new WebMvcConfigurerAdapter() {
           @Override
           public void addCorsMappings(CorsRegistry registry) {
               registry.addMapping("/greeting-javaconfig").allowedOrigins("http://localhost:9000");
           }
       };
   }
```

## Exception

Three approaches for handling exception

1. Per Exception
   - When an exception annotated with `@ResponseStatus` annotation is thrown from a controller method, and not handled elsewhere, it will automatically cause the appropriate HTTP response to be returned with the specified status-code.
2. Per controller

   - annotate methods with `@ExceptionHandler` to specifically **handle exceptions thrown by request handling (@RequestMapping) methods in the same controller**. Such methods can:

     1. Handle exceptions with the @ResponseStatus annotation (typically predefined exceptions that you didn’t write)
     2. Redirect the user to a dedicated error view
     3. Build a totally custom error response

3. Glolablly

   - A controller advice, a class annotated with `@ControllerAdvice`, allows you to use exactly the same exception handling techniques but **apply them across the whole application**, not just to an individual controller.

     1. Exception handling methods annotated with `@ExceptionHandler`.

     2. Model enhancement methods (for adding additional data to the model) annotated with `@ModelAttribute`. Note that **these attributes are not available to the exception handling views**.

     3. **Binder initialization methods** (used for configuring form-handling) annotated with `@InitBinder`.

Default:

1. `SimpleMappingExceptionResolver` in **Spring MockMvc**
2. in Spring Boot
   1. map `/error` to view `error` which is mapped to file **error.html**, **error.jsp** according to `ViewResolver`(e.g., `InternalResourceViewResolver`)
   2. if no view-resolver mapping for **/error**, a “**Whitelabel Error Page**” (a minimal page with just the HTTP status information and any error details, such as the message from an uncaught exception)

### HandlerExceptionResolver

Any Spring bean declared in the `DispatcherServlet`’s application context that implements `HandlerExceptionResolver` will be used to **intercept and process any exception raised in the MVC system and not handled by a Controller**.

```java
public interface HandlerExceptionResolver {
    ModelAndView resolveException(HttpServletRequest request,
            HttpServletResponse response, Object handler, Exception ex);
}
```

The `handler` refers to the controller that generated the exception

- Note that `@Controller` instances are **only one type of handler** supported by Spring MVC.
- For example: `HttpInvokerExporter` and the `WebFlow Executor` are also types of handler.

Implementation

1. `ExceptionHandlerExceptionResolver` matches uncaught exceptions against suitable `@ExceptionHandler` methods on both the handler (controller) and on any controller-advices.
2. `ResponseStatusExceptionResolver` looks for uncaught exceptions annotated by `@ResponseStatus` (as described in Section 1)
3. `DefaultHand`lerExceptionResolver converts standard Spring exceptions and converts them to HTTP Status Codes (I have not mentioned this above as **it is internal to Spring MVC**).

These are **chained and processed in the order listed** - internally Spring creates a dedicated bean (the `HandlerExceptionResolverComposite`) to do this.

## Logging

Spring Boot uses **Commons Logging** for all internal logging but leaves the underlying log implementation open.

- Default configurations are provided for **Java Util Logging**, **Log4J2**, and **Logback**.

- By default, if you use the **“Starters”**, **Logback** is used for logging.

### Log Format

- `Date and Time`: Millisecond precision and easily sortable.
- `Log Level`: **ERROR**, **WARN**, **INFO**, **DEBUG**, or **TRACE**.
- `Process ID`.
- A `---` separator to distinguish the start of actual log messages.
- `Thread name`: Enclosed in square brackets (may be truncated for console output).
- `Logger name`: This is usually the source class name (often abbreviated).
- `The log message`.

### File output

- By default, Spring Boot logs **only** to the `console` and does not write log files.
- If you want to write log files **in addition to** the console output, you need to set a `logging.file` or `logging.path` property
  - If we use **both** `logging.path` and `logging.file` then `logging.path` property will be **ignored**.
- Log files rotate when they reach **10 MB** and, as with console output, **ERROR-level, WARN-level, and INFO-level messages are logged by default**.
- Size limits can be changed using the `logging.file.max-size` property.
- Previously rotated files are **archived indefinitely** unless the `logging.file.max-history` property has been set.
