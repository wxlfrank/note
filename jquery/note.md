# jQuery

## Objects
- The $.ajax() method returns the jqXHR object, which is a superset of the XMLHTTPRequest object

## Global

```javascript
.ajaxSend(function(event: Event, jqXHR: jqXHR, ajaxOptions: Object){

})
```
- Whenever an Ajax request is about to be sent, jQuery triggers **the ajaxSend event**.
  - Any and all handlers that have been registered with the .ajaxSend() method are executed at this time.
  - function is executed before an Ajax request is sent.
