# CSS
<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

## Statements

### `CSS rules`

![](images/html-css-syntax-ruleset.png)
![](images/html-css-syntax-declarations-block.png)
![](images/html-css-syntax-declaration.png)

### `At-rules`

At-rules are used in CSS to convey

- metadata, (`@charset` and `@import` )
- conditional information (@media or @document, also called `nested statements`), or
- other descriptive information (`@font-face`).

They

- start with an at sign (`@`),
- followed by `an identifier` to say what kind of rule it is,
- then `a syntax block` of some kind, ending with a semi-colon (;)

### `Nested statements`

Nested statements are

- a specific subset of at-rule,
- the syntax is a nested block of CSS rules that will only be applied to the document if a specific condition is matched:
  - The `@media` at-rule content is applied only if the device which runs the browser matches the expressed condition;
  - the `@supports` at-rule content is applied only if the browser actually supports the tested feature;
  - the `@document` at-rule content is applied only if the current page matches some conditions.

## CSS Units

### Absolute Lengths

- used if the output medium is known, such as for print layout
- Pixels (px) are relative to the viewing device.
  - For low-dpi devices, 1px is one device pixel (dot) of the display.
  - For printers and high resolution screens 1px implies multiple device pixels.

| Unit | Description                  |
| ---- | ---------------------------- |
| cm   | centimeters                  |
| mm   | millimeters                  |
| in   | inches (1in = 96px = 2.54cm) |
| px * | pixels (1px = 1/96th of 1in) |
| pt   | points (1pt = 1/72 of 1in)   |
| pc   | picas (1pc = 12 pt)          |

#### ppi & px

- `ppi` (pixel per inch) denotes how many physical pixels each inch
- `px` denotes a fixed logical unit where each inch contains 96px
- each px contains maybe more than one pixels

### Relative Lengths

| Unit | Description                                                                               |
| ---- | ----------------------------------------------------------------------------------------- |
| em   | Relative to the font-size of the element (2em means 2 times the size of the current font) |
| rem  | Relative to font-size of the root element                                                 |
| %    | Relative to the parent element                                                            |
| ex   | Relative to the x-height of the current font (rarely used)                                |
| ch   | Relative to width of the "0" (zero)                                                       |
| vw   | Relative to 1% of the width of the viewport*                                              |
| vh   | Relative to 1% of the height of the viewport*                                             |
| vmin | Relative to 1% of viewport's* smaller dimension                                           |
| vmax | Relative to 1% of viewport's* larger dimension                                            |

- Viewport = the browser window size. If the viewport is 50cm wide, 1vw = 0.5cm.                                                         |

## layout

### Flexbox

`The Flexible Box Module`, usually referred to as `flexbox`

- was designed as a **_one-dimensional_** layout model
- could offer `space distribution` between items in an interface and powerful `alignment` capabilities.

### The two axes

- `The main axis`
  - is defined by the `flex-direction` property
    - row
    - row-reverse
    - column
    - column-reverse
- `the cross axis`
  - perpendicular to the main axis

### The flex container

- set the value of the area's container's `display` property to `flex` or `inline-flex`
  - The `align-items` property
    - align the items on the cross axis.
    - `stretch`, `flex-start`, `flex-end`, `center`
  - The `justify-content` property
    - align the items on the main axis
    - `flex-start`, `flex-end`, `center`, `space-around`, `space-between`, `space-evenly`
  - the direct children of that container become `flex items`
    - Items display in a row (the flex-direction property's default is row).
    - The items start from the start edge of the main axis.
    - The items _**do not stretch**_ on the main dimension, but can shrink.
    - The items _**will stretch**_ to fill the size of the cross axis.
    - The `flex-basis` property is set to `auto`.
      - defines the size of that item in terms of the space it leaves as available space
      - If the items don’t have a size then the content's size is used as the `flex-basis`.
    - The `flex-wrap` property is set to `nowrap`.
    - The `flex-grow` property is set to 0
      - With the `flex-grow` property set to a positive integer, flex items can grow along the main axis from their `flex-basis`. This will cause the item to stretch and take up any available space on that axis, or a proportion of the available space if other items are allowed to grow too.
    - The `flex-shrink` property is set to 1
      - controls how it is taken away. If we do not have enough space in the container to lay out our items and `flex-shrink` is set to a positive integer the item can become smaller than the `flex-basis`

## Bootstrap

### Overview

- `Containers` provide a means to _**center and horizontally pad**_ your site’s contents. Use `.container` for a `responsive pixel width` or `.container-fluid` for `width: 100%` across all viewport and device sizes.
- `Rows` are wrappers for columns. Each column has `horizontal padding` (called a `gutter`) for _**controlling the space between them**_. This padding is then counteracted on the rows with negative margins. This way, all the content in your columns is visually aligned down the left side.
- In a grid layout, content must be placed within `columns` and _**only columns may be immediate children of rows**_.
- Thanks to flexbox, grid columns _**without a specified width**_ will automatically layout as equal width columns. For example, four instances of .col-sm will each automatically be 25% wide from the small breakpoint and up. See the auto-layout columns section for more examples.
- Column classes indicate the number of columns you’d like to use out of the possible 12 per row. So, if you want three equal-width columns across, you can use .col-4.
- Column widths are set in percentages, so they’re always **_fluid and sized relative to their parent element_**.
- Columns have horizontal padding to create the gutters between individual columns, however, you can remove the margin from rows and padding from columns with `.no-gutters` on the `.row`.
- To make the grid responsive, there are five grid breakpoints, one for each responsive breakpoint: all breakpoints (extra small), small, medium, large, and extra large.
- Grid breakpoints are based on `minimum width media queries`, meaning they apply to that one breakpoint and all those above it (e.g., .col-sm-4 applies to small, medium, large, and extra large devices, but not the first xs breakpoint).
- You can use predefined grid classes (like .col-4) or Sass mixins for more semantic markup.


|                     | Extra small <576px                   | Small ≥576px | Medium ≥768px | Large ≥992px | Extra large ≥1200px |
| ------------------- | ------------------------------------ | ------------ | ------------- | ------------ | ------------------- |
| Max container width | None (auto)                          | 540px        | 720px         | 960px        | 1140px              |
| Class prefix        | .col-                                | .col-sm-     | .col-md-      | .col-lg-     | .col-xl-            |
| # of columns        | 12                                   |
| Gutter width        | 30px (15px on each side of a column) |
| Nestable            | Yes                                  |
| Column ordering     | Yes                                  |

```css
.col{
    // flexible
    flex: 1 1 0;
    max-width: 100%;
}
.col-auto{
    // flexible without grow
    flex: 0 0 auto;
    width: auto;
    max-width: 100%;
}
.col-n{
    // fixed without grow
    flex: 0 0 (n*100/12)%;
    max-width: (n*100/12)%;
}

@media(min-width: with){
    .col-(sm, md, lg, xl){
       // flexible
        flex: 1 1 0;
        max-width: 100%;
    }
    }
    .col-(sm, md, lg, xl)-auto{
        // flexible without grow
        flex: 0 0 auto;
        width: auto;
        max-width: 100%;
    }
    .col-(sm, md, lg, xl)-n{
        // fixed without grow
        flex: 0 0 (n*100/12)%;
        max-width: (n*100/12)%;
    }
}

.col-*{
    // for all
    width: 100%;
    position: relative;
    padding: 0 15px;
}
```