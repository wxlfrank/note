# C++

## Environment

### Windows 10

- enviroment:
  - PATH: C:\Program Files (x86)\Microsoft Visual Studio\2019\Professional\VC\Tools\MSVC\14.26.28801\bin\Hostx64\x64
  - INCLUDE: C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\INCLUDE;C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\ATLMFC\INCLUDE;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\ucrt;C:\Program Files (x86)\Windows Kits\NETFXSDK\4.6.1\include\um;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\shared;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\um;C:\Program Files (x86)\Windows Kits\10\include\10.0.18362.0\winrt;
  - LIB: C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\LIB\amd64;C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\ATLMFC\LIB\amd64;C:\Program Files (x86)\Windows Kits\10\lib\10.0.18362.0\ucrt\x64;C:\Program Files (x86)\Windows Kits\NETFXSDK\4.6.1\lib\um\x64;C:\Program Files (x86)\Windows Kits\10\lib\10.0.18362.0\um\x64;
- compiler: cl
- stdio.h: C:\Program Files (x86)\Windows Kits\10\Include\10.0.18362.0\ucrt
- libcmt.h: C:\Program Files (x86)\Microsoft Visual Studio 14.0\VC\lib\amd64

## Terms

- `Translation unit`: Each C source file, together with all the header files included in it, makes up a trans‐ lation unit.

## Concepts

### variable

### function

### Common properties

#### Storage class

> The storage class of a variable determines whether the item has one of the following lifetime
>
> - global (static, c term)
>   - exists and has a value throughout the execution of the program
> - local (automatic, c term)
>   - are allocated new storage each time execution control passes to the block in which they are defined
>   - When execution returns, the variables no longer have meaningful values
>
> All functions have global lifetimes.

## Keywords

### const

1. constant value: `const int i = 10;`
   1. subject to type checking
   2. can be used in place of constant expressions

```cpp
const int maxarray = 255;
char store_char[maxarray];  // allowed in C++; not allowed in c
```

2. const point

```cpp
char *mybuf = 0, *yourbuf;
char *const aptr = mybuf;
*aptr = 'a';   // OK
aptr = yourbuf;   // C3892
```

3. point to a constant value
   1. A pointer to a variable declared as const can be assigned only to a pointer that is also declared as const

```cpp
const char *mybuf = "test";
char *yourbuf = "test2";
const char *bptr = mybuf;   // Pointer to constant data
*bptr = 'a';   // Error
```

4. const member function
   1. "read-only" function that does not modify the object for which it is called
   2. cannot modify any **non-static data members** or call any member functions that aren't constant

## Declaration

- structure, union, emumeration tag, emumeration member
- object identifier, function identifier
  - storage class specifier
    - syntax
      - extern
      - static
      - \_Thread_local
      - auto
      - register
      - \_Thread_local extern
      - \_Thread_local static
    - semantics
      - storage duration: object property
        - automatic (auto, register)
        - static (static object inside function, extern object inside function, object outside function)
        - allocated
      - linkage: identifier property
        - external (object outside function, function, extern function, extern object)
        - internal (static function, static object outside function)
        - none (static object inside function, function parameters, label names, structure tag, typedef names)
    - constraints
      - auto is only permissible in object declarations within a function
      - programs must not use the address operator on objects declared with the register specifier
      - function identifiers can only use extern or static
      - function parameters can only user regsiter
      - all declarations within function blocks are definitions unless they contain the storage class specifier extern.
  - type
    - type specifier
      - inline
      - \_Noreturn
    - type qualifier
      - const
      - volatile (reread the object’s value each time it is used, usually for hardware)
      - restrict (apply only to object point type)
      - \_Atomic
      - \_Atomic(type_name)
      - \_Alignas
- typedef declaration
- \_Static_assert declaration

### c++ difference

- External linkage
  - free function: defined at global or namespace scope
  - non-const global variables
- internal linkage
  - static global name
  - const objects
  - constexpr objects
  - typedefs
  - static objects in namespace scope
