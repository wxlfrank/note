# Google Web Toolkit (GWT)

## Project Structure

- `host page`: Any HTML page can include a GWT application via a `SCRIPT` tag
  - to load `GWT modules` stored on a web server as a set of JavaScript and related files in order to run the modules
  - typical: not include any visible HTML body content at all
  - customize: allow the GWT module to selectively insert widgets into specific places in an HTML page
    - use the `id` attribute in HTML tags to specify a unique identifier that GWT code will use to attach widgets to that HTML element
    - attach widgets using the method `RootPanel.get(id)`
- folder structure
  - src: production Java source
    - root package
      - client: translated into JavaScript
      - server
      - `module-name.gwt.xml`: module configuration file
      - public
        - Static resources that are loaded programmatically by GWT code.
        - Files in the public directory are copied into the same directory as the GWT compiler output.
        - load this file programmatically using this URL: `GWT.getModuleBaseForStaticFiles() + filename`
  - war: web app; contains static resources as well as compiled output
    - `module-name`
      - where the GWT compiler writes output and files on the public path
      - fully-qualified module name: `package-name.module-name`
      - `module-name.nocache.js`: the script that must be loaded from the host page to load the GWT module into the page
    - `WEB-INF`: all non-public resources
      - `web.xml`: web app and servlet configuration
      - `classes`: Java compiled class files to implement server-side functionality
      - `lib`: library dependencies
        - `gwt-serlvet.jar`: If you have any servlets using `GWT RPC`
  - test (optional): JUnit test code

## Module

A module bundles together all the configuration settings that a GWT project needs in XML file:

- an entry point application class name
  - class extends `EntryPoint`
  - optional if the module is designed as library
  - When a module is loaded, every entry point class is instantiated and its `EntryPoint.onModuleLoad()` method gets called.
- inherited modules
- source path entries
  - files on the path are candidates to be translated into JavaScript
  - When module inherit other modules, their source paths are combined so that each module will have access to the translatable source it requires.
  - default is `client`
- public path entries
  - static resources referenced by your GWT module
  - When you compile your application into JavaScript, all the files that can be found on your `public` path are copied to the module’s output directory
  - referencing public resources from a Module XML File, just use the relative path within the public folder
- deferred binding rules, including property providers and class generators

Two ways to load modules

1. Compile each module separately and include each module with a separate `script` tag in your HTML host page.
2. Create a top level module XML definition that includes all the modules you want to include. Compile the top level module to create a single set of JavaScript output.
   - recommended (seperate modules cause redundancy)

## AJAX Communication

- three options
  - Java Server
    - GWT RPC
      - a mechanism for passing `Java objects` to and from a server over `standard HTTP`
      - make calls to Java servlets and let GWT take care of low-level details like object serialization
  - Non-Java Server or JSON/XML data format
  - cross-site request for JSONP
- asynchronous calls
  - do not block while waiting for the call to return.
  - The code following the call executes immediately.
  - When the call completes, the callback method you specified when you made the call will execute.
