# HTML

<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

## Basic

- It is best to get into the habit of **writing your folder and file names lowercase with no spaces and with words separated by dashes**, at least until you know what you're doing
- recommended structure
  - index.html
  - images
  - scripts
  - styles
- rules for path
  - To link to a target file in the same directory as the invoking HTML file, just use the filename, e.g. my-image.jpg.
  - To reference a file in a subdirectory, write the directory name in front of the path, plus a forward slash, e.g. subdirectory/my-image.jpg.
  - To link to a target file in the directory above the invoking HTML file, write two dots.
- HTML element
  ![HTML Element](images/html-htmlelement.png)
  - opening tag
  - content
  - closing tag
  - element

## DOM

## Event Flow

![Event Flow](images/html-eventflow.svg)

Event objects are dispatched to an `event target`. But before dispatch can begin, the event object’s `propagation path` must first be determined.

The propagation path

- is an ordered list of `current event targets` through which the event passes.
- `The event target` plus its ancestors form a branch in the DOM tree
  - The last item in the list is `the event target` **(the innermost element over which an event originated)**, and
  - the preceding items in the list are referred to as `the target’s ancestors`, with
  - the immediately preceding item as `the target’s parent`.
- This propagation is the process of calling all the listeners for the given event type **(multiple listeners may be registered for a given event type)**, attached to the nodes on the branch.
- the branch determination is static, that is, it is established at the initial dispatch of the event.

Once the propagation path has been determined, the event object passes through one or more event phases. There are three event phases: `capture phase`, `target phase` and `bubble phase`.

- A phase will be skipped if
  - it is not supported, or
  - the event object’s propagation has been stopped.
    - if `the bubbles attribute` is set to false, the bubble phase will be skipped,
    - if `stopPropagation()` has been called prior to the dispatch, all phases will be skipped.

### The capture phase

The event object propagates through the target’s ancestors from the Window to the target’s parent.
only `the capturer listeners` are called, namely, those listeners that were registered using a value of `true` for the third parameter of `addEventListener`

### The target phase:

The event object arrives at the event object’s event target. This phase is also known as the at-target phase.

- If the event type indicates that the event doesn’t bubble, then the event object will halt after completion of this phase.
- all the listeners registered on the event target will be invoked, regardless of the value of their capture flag

### The bubble phase:

The event object propagates through the target’s ancestors in reverse order, starting with the target’s parent and ending with the Window.
During the event bubbling phase only the non-capturers will be called. That is, only the listeners registered with a value of false for the third parameter of addEventListener()

- `focus`, `blur`, `load` and some others, don’t bubble up
- if an event bubbles by reading `the bubbles attribute` of the event object.

### Event properties and methods

- `target` references `the event target`.
- `currentTarget` is the node on which the running listener was registered on.
  - This is the same value of the listener invocation context, i.e, the value referenced by the `this` keyword.
- `eventPhase` indicates the current phase
- `stopPropagation` means that all the listeners registered on the nodes on the propagation path that follow the current target will not be called.
  - Instead, all the other remaining listeners attached on the current target will still receive the event.
- `stopImmediatePropagation` throws the brakes on straight away, preventing even the siblings of the current listener from receiving the event.
- `preventDefault` stops all related default actions of an event object

### Dispatch

Events are typically dispatched by the implementation as a result of `a user action`, in response to

- the completion of a task, or
- to signal progress during asynchronous activity (such as a network request).
- to control the behavior that the implementation may take next
- undo an action that the implementation already took

Events in this category are said to be **cancelable**

- the behavior they cancel is called their **default action**.
- **Cancelable event objects** can be associated with one or more **default actions**.
- To cancel an event, call the **preventDefault()** method.

Default actions are usually performed after the event dispatch has been completed,

- in exceptional cases they may also be performed immediately before the event is dispatched.

```
The default action associated with the click event on <input type="checkbox"> elements
toggles the checked IDL (Interface Definition Language) attribute value of that element.
If the click event’s default action is cancelled,
then the value is restored to its former state.
```

- When an event is canceled, then the conditional default actions associated with the event is skipped
  - if the default actions are carried out before the dispatch, their effect is undone
- Whether an event object is cancelable is indicated by the cancelable attribute.
- Calling preventDefault() stops all related default actions of an event object.
- The defaultPrevented attribute indicates whether an event has already been canceled
- If the DOM application itself initiated the dispatch, then the return value of the dispatchEvent() method indicates whether the event object was cancelled.

### EventTarget

EventTarget is an interface implemented by objects that can receive events and may have listeners for them.

Examples:

- Element
- Document
- Window
- XMLHttpRequest
- AudioNode
- AudioContext

Methods

1. addEventListener:

```typescript
expose class EventTarget{
  addEventListener (
    type: string, # case-sensitive, representing event type
    listener: EventListener | Function,
    options?: {
      capature: boolean,
      once: boolean,
      passive: boolean
    },
    userCapture?: boolean
  ) // Register an event handler of a specific event type on the EventTarget.
  removeEventListener(
    type: string,
    listener: EventListener | Function,
    options?: {
      capature: boolean,
      once: boolean,
      passive: boolean
    },
    userCapture?: boolean) // Removes an event listener from the EventTarget.
    dispatchEvent(event: Event) // Dispatches an Event at the specified EventTarget
    // The return value is false if event is cancelable and at least one of the event handlers which handled this event called Event.preventDefault(). Otherwise it returns true.
}

expose class Node extends EventTarget{
  const baseURI: DOMString; // representing the base URL. The concept of base URL changes from one language to another;
  // in HTML, it corresponds to the protocol, the domain name and the directory structure, that is all until the last '/'
  const childNodes: NodeList; // a live NodeList containing all the children of this node. NodeList being live means that if the children of the Node change, the NodeList object is automatically updated.
  const firstChild: Node;
  const lastChild: Node;
  const previousSibling: Node;
  const nextSibling: Node;
  const isConnected: boolean; // whether or not the Node is connected (directly or indirectly) to the context object, e.g. the Document object in the case of the normal DOM, or the ShadowRoot in the case of a shadow DOM.
  const nodeName: DOMString; // The structure of the name will differ with the node type
  {
    Attr : The value of Attr.name,
    CDATASection : "#cdata-section",
    Comment : "#comment",
    Document : "#document",
    DocumentFragment : "#document-fragment",
    DocumentType : The value of DocumentType.name,
    Element : The value of Element.tagName,
    Entity : The entity name,
    EntityReference : The name of entity reference,
    Notation : The notation name,
    ProcessingInstruction : The value of ProcessingInstruction.target,
    Text : "#text"
  }
  const nodeType: unsigned short; // the type of the node
  {
      ELEMENT_NODE : 1,
      ATTRIBUTE_NODE : 2,
      TEXT_NODE : 3,
      CDATA_SECTION_NODE : 4,
      ENTITY_REFERENCE_NODE : 5,
      ENTITY_NODE : 6,
      PROCESSING_INSTRUCTION_NODE : 7,
      COMMENT_NODE : 8,
      DOCUMENT_NODE : 9,
      DOCUMENT_TYPE_NODE : 10,
      DOCUMENT_FRAGMENT_NODE : 11,
      NOTATION_NODE : 12
  }
  nodeValue: // the value of the current node
  {
    CDATASection : content of the CDATA Section,
    Comment : content of the comment,
    Document : null,
    DocumentFragment : null,
    DocumentType : null,
    Element : null,
    NamedNodeMap : null,
    EntityReference : null,
    Notation : null,
    ProcessingInstruction : entire content excluding the target,
    Text : content of the text node
  }
  const ownerDocument: Document; // Returns the Document that this node belongs to. If the node is itself a document, returns null.
  const parentNode: Node; // Returns a Node that is the parent of this node. If there is no such node, like if this node is the top of the tree or if doesn't participate in a tree, this property returns null.
  const parentElement: Element; // Returns an Element that is the parent of this node. If the node has no parent, or if that parent is not an Element, this property returns null.
  textContent: string; // represents the text content of a node and its descendants.
}

expose interface EventListener{
  handleEvent(event: Event);
}

expose Event{
  const bubbles: boolean; // whether the event bubbles up through the DOM
  cancelBubble: boolean; // alias to Event.stopPropagation()
                         // Setting its value to true before returning from an event handler prevents propagation of the event.
  const concelable: boolean;
  const composed: boolean; // whether the event can bubble across the boundary between the shadow DOM and the regular DOM.
  const currentTarget: EventTarget; // (???)
                          // Identifies the current target for the event, as the event traverses the DOM.
                          // It always refers to the element to which the event handler has been attached
                          // In comparison, event.target identifies the element on which the event occurred.
  const defaultPrevented: boolean; // Indicates whether or not event.preventDefault() has been called on the event
  const eventPhase: ? // Indicates which phase of the event flow is being processed
  const target: EventTarget // A reference to the target to which the event was originally dispatched.
  const timestamp: number; // milliseconds The time at which the event was created
  const type: string; // the name of the event (case-insensitive).
  const isTrusted: boolean; // Indicates whether or not the event was initiated
                            // by the browser (after a user click for instance) or
                            // by a script (using an event creation method, like event.initEvent).
  createEvent(type:string):Event // must be initialized before use.
  composePath(): any[]// returns the event’s path which is an array of the objects on which listeners will be invoked.
                      //This does not include nodes in shadow trees if the shadow root was created with its ShadowRoot.mode closed.
  preventDefault() // tells the user agent that if the event does not get explicitly handled, its default action should not be taken as it normally would be.
  stopImmediatePropagation() // no other listener will be called. Neither those attached on the same element, nor those attached on elements which will be traversed later
  stopPropagation() // Stops the propagation of events further along in the DOM.
}
```

## History

- `path`: **window.history**
- `type`: **History**
  - an interface for manipulating **the browser session history**
    - pages visited in the tab
    - frame that the current page is loaded in
  - `method`
    - `go(int)`: load a specific page from session history, identified by its relative position to the current page
    - `back`: == `go(-1)`
    - `forward`: == `go(1)`
    - `pushState(a state, a title, a URL)`
      - **state object** 
        - a JavaScript object which is associated with **the new history entry created by pushState()**. 
        - A `popstate` event is dispatched to the window every time **the active history entry changes**. If the history entry being activated was created by a call to pushState or affected by a call to replaceState, the popstate event's state property contains a copy of the history entry's state object.
        - less than 640k, use **sessionStorage** or **localStorage** if necessary 
      - changes **the referrer** used in the HTTP header for XMLHttpRequest objects created after you change the state. 
      - The referrer will be the URL of the document whose window is `this` at the time of creation of the XMLHttpRequest object.
    - replaceState() 
      - operates exactly like history.pushState() except that replaceState() **modifies the current history entry** instead of **creating a new one**
  - `property`
    - `length`: the number of pages in the history stack
- `value`:
  - read-only reference
