for file in *;
do
	t=${file// - /-}
	t=$(tr -s ' ' <<< $t)	
	t=${t// /-}
	if [ "$file" != "$t" ]; then
		echo $t
		mv "$file" $t
	fi
done
