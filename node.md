# Node

## NPM

### Global vs Local installation

1. `globally` —- This drops modules in `{prefix}/lib/node_modules`, and puts executable files in `{prefix}/bin`, where `{prefix}` is usually something like `/usr/local`. It also installs man pages in `{prefix}/share/man`, if they’re supplied.
2. `locally` —- This installs your package in the current working directory. Node modules go in `./node_modules`, executables go in `./node_modules/.bin/`, and _man pages aren’t installed at all_.

## Module

### export

- `exports.key = object | function`
- `module.exports = object`

When a file is run directly from Node.js, `require.main` is set to its module. That means that it is possible to determine whether a file has been run directly by testing `require.main === module`.

For a file `foo.js`, this will be true if run via `node foo.js`, but false if run by `require('./foo')`.

Because module provides a `filename` property (normally equivalent to `__filename`), **_the entry point of the current application_** can be obtained by checking `require.main.filename`.
