# Syntactically Awesome Style Sheets (SASS)

## Two Syntax

### SCSS

- file extension `.scss`
- superset of `CSS`

### The Indented Syntax

- original syntax
- file extension `.sass`
- support all the same feature as `SCSS`
  - use `indentation` instead of `curly braces` and `semicolons`

## Varaible

- `$var`
- assignment: `$var: var-value;`
  - `!default` flag
    - This assigns a value to a variable only if that variable isn’t defined or its value is null.
    - Otherwise, the existing value will be used.
- scope
  - `global`: Variables declared at the top level of a stylesheet
    - they can be accessed anywhere after they’ve been declared—even in another stylesheet
  - `local`: Variables declared in blocks (curly braces in SCSS or indented code in Sass)
    - only be accessed within the block they were declared
  - Variables declared in `flow control rules` have special scoping rules
    - they don’t shadow variables at the same level as the flow control rule.
    - Instead, they just assign to those variables.

## Data Type

### Map

- construct: `(key-exp: value-exp, ...)`
  - all maps count as lists
    - Every map counts as a list that contains a two-element list for each key/value pair.
    - (1: 2, 3: 4) counts as (1 2, 3 4)
  - `immutable`
    - the contents of a map value never changes
    - map functions all return new maps rather than modifying the originals
- get: `map-get(map-var, key-exp)`
- merge: `map-merge(map-var, map-var)`
