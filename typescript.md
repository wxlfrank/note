# TypeScript

JavaScript `code modules` are a concept introduced in Gecko 1.9

> Gecko is the name of the `layout engine` developed by the Mozilla Project.
> It was originally named `NGLayout`.
> Gecko's function is to `render web content`, such as HTML, CSS, XUL (`XML User Interface Language`), JavaScript, and render it on the user's screen or print it.
> In XUL-based applications Gecko also renders the application's user interface.

Modules can be used

1. for sharing code between different `privileged scopes`
2. to create global JavaScript `singletons` that previously required using `JavaScript XPCOM objects`

A JavaScript code module is simply `some JavaScript code located in a registered location`.
The module is loaded into `a specific JavaScript scope`, such as `XUL script` or `JavaScript XPCOM script`, using `Components.utils.import()` or `Components.utils["import"]()`.

## create a JavaScript code module

- Any JavaScript item named in `EXPORTED_SYMBOLS` will be exported from the module and injected into the importing scope
- need a URL to import a code module
  - `chrome:()`, `resource:`, `file:`
- `Components.utils.import()` caches loaded modules and subsequent imports do not reload a new version of the module, but instead use the previously cached version.
- a given module will be shared when imported multiple times.
- Any modifications to data, objects, or functions will be available in any scope that has imported the module
- Each scope that imports a module receives a `by-value copy of the exported symbols in that module`.

## Typescript

- Modules are executed within their own scope, not in the global scope
  - variables, functions, classes, declared in a module are not visible outside the module unless they are explicitly exported using one of the `export forms`.
  - to consume a variable, function, class, interface, etc. exported from a different module, it has to be imported using one of the `import forms`
- Modules are declarative; the relationships between modules are specified in terms of imports and exports at the file level
- Modules import one another using `a module loader`.
  - At runtime the module loader is responsible for locating and executing all dependencies of a module before executing it.
  - Well-known modules loaders used in JavaScript are
    - the `CommonJS module loader for Node.js` and
    - `require.js for Web applications`.
- any file containing `a top-level import or export` is considered a module
  - a file without any top-level import or export declarations is treated as a script whose contents are available in the global scope (and therefore to modules as well).

### Export

- Any `declaration` (such as a variable, function, class, type alias, or interface) can be exported by adding the export keyword.
- `Export statements` are handy when exports need to be renamed for consumers

```typescript
class ZipCodeValidator implements StringValidator {
  isAcceptable(s: string) {
    return s.length === 5 && numberRegexp.test(s);
  }
}
export { ZipCodeValidator };
export { ZipCodeValidator as mainValidator };
```

- Often modules extend other modules, and partially expose some of their features
  - a module can wrap one or more modules and combine all their exports using `export * from "module"`

### Import

- Import a single export from a module

```typescript
import { ZipCodeValidator } from "./ZipCodeValidator";

let myValidator = new ZipCodeValidator();
```

- rename import

```typescript
import { ZipCodeValidator as ZCV } from "./ZipCodeValidator";
let myValidator = new ZCV();
```

- Import the entire module into a single variable, and use it to access the module exports

```typescript
import * as validator from "./ZipCodeValidator";
let myValidator = new validator.ZipCodeValidator();
```

- Import a module for side-effects only

some modules set up some global state that can be used by other modules.

Each module can optionally export a `default export`.

- Default exports are marked with the keyword default; and
- there can only be one `default export` per module.
- `default export`s are imported using a different import form.
  - `import name from "module"`

Both CommonJS and AMD generally have the concept of `an exports object` which contains all exports from a module.

- support replacing the exports object with a custom single object
- The `export =` syntax specifies a single object that is exported from the module
- When exporting a module using `export =`, TypeScript-specific `import module = require("module")` must be used to import the module.

Depending on the module target specified during compilation, the compiler will generate appropriate code for

- Node.js (CommonJS),
- require.js (AMD),
- UMD, SystemJS, or ECMAScript 2015 native modules (ES6) module-loading systems.

## Type

### Type alias

- `type type-alias = type-format`
- creates **a new name** to refer to a type

### Generic Type

- `<T>`

### Type casting

- `<T>`

## Function

### Parameters

- every parameter is assumed to be `required`
  - The compiler expects that, the number of arguments given to a function has to match the number of parameters the function expects.
- In `Javascript`, every parameter is `optional`

#### Optional Parameters

- `?` at the end of a parameter
- Any optional parameters must follow required parameters

#### Default Parameters

- `=value` at the end of a parameter
- Default parameters that come after all required parameters are treated as optional
- default parameters **don’t** need to occur after required parameters
  - users need to explicitly pass `undefined` to get the default initialized value

#### Rest Parameters

- `...parameter:type[]`
- treated as **a boundless number of optional parameters**

## Interface

type-checking focuses on `the shape` that values have (called `duck typing` or `structural subtyping`). In TypeScript, interfaces fill the role of `naming these types`, and are a powerful way of defining `contracts within your code` as well as `contracts with code outside of your project`.

```typescript
function printLabel(labelledObj: { label: string }) {
  console.log(labelledObj.label);
}

let myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);

// can be interfaced as

interface LabelledValue {
  label: string;
}

function printLabel(labelledObj: LabelledValue) {
  console.log(labelledObj.label);
}

let myObj = { size: 10, label: "Size 10 Object" };
printLabel(myObj);
```

- type-checker does not require that these properties come in any sort of order, only that `the properties the interface requires` are present and have `the required type`.
- `optional properties` are denoted by a `?` at the end of `the property name` in the declaration.
- `Readonly properties` should only be modifiable when an object is first created, denoted by `readonly` before the name of the property
  - `Variables` use `const` whereas `properties` use `readonly`
- `Excess property checking`
  - `Object literals` get special treatment and undergo excess property checking when `assigning them to other variables`, or `passing them as arguments`.
  - If an object literal has any properties that the “target type” doesn’t have, you’ll get an error.
- `function type` defined as `a call signature`, a function declaration with `only` the parameter list and return type given.
  - Each parameter in the parameter list requires both name and type
  - Function parameters are checked one at a time, with the type in each corresponding parameter position checked against each other.
- `Indexable types` have `an index signature` that describes the types we can use to index into the object, along with the corresponding return types when indexing
  - two types of supported index signatures: string and number.
  - It is possible to support both types of indexers, but the type returned from a numeric indexer must be a subtype of the type returned from the string indexer.
    - when indexing with a number, JavaScript will actually convert that to a string before indexing into an object
  - `string index signatures` enforces that all properties match their return type

## tscompiler

A typescript project contains a `tsconfig.json` file that specifies the configuration of the project.

### compile options

- `rootDir`: the root directory of input files
  - common root directory is computed from the list of input files
  - `rootDirs`: List of root folders whose combined content represent the structure of the project at runtime (only used in `tsconfig.json`)
- `watch` implementation of the compiler relies on `fs.watch` and `fs.watchFile` provided by node

  - `fs.watch` uses file system events to notify the changes in the file/directory. But this is OS dependent and the notification is not completely reliable and does not work as expected on many OS. Also there could be limit on number of watches that can be created, eg. linux, and we could exhaust it pretty quickly with programs that include large number of files. But because this uses file system events, there is not much CPU cycle involved. Compiler typically uses fs.watch to watch directories (eg. source directories included by config file, directories in which module resolution failed etc) These can handle the missing precision in notifying about the changes. But recursive watching is supported on only Windows and OSX. That means we need something to replace the recursive nature on other OS.

  - `fs.watchFile` uses `polling` and thus involves CPU cycles. But this is the most reliable mechanism to get the update on the status of file/directory. Compiler typically uses fs.watchFile to watch source files, config files and missing files (missing file references) that means the CPU usage depends on number of files in the program.

| S.NO | INTERRUPT                                                                                                 | POLLING                                                                                                                                          |
| ---- | --------------------------------------------------------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------ |
| 1.   | In interrupt, the device notices the CPU that it requires its attention.                                  | Whereas, in polling, CPU steadily checks whether the device needs attention.                                                                     |
| 2.   | An interrupt is not a protocol, its a hardware mechanism.                                                 | Whereas it isn’t a hardware mechanism, its a protocol.                                                                                           |
| 3.   | In interrupt, the device is serviced by interrupt handler.                                                | While in polling, the device is serviced by CPU.                                                                                                 |
| 4.   | Interrupt can take place at any time.                                                                     | Whereas CPU steadily ballots the device at regular or proper interval.                                                                           |
| 5.   | In interrupt, interrupt request line is used as indication for indicating that device requires servicing. | While in polling, `Command ready bit` is used as indication for indicating that device requires servicing.                                       |
| 6.   | In interrupts, processor is simply disturbed once any device interrupts it.                               | On the opposite hand, in polling, processor waste countless processor cycles by repeatedly checking the command-ready little bit of each device. |
