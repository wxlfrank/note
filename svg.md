<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

# SVG

## <svg\>

- All SVG elements, even shapes, can have child elements. 
- default unit: **px**
  - Pixels (px) are relative to the viewing device. 
  - For low-dpi devices, 1px is one device pixel (dot) of the display. 
  - For printers and high resolution screens 1px implies multiple device pixels.
- dimensions
  - attributes: `width`, `height`
  - css: **width**, **height**
- an SVG image consists of a series of **shapes** that are drawn to the screen. 
  - Everything else builds upon the shapes.
  - The shapes are drawn on top of one another, **in the order they appear in the code**.
  - rectangle
    - <rect\>
    - `x`: position goes from **the left of the page** to **the right** in increasing value
    - `y`: position goes from **the top of the page** to **the bottom**
    - `width`
    - `height`
    - `fill`: how the interior of the rectangle should be filled in
    - `stroke`, `stroke-width`: define the **color** and **thickness** of the lines that draw the rectangle’s edges
  - circle
    - <circle\>
    - `cx`, `cy`: **coordinates** for **the center point** of the circle
    - `r`: **radius**
    - `fill`: how the interior of the rectangle should be filled in
      - solid colors
      - gradients
        - <radialGradient\>
          - `fx`, `fy`: off-center effect (the x,y  coordinate of the start circle of the radial gradient)
        - <linearGraident\>
          - `spreadMethod`, `gradientTransform`: control the **angle**, **scale**, and **repetition** of the gradient
        - <stop\> elements that define **the color transition**
      - patterns
    - `stroke`, `stroke-width`: define the **color** and **thickness** of the lines that draw the rectangle’s edges
    - `visibility`: **visible**, **hidden**
  - text
    - <text\>
    - The words (or other characters) to be drawn are the child content of the element
    - The text is positioned (by default) in a single line around an **anchor point**; the anchor is set with `x` and `y` attributes
    - The text is painted using the `fill` and `stroke` properties
  - group
    - a logical structure to the shapes in your graphic
    - **styles** applied to a group will be **inherited by the shapes within it**
    - <g\>
  - reuse
    - <use\>
    - reuse graphics that you’ve already defined once in your file, to draw the same geometry in multiple places.
    - `x`, `y`: shift the reused graphic horizontally or vertically so that the x-axis, y-axis from the original graphic now lines up with the specified x-position, y-position 
  - definition
    - <defs\>
    - contains definitions of SVG content for later use
    - Children of a <defs\> element are never drawn directly


### shapes

#### common

- style
  - `fill`: **black**
  - `stroke`: **none**
  - `stroke-width`: **1px**
- position and lengths
  - default: **0**
  - coordinates are (by default) measured from the top-left corner of the graphic
  - When numbers are given without units, these **user coordinate** lengths are (again, by default) equivalent to CSS `px` units.

#### line

- <line\>
- represents a straight segment connecting two points
- `x1`, `y1`, `x2`, `y2`

#### rectangle

- <rect\>
- `x`, `y`, `width`, `height`
- `rx` and `ry` : create rounded corners on the rectangle

#### polygon & polyline

- <polygon\>
- <polyline\>
- `points`
- use straight segments
- absolute coordinates

#### path

- <path\>
  - `d`: the `path instruction` set, which can be thought of as a separate coding language within SVG
    - a sequence of **points** and **single letter instructions** that give the directions for drawing the shape
    - Each piece is defined by the `type` of `drawing command`, indicated by **a letter code**, and **a series of numerical parameters**.
    - The path instructions are written as a single string of data, and are followed in order. 
    - Each path segment starts from the end point of the previous statement; 
    - the command letter is followed by coordinates that specify the new end point and
      - for the more complex curves—the route to get there. 
      - Each letter is an abbreviation for a specific command
        - `M`: move-to
        - `L`: line-to
        - `C`: curve-to
      - uppercase for **absolute coordinates**
      - lowercase for **relative coordinates**

<svg width="100" height="100" viewbox="-10,-10,30,30" style="background-color: transparent">
<!-- <path d="M3,10 L10,0 L17,10 L10,20 L3,10" fill="red"/> -->
<path id="axes" fill="none" stroke="royalBlue" stroke-width="0.3" d="M-5, 0H25 M 0,-5V25"/>
<path id="major-grid" fill="none" stroke="cornflowerBlue" stroke-width="0.15" d="M-5, 5H25 M 5,-5V25 M-5,10H25 M10,-5V25 M-5,15H25 M15,-5V25 M-5,20H25 M20,-5V25"/>
<path d="M3,10 L10,0 17,10 10,20 Z M9,11 L10,18 10,10 15,10 11,9 10,2 10,10 5,10 Z"  fill="red"/>
<path d="M0,0 Q 10, 0 10,10 Q 0,10 0,0 Z" fill="royalBlue"/>
</svg>

## View and Scale

### ViewBox

`viewBox` is a list of four numbers:

- **min-x**, **min-y**: the minimum coordinate, in each direction, that should be included in the viewport region
  - The minimum coordinates implicitly define where the origin of the coordinate system—the (0,0) point—will be.
- **width**, **height**: the total number of coordinates, in each direction, that should fit within the viewport
- The four numbers can be separated by **whitespace** or by **commas**
