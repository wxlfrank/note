<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

# Database

## Transaction

Transaction

- is the foundation for `concurrent execution` and `recovery from system failure` in a DBMS.
- is defined as `any one execution` of a user program in a DBMS
  - seen by the DBMS as a series of `actions`, which `read` and `write` of `database objects`.
    - each transaction must specify as its `final action` either `commit` (i.e., complete successfully) or `abort` (i.e., terminate and undo all the actions carried out thus far).
  - differs from an execution of a program outside the DBMS
- ACID properties
  - `atomic`: Either all actions are carried out or none are.
  - `consistency`: Each transaction, run by itself with no concurrent execution of other transactions, must preserve the consistency of the database.
    - Users are responsible for ensuring transaction consistency.
      - the user who submits a transaction must ensure that, when run to completion by itself against a 'consistent' database instance, the transaction will leave the database in a 'consistent' state.
  - `isolation`: Users should be able to understand a transaction without considering the effect of other concurrently executing transactions, even if the DBMS interleaves (交叉) the actions of several transactions for performance reasons.
    - even though actions of several transactions might be interleaved, the net effect is identical to executing all transactions one after the other in some serial order.
  - `durability`: Once the DBMS informs the user that a transaction has been successfully completed, its effects should persist even if the system crashes before all its changes are reflected on disk

### Partial Transaction (Incomplete Transaction)

- 3 reasons
  - a transaction may encounter an unexpected situation (for example, read an unexpected data value or be unable to access some disk) and decide to abort (i.e., terminate itself)
  - be aborted, or terminated unsuccessfully, by the DBMS because some anormaly arises during execution
  - the system crashes (e.g., because the power supply is interrupted)
- A DBMS ensures transaction atomicity by undoing the actions of incomplete transactions.
  - the DBMS maintains a record, called the `log` of all writes to the database.
  - The log is also used to ensure durability:
    - If the system crashes before the changes made by a completed transaction are written to disk, the log is used to remember and restore these changes when the system restarts.
- The DBMS component that ensures `atomicity and durability`, called the `recovery manager`

### CONCURRENT EXECUTION OF TRANSACTIONS

- A `schedule` is a list of actions (reading, writing, aborting, or committing) from a set of transactions, and the order in which two actions of a transaction T appear in a schedule must be the same as the order in which they appear in T.
  - Intuitively, a schedule represents an actual or potential execution sequence
- A `schedule` that contains either an abort or a commit for each transaction whose actions are listed in it is called a `complete schedule`.
  - If the actions of different transactions are not interleaved, i.e., transactions are executed, from start to finish, one by one, we call the schedule a `serial schedule`
- The DBMS interleaves the actions of different transactions to improve performance, but not all interleavings should be allowed.
- A `serializable schedule` over a set S of committed transactions is a schedule whose effect on any consistent database instance is guaranteed to be identical to that of some complete serial schedule over S

### Anomalies Due to Interleaved Execution

- Reading Uncommitted Data (WR Conflicts) (`dirty read`)
- Unrepeatable Reads (RW Conflicts)
- Overwriting Uncommitted Data (WW Conflicts)(`lost update`)

A `serializable schedule` over a set S of transactions is a schedule whose effect on any consistent database instance is guaranteed to be identical to that of some complete serial schedule over the set of committed transactions in S.

In a `recoverable schedule`, transactions commit only after (and if!) all transactions whose changes they read commit. - If transactions read only the changes of committed transactions, not only is the schedule recoverable, but also aborting a transaction can be accomplished without cascading the abort to other transactions. Such a schedule is said to `avoid cascading aborts`.

### Concurrency control

A DBMS must be able to ensure that

- only serializable, recoverable schedules are allowed and that
- no actions of committed transactions are lost while undoing aborted transactions.

### locking protocol

A `lock` is a small bookkeeping object associated with a database object.

A `locking protocol` is a set of rules to be followed by each transaction (and enforced by the DBMS) to ensure that, even though actions of several transactions might be interleaved, the net effect is identical to executing all transactions in some serial order.

Strict Two-Phase Locking (Strict 2PL)

- If a transaction T wants to read (respectively, modify) an object, it first requests a `shared` (respectively, `exclusive`) `lock` on the object.
  - a transaction that has an exclusive lock can also read the object; an additional shared lock is not required.
  - A transaction that requests a lock is suspended until the DBMS is able to grant it the requested lock.
  - The DBMS keeps track of the locks it has granted and ensures that if a transaction holds an exclusive lock on an object, no other transaction holds a shared or exclusive lock on the same object.
- All locks held by a transaction are released when the transaction is completed.
