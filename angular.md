# Angular
<link rel="stylesheet" type="text/css" media="all" href="markdown.css" />

## Property Binding

- `[${view_element_property}]="${template_expression}"`
  - `canonical form`: `bind-${view_element_property}="${template_expression}"`
  - Remember the brackets except all of the following are true:
    - The target property accepts a string value.
    - The string is a fixed value that you can bake into the template.
    - This initial value never changes.
- set `a property of a view element` to `the value of a template expression`
  - set `an element property` to `a component property value`

```html
<img [src]="heroImageUrl"> // binding the src property of an image element to a component's heroImageUrl property:
```

- Element properties may be the more common targets,  but Angular looks first to see if the name is `a property of a known directive`

```html
<div [ngClass]="classes">[ngClass] binding to the classes property</div>
```


### one-way data binding

- it flows a value in one direction, from `a component's data property` into `a target element property`.
  - you cannot read a property of the target element
    - solution:
      - `ViewChild`
        - get `the first element` or `the directive` matching the selector from the content DOM. If the content DOM changes, and a new child matches the selector, the property will be updated.
        - View queries are set before the `ngAfterViewInit` callback is called.
      - `ContentChild`
        - Content queries are set before the `ngAfterContentInit` callback is called.
  - you cannot call a method of the target element
    - solution: `event binding`

## Component

### Lifecycle

After creating a component/directive by calling its constructor, Angular calls the lifecycle hook methods in the following sequence at specific moments:
|Hook|Purpose and Timing|
|---|---|
|`ngOnChanges()`|Respond when Angular (re)sets `data-bound input properties`. The method receives a `SimpleChanges` object of current and previous property values. Called `before ngOnInit()` and `whenever one or more data-bound input properties change`.|
|`ngOnInit()`|Initialize the directive/component after Angular first displays the data-bound properties and sets the directive/component's input properties. `Called once`, after the first ngOnChanges().|
|`ngDoCheck()`|Detect and act upon changes that Angular can't or won't detect on its own. `Called during every change detection run`, `immediately after` ngOnChanges() and ngOnInit().|
|`ngAfterContentInit()`|Respond after Angular projects `external content` into the component's view / the view that a directive is in. `Called once after the first ngDoCheck()`.|
|`ngAfterContentChecked()`|Respond after Angular checks the content projected into the directive/component. `Called after the ngAfterContentInit() and every subsequent ngDoCheck()`.|
|`ngAfterViewInit()`|Respond after Angular initializes `the component's views` and `child views` / the view that a directive is in. `Called once after the first ngAfterContentChecked()`.|
|`ngAfterViewChecked()`|Respond after Angular checks the component's views and child views / the view that a directive is in. `Called after the ngAfterViewInit() and every subsequent ngAfterContentChecked()`.|
|`ngOnDestroy()`|Cleanup just before Angular destroys the directive/component. `Unsubscribe Observables and detach event handlers to avoid memory leaks`. Called just before Angular destroys the directive/component.|

### Retrieve View

- get host view
  - `private host: ElementRef`
  - inject host `ElementRef`
    - get dom element: `ElementRef.nativeElement`
- get child view
  - `@ViewChild`
  - `@ViewChildren`

## Change detection

`Change detection` is the mechanism designed to `track changes` in `an application state` and `render` `the updated state` on the screen. It ensures that the user interface always stays in sync with the internal state of the program.

- the process of `rendering` takes the internal state of the program and projects it into something we can see on the screen.

### In Angular

When the compiler analyzes the template, 

- it identifies properties of a component that are associated with DOM elements. For each such association, 
  - the compiler creates a binding in the form of instructions. 
    - A binding is the core part of change detection in Angular. It 
      - defines an association between a component’s property (usually wrapped in some expression) and the DOM element property.
  - Once bindings are created, Angular no longer works with the template. The change detection mechanism executes instructions that process bindings. 
    - The job of these instructions is to 
      - check if the value of an expression with a component property has changed and 
      - perform DOM updates if necessary.

```language
[className]="'star ' + ((ctx.rating > 0) ? 'solid' : 'outline')"
the rating property in the template is bound to the className property through the expression
```

1. run change detection manually
```javascript
class RatingWidget {
    constructor(changeDetector) {
        this.cd = changeDetector;
    }

    handleClick(event) {
        this.rating = Number(event.target.dataset.value);
        this.cd.detectChanges();
    };
}
```
2. rely on the framework to trigger change detection automatically
```javascript
class RatingWidget {
    handleClick(event) {
        this.rating = Number(event.target.dataset.value);
    };
}
```

### Component, View

```typescript
@Component({
    selector: 'my-app',
    template: `
        <h3>
            Change detection is triggered at:
            <span [textContent]="time | date:'hh:mm:ss:SSS'"></span>
        </h3>
        <button (click)="0">Trigger Change Detection</button>
    `
})
export class AppComponent {
    get time() {
        return Date.now();
    }
}
```
Angular creates the DOM nodes to render the contents of the template on the screen, it needs a place to store the references to those DOM nodes. For that purpose, internally there’s a data structure known as `View`. It’s also used to store 
- the reference to the component instance and 
- the previous values of binding expressions. 
- There’s a one to one relationship between a component and a view. 

![Component View](images/angular-component-view.png)

As the compiler analyzes the template, it identifies `properties of the DOM elements` that may need to be updated during change detection. For each such property, the compiler creates a `binding`. The binding defines `the property name to update` and `the expression that Angular uses to obtain a new value`.
- A `viewDefinition` defines actual bindings for template elements and the properties to update. The expression used for a binding is placed in the `updateRenderer` function.

![Binding](images/angular-angular-bind.png)

When Angular checks a view, it simply 
 
- runs over all bindings generated for a view by the compiler. 
- evaluates expressions and compares their result to the values stored in the `oldValues` array on the view. 
  - That’s where the name dirty checking comes from. 
- If it detects the difference, it updates the DOM property relevant to the binding. And it also needs to put the new value into the `oldValues` array on the view.  
- Once Angular is done checking the current component, it repeats exactly the same steps for child components.


After each change detection cycle, in the development mode, Angular
- **synchronously** runs another check to ensure that expressions produce the same values as during the preceding change detection run. 
- This check is not part of the original change detection cycle. 
- It runs after the check is finished for the entire tree of components and performs exactly the same steps. 
  - However, this time, as Angular detects the difference, it doesn’t update the DOM. Instead, it throws the ExpressionChangedAfterItHasBeenCheckedError.
  
## Template

### Template expression operators

- pipe operator `|`
- safe navigation operator `?`
- non-null assertion operator `!`
  - enforce `strict null checking` with the `--strictNullChecks` flag
  - typed variables disallow null and undefined by default.
  - The type checker throws an error if you leave a variable unassigned or try to assign null or undefined to a variable whose type disallows null and undefined.
  - The type checker also throws an error if it can't determine whether a variable will be null or undefined at runtime. 

## Directives

Three kinds of directives

- `Components`—directives with a `template`.
- `Structural directives`—change the DOM layout by adding and removing DOM elements, e.g., `NgFor`, `NgIf`
- `Attribute directives`—change the appearance or behavior of an `element`, `component`, or another directive, e.g., `NgStyle`.


### Structural Directives

- `Function`: shape or reshape the DOM's structure, typically by adding, removing, or manipulating elements.
- `Usage`: applied to a `host element`
  - One structural directive per host element
- `Syntax`: preceded with an asterisk `*` (Angular desugars this notation into a marked-up `<ng-template>` that surrounds the host element and its descendents)



The Angular `<ng-template>` is

- an Angular element for rendering HTML. It is 
- never displayed directly. 
  - In fact, before rendering the view, Angular replaces the `<ng-template` and its contents with a comment.

The Angular `<ng-container>` is

- a grouping element that doesn't interfere with styles or layout because Angular doesn't put it in the DOM.
- is a syntax element recognized by the Angular parser. 
- not a directive, component, class, or interface.

```
A directive class is spelled in UpperCamelCase (NgIf). 

A directive's attribute name is spelled in lowerCamelCase (ngIf). 
```

#### `NgIf`

When a condition is false, NgIf

- removes its host element from the DOM
- detaches it from DOM events (the attachments that it made)
- detaches the component from Angular change detection, and destroys it
- The component and DOM nodes can be garbage-collected and free up memory.

When you use `[style.display]` to hide/show

- The component stays attached to its DOM element.
- It keeps listening to events. 
- Angular keeps checking for changes that could affect data bindings.

```html
  <div *ngFor="let hero of heroes; let i=index; let odd=odd; trackBy: trackById" [class.odd]="odd">
    ({{i}}) {{hero.name}}
  </div>
  After desugar
  <ng-template ngFor let-hero [ngForOf]="heroes" let-i="index" let-odd="odd" [ngForTrackBy]="trackById">
    <div [class.odd]="odd">({{i}}) {{hero.name}}</div>
  </ng-template>
```

#### `NgFor`

```html
  <div *ngFor="let hero of heroes; let i=index; let odd=odd; trackBy: trackById" [class.odd]="odd">
    ({{i}}) {{hero.name}}
  </div>
  After desugar
  <ng-template ngFor let-hero [ngForOf]="heroes" let-i="index" let-odd="odd" [ngForTrackBy]="trackById">
    <div [class.odd]="odd">({{i}}) {{hero.name}}</div>
  </ng-template>
```

#### `NgSwitch`

```html
<div [ngSwitch]="hero?.emotion">
  <app-happy-hero    *ngSwitchCase="'happy'"    [hero]="hero"></app-happy-hero>
  <app-sad-hero      *ngSwitchCase="'sad'"      [hero]="hero"></app-sad-hero>
  <app-confused-hero *ngSwitchCase="'app-confused'" [hero]="hero"></app-confused-hero>
  <app-unknown-hero  *ngSwitchDefault           [hero]="hero"></app-unknown-hero>
</div>
```

#### customized structural directive

Creating a directive is similar to creating a component.

- Import the `Directive` decorator (instead of the `Component` decorator).
- Import the `Input`, `TemplateRef`, and `ViewContainerRef` symbols; you'll need them for any structural directive.
- Apply the decorator to `the directive class`.
- Set `the CSS attribute selector` that identifies the directive when applied to an element in a template.

```typescript
import { Directive, Input, TemplateRef, ViewContainerRef } from '@angular/core';

@Directive({ selector: '[appUnless]'})
export class UnlessDirective {

  constructor(
  private templateRef: TemplateRef<any>,
  private viewContainer: ViewContainerRef) { }
}
```

A simple structural directive

- creates an `embedded view` from the `Angular-generated <ng-template>` and 
- inserts that view in a `view container` adjacent to `the directive's original host element`.
- acquire the `<ng-template>` contents with a `TemplateRef` and 
- access the view container through a `ViewContainerRef`.

### Build a attribute directive

- An attribute directive minimally requires building a class annotated with `@Directive`, which specifies the `selector` that identifies the attribute.
  - The class implements the desired directive behavior.

```typescript
@Directive({
  selector: '[appHighlight]'
})
export class HighlightDirective {
  constructor(el: ElementRef) {
    el.nativeElement.style.backgroundColor = 'yellow';
  }
  @HostListener('mouseenter')
  onMouseEnter() {
    this.highlight('yellow');
  }

  @HostListener('mouseleave')
  onMouseLeave() {
    this.highlight(null);
  }

  private highlight(color: string) {
    this.el.nativeElement.style.backgroundColor = color;
  }
}
```

- the brackets ([]) that make it an attribute selector.
- the `ElementRef` injects a reference to the host DOM element, the element to which the directive is applied
  - ElementRef grants direct access to the host DOM element through its `nativeElement` property.
  - The `@HostListener` decorator lets you subscribe to events of the DOM element that hosts an attribute directive

## Form

Two different approaches to handling user input through forms:

- `reactive`
- `template-driven`

Both can

- capture user input events from the view
- validate the user input
- create a `form model` and `data model` to update, and
- provide a way to track changes.
- use a `form model` to track value changes between `Angular forms` and `form input elements`.

### Comparison

- Reactive are more robust:
  - they're more scalable, reusable, and testable.
  - If forms are a key part of your application, or you're already using reactive patterns for building your application, use reactive forms.
- Template-driven forms are useful for adding a simple form to an app (e.g., an email list signup form)
  - They're easy to add to an app, but they don't scale as well as reactive forms.
  - If you have very basic form requirements and logic that can be managed solely in the template, use template-driven forms.

|                    | Reactive                                  | Template-driven                      |
| ------------------ | ----------------------------------------- | ------------------------------------ |
| Setup (form model) | More explicit, created in component class | Less explicit, created by directives |
| Data model         | Structured                                | Unstructured                         |
| Predictability     | Synchronous                               | Asynchronous                         |
| Form validation    | Functions                                 | Directives                           |
| Mutability         | Immutable                                 | Mutable                              |
| Scalability        | Low-level API access                      | Abstraction on top of APIs           |

### Reactive forms

```typescript
@Component({
  selector: 'app-reactive-favorite-color',
  template: `
    Favorite Color: <input type="text" [formControl]="favoriteColorControl">
  `
})
export class FavoriteColorComponent {
  favoriteColorControl = new FormControl('');
}
```

![Reactive Forms](images/angular-key-diff-reactive-forms.png)

### Template-driven Forms

```typescript
@Component({
  selector: 'app-template-favorite-color',
  template: `
    Favorite Color: <input type="text" [(ngModel)]="favoriteColor">
  `
})
export class FavoriteColorComponent {
  favoriteColor = '';
}
```

![Template-driven Forms](images/angular-key-diff-td-forms.png)

### Dataflow

#### Reactive forms

Updates from the view to the model and from the model to the view are synchronous and aren't dependent on the UI rendered

![Reactive forms workflow](images/angular-dataflow-reactive-forms-vtm.png)

1. The user types a value into the input element, in this case the favorite color Blue.
2. The `form input element` emits an "input" event with the latest value.
3. The `control value accessor` listening for events on the form input element immediately relays the new value to the `FormControl` instance.
4. The `FormControl` instance emits the new value through the `valueChanges observable`.
5. Any subscribers to the valueChanges observable receive the new value.

### Common foundation

- `FormControl` tracks the value and validation status of an individual form control.

- `FormGroup` tracks the same values and status for a collection of form controls.

- `FormArray` tracks the same values and status for an array of form controls.

- `ControlValueAccessor` creates a bridge between Angular FormControl instances and native DOM elements.

## Template

### Template input variable

A `template input variable` is a variable whose value you can reference within a single instance of the template.

`Syntax:` Use the `let` keyword to declare a template input variable

The variable's scope is limited to a single instance of the repeated template.

### Template Reference Variable (#var)

A `template reference variable` refers to

- a `DOM element` within a `template`
- an Angular `component` or `directive` or a `web component`.
  - [ ] What is `web component`?

`Syntax:` Use the hash symbol (#) to declare a reference variable

- use the `ref-` prefix alternative to #

The variable's scope is the entire template.

```html
<!-- The #phone declares a phone variable on an <input> element -->
<input #phone placeholder="phone number" />
<button (click)="callPhone(phone.value)">Call</button>
```

- [ ] How a reference variable gets its value
- Angular sets the reference variable's value to the element on which it was declared
- a directive can change that behavior and set the value to something else, such as itself. The `NgForm` directive does that

Template input and reference variable names have their own namespaces.

## Binding

Data binding is a mechanism for coordinating what users see (`view`), with application data values (`data source`)

- three categories distinguished by `the direction of data flow`
  1. data source to view
  2. `interpolation`: `{{expression}}`
  3. `property binding | attribute binding | class binding | style binding` :
     1. `[target] = "expression"`
     2. `bind-target = "expression"`
  4. view to data source
  5. `event binding` :
     1. `(target) = "statement"`
     2. `on-target = "statement"`
  6. view to data source to view
  7. `two-way binding`
     1. `[(target)] = "expression"`
     2. `bindon-target = "expression"`
- binding targets
  - `HTML attributes` && `DOM (Document Object Model) properties`
    1. Attributes are defined by HTML. Properties are defined by the DOM (Document Object Model)
    - A few HTML attributes have 1:1 mapping to properties, e.g., `id`
    - Some HTML attributes don't have corresponding properties, e.g., `colspan`
    - Some DOM properties don't have corresponding attributes, e.g., `textContent`
    2. Attributes initialize DOM properties and then they are done.
    - `Property values can change; attribute values can't`.
    - `The HTML attribute value specifies the initial value; the DOM value property is the current value.`
    - Special case `disabled`
      - Adding and removing `the disabled attribute` disables and enables the button; The value of the attribute is irrelevant
      - Setting `the disabled property` disables or enables the button; The value of the property matters.
  - Template binding works with `properties` and `events`, `not attributes`.
  - property binding
    - `element property` : `<img [src]="heroImageUrl">`
    - `component property` : `<app-hero-detail [hero]="currentHero"></app-hero-detail>`
    - `directive property` : `<div [ngClass]="{'special': isSpecial}"></div>`
  - event binding
    - `Element event` : `<button (click)="onSave()">Save</button>`
    - `Component event` : `<app-hero-detail (deleteRequest)="deleteHero()"></app-hero-detail>`
    - `Directive event` : `<div (myClick)="clicked=$event" clickable>click me</div>`
  - two-way binding
    - `event and property`: `<input [(ngModel)]="name">`
  - attribute binding : `<button [attr.aria-label]="help">help</button>`
  - class binding : `<div [class.special]="isSpecial">Special</div>`
  - style binding : `<button [style.color]="isSpecial ? 'red' : 'green'">`

---

## Routing

- presents a particular **component view** for a given **URL**.
- **optional parameters** along to the supporting view component that help it decide what **specific content** to present.
- **bind the router to links** on a page and it will navigate to the appropriate application view when the user performs actions
- the router **logs activity** in **the browser history journal** so the back and forward buttons work as well.

### basics

- 
- The Angular Router is not part of the Angular core. It is in its own library package, `@angular/router`
- A routed Angular application has one singleton instance of `the Router service`. 
  - the `RouterModule.forRoot(routes:Route)` method in the module `imports` to configure the router
  - Each `Route` maps **a URL path** to **a component**
    - **no leading slashes** in the path
    - **The empty path** represents the default route
    - **The \*\* path** a wildcard router which will be selected if the requested URL doesn't match any paths for routes defined earlier
    - **The order of the routes matters**
      - The router uses **a first-match wins strategy** when matching routes, so **more specific routes** should be placed above **less specific routes**.
    - `a route parameter`: **:parameter**
    - `The data property` : store **static data** associated with a route
      - The data property is accessible within each `activated route`
      - use `the resolve guard` to retrieve **dynamic data**
- `<base href>` Most routing applications should add a `<base>` element to the index.html as the first child in the `<head>` tag to tell the router how to compose navigation URLs
- `<router-outlet>` displays the corresponding component after a `RouterOutlet` in the host view's HTML when the router matches that URL to the route path.
- `<a routerLink="path"></a>` The URL could arrive directly from the browser address bar. But it comes also from `Router links`
  - The `RouterLink` directives on `anchor tags` give the router control over those elements.
  - The `RouterLinkActive` directive on each anchor tag helps visually distinguish the anchor for the currently selected "active" route.
    - The template expression to the right of the equals (=) contains **a space-delimited string of CSS classes that the Router will add when this link is active (and remove when the link is inactive)**
    - toggles css classes for active RouterLinks based on the current RouterState. This cascades down through each level of the route tree, so parent and child router links can be active at the same time.
      - To override this behavior, you can bind to the `[routerLinkActiveOptions]` input binding with the `{ exact: true }` expression.
- After the end of each successful navigation lifecycle, the router builds a tree of `ActivatedRoute` objects that make up the current state of the router.
  - You can access the current `RouterState` from anywhere in the application using the `Router` service and the `routerState` property.

### Configuration

A routed Angular application has one singleton instance of the `Router` service

- configures the router via the `RouterModule.forRoot` method, and adds the result to the AppModule's `imports` array.
- Each `Route` maps a `URL` path to a component.
  - There are `no leading slashes` in the path.
  - The router parses and builds the final URL for you, allowing you to use both relative and absolute paths when navigating between application views.
- allow `route parameter` by form of `:parameter` which is passed to `component`
- The empty path represents the default path for the application, the place to go when the path in the URL is empty
- The \*\* path in a route will be selected by a router if the requested URL doesn't match any paths for routes defined earlier in the configuration.
  - This is useful for displaying a "404 - Not Found" page or redirecting to another route.
- The order of the routes in the configuration matters and this is by design.
  - The router uses a first-match wins strategy when matching routes, so more specific routes should be placed above less specific routes.
- A `redirect route` requires a `pathMatch` property to tell the router how to match a URL to the path of a route.

---

## HTTP

### HttpClient

Two types of errors

1. The server backend might reject the request, returning an HTTP response with a status code such as 404 or 500. These are **error responses**.
2. something could go wrong on the client-side such as a network error that prevents the request from completing successfully or an exception thrown in an RxJS operator. These errors produce JavaScript **ErrorEvent** objects.

- The HttpClient captures both kinds of errors in its **HttpErrorResponse**

### HTTP Interception

Function:

- inspect and transform HTTP requests from your application to the server
- inspect and transform the server's responses on their way back to the application
- Multiple interceptors form a forward-and-backward chain of request/response handlers
- Although interceptors are capable of mutating requests and responses, the HttpRequest and HttpResponse instance properties are readonly, rendering them largely immutable.
  - clone() method's hash argument allows you to mutate specific properties of the request while copying the others.

Syntax

```typescript
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class NoopInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req);
  }
}
```

- The **intercept** method transforms a request into an Observable that eventually returns the HTTP response
- The **next** object represents the next interceptor in the chain of interceptors.
  - The final next in the chain is the HttpClient backend handler that sends the request to the server and receives the server's response.

```typescript
export abstract class HttpHandler {
  abstract handle(req: HttpRequest<any>): Observable<HttpEvent<any>>;
}
```

- the handle() method transforms an HTTP request into an Observable of HttpEvents which ultimately include the server's response.
  - The intercept() method could inspect that observable and alter it before returning it to the caller

### Injection

- Because interceptors are (optional) dependencies of the HttpClient service, you must provide them in the same injector (or a parent of the injector) that provides HttpClient.
- Interceptors provided after DI creates the HttpClient are ignored.

```typescript
{ provide: HTTP_INTERCEPTORS, useClass: NoopInterceptor, multi: true },
```

multi: true option. This required setting tells Angular that HTTP_INTERCEPTORS is a token for a multiprovider that injects an array of values, rather than a single value.

### HttpEvents

intercept() and handle() methods return observables of HttpEvent<any>

- interceptors work at a lower level than those HttpClient methods.
  - A single HTTP request can generate multiple events, including upload and download progress events.
  - The HttpResponse class itself is actually an event, whose type is HttpEventType.HttpResponseEvent.

## Q&A

Q: Local workspace file ('angular.json') could not be found
Reason:New configuration format. The new file can be found at angular.json (but .angular.json is also accepted). Running ng update on a CLI 1.7 project will move you to the new configuration.

# A:ng update @angular/cli --migrate-only --from=1.7.4

## Form

Two different approaches to handling user input through forms:

- `reactive`
- `template-driven`

Both can

- capture user input events from the view
- validate the user input
- create a `form model` and `data model` to update, and
- provide a way to track changes.

### Comparison

- Reactive are more robust:
  - they're more scalable, reusable, and testable.
  - If forms are a key part of your application, or you're already using reactive patterns for building your application, use reactive forms.
- Template-driven forms are useful for adding a simple form to an app (e.g., an email list signup form)
  - They're easy to add to an app, but they don't scale as well as reactive forms.
  - If you have very basic form requirements and logic that can be managed solely in the template, use template-driven forms.

### Common foundation

- `FormControl` tracks the value and validation status of an individual form control.

- `FormGroup` tracks the same values and status for a collection of form controls.

- `FormArray` tracks the same values and status for an array of form controls.

- `ControlValueAccessor` creates a bridge between Angular FormControl instances and native DOM elements.

## Template

### Template input variable

A `template input variable` is a variable whose value you can reference within a single instance of the template.

`Syntax:` Use the `let` keyword to declare a template input variable

The variable's scope is limited to a single instance of the repeated template.

### Template Reference Variable (#var)

A `template reference variable` refers to

- a `DOM element` within a `template`
- an Angular `component` or `directive` or a `web component`.
  - [ ] What is `web component`?

`Syntax:` Use the hash symbol (#) to declare a reference variable

- use the `ref-` prefix alternative to #

The variable's scope is the entire template.

```html
<!-- The #phone declares a phone variable on an <input> element -->
<input #phone placeholder="phone number" />
<button (click)="callPhone(phone.value)">Call</button>
```

- [ ] How a reference variable gets its value
- Angular sets the reference variable's value to the element on which it was declared
- a directive can change that behavior and set the value to something else, such as itself. The `NgForm` directive does that

Template input and reference variable names have their own namespaces.

## Binding

Data binding is a mechanism for coordinating what users see (`view`), with application data values (`data source`)

- three categories distinguished by `the direction of data flow`
  1. data source to view
  2. `interpolation`: `{{expression}}`
  3. `property binding | attribute binding | class binding | style binding` :
     1. `[target] = "expression"`
     2. `bind-target = "expression"`
  4. view to data source
  5. `event binding` :
     1. `(target) = "statement"`
     2. `on-target = "statement"`
  6. view to data source to view
  7. `two-way binding`
     1. `[(target)] = "expression"`
     2. `bindon-target = "expression"`
- binding targets
  - `HTML attributes` && `DOM (Document Object Model) properties`
    1. Attributes are defined by HTML. Properties are defined by the DOM (Document Object Model)
    - A few HTML attributes have 1:1 mapping to properties, e.g., `id`
    - Some HTML attributes don't have corresponding properties, e.g., `colspan`
    - Some DOM properties don't have corresponding attributes, e.g., `textContent`
    2. Attributes initialize DOM properties and then they are done.
    - `Property values can change; attribute values can't`.
    - `The HTML attribute value specifies the initial value; the DOM value property is the current value.`
    - Special case `disabled`
      - Adding and removing `the disabled attribute` disables and enables the button; The value of the attribute is irrelevant
      - Setting `the disabled property` disables or enables the button; The value of the property matters.
  - Template binding works with `properties` and `events`, `not attributes`.
  - property binding
    - `element property` : `<img [src]="heroImageUrl">`
    - `component property` : `<app-hero-detail [hero]="currentHero"></app-hero-detail>`
    - `directive property` : `<div [ngClass]="{'special': isSpecial}"></div>`
  - event binding
    - `Element event` : `<button (click)="onSave()">Save</button>`
    - `Component event` : `<app-hero-detail (deleteRequest)="deleteHero()"></app-hero-detail>`
    - `Directive event` : `<div (myClick)="clicked=$event" clickable>click me</div>`
  - two-way binding
    - `event and property`: `<input [(ngModel)]="name">`
  - attribute binding : `<button [attr.aria-label]="help">help</button>`
  - class binding : `<div [class.special]="isSpecial">Special</div>`
  - style binding : `<button [style.color]="isSpecial ? 'red' : 'green'">`

---

## Routing

- interpret a browser URL as an instruction to navigate to a client-generated view.
- optional parameters along to the supporting view component that help it decide what specific content to present.
- bind the router to links on a page and it will navigate to the appropriate application view when the user clicks a link.
- navigate imperatively when the user performs actions, such as clicks a button, selects from a drop box, or in response to some other stimulus from any source.
- the router logs activity in the browser's history journal so the back and forward buttons work as well.

### basics

- `<base href>` Most routing applications should add a `<base>` element to the index.html as the first child in the `<head>` tag to tell the router how to compose navigation URLs
- `<router-outlet>` Given a configuration, when the router matches that URL to the route path and displays the corresponding component after a `RouterOutlet` that you've placed in the host view's HTML.
- `<a routerLink="path"></a>` The URL could arrive directly from the browser address bar. But it comes also from `Router links`
  - The `RouterLink` directives on `anchor tags` give the router control over those elements.
  - The `RouterLinkActive` directive on each anchor tag helps visually distinguish the anchor for the currently selected "active" route.
    - The template expression to the right of the equals (=) contains a space-delimited string of CSS classes that the Router will add when this link is active (and remove when the link is inactive)
    - toggles css classes for active RouterLinks based on the current RouterState. This cascades down through each level of the route tree, so parent and child router links can be active at the same time.
      - To override this behavior, you can bind to the `[routerLinkActiveOptions]` input binding with the `{ exact: true }` expression.
- After the end of each successful navigation lifecycle, the router builds a tree of `ActivatedRoute` objects that make up the current state of the router.
  - You can access the current `RouterState` from anywhere in the application using the `Router` service and the `routerState` property.

### Configuration

A routed Angular application has one singleton instance of the `Router` service

- configures the router via the `RouterModule.forRoot` method, and adds the result to the AppModule's `imports` array.
- Each `Route` maps a `URL` path to a component.
  - There are `no leading slashes` in the path.
  - The router parses and builds the final URL for you, allowing you to use both relative and absolute paths when navigating between application views.
- allow `route parameter` by form of `:parameter` which is passed to `component`
- The empty path represents the default path for the application, the place to go when the path in the URL is empty
- The \*\* path in a route will be selected by a router if the requested URL doesn't match any paths for routes defined earlier in the configuration.
  - This is useful for displaying a "404 - Not Found" page or redirecting to another route.
- The order of the routes in the configuration matters and this is by design.
  - The router uses a first-match wins strategy when matching routes, so more specific routes should be placed above less specific routes.
- A `redirect route` requires a `pathMatch` property to tell the router how to match a URL to the path of a route.

---

## HTTP

### HttpClient

Two types of errors

1. The server backend might reject the request, returning an HTTP response with a status code such as 404 or 500. These are **error responses**.
2. something could go wrong on the client-side such as a network error that prevents the request from completing successfully or an exception thrown in an RxJS operator. These errors produce JavaScript **ErrorEvent** objects.

- The HttpClient captures both kinds of errors in its **HttpErrorResponse**

### HTTP Interception

Function:

- inspect and transform HTTP requests from your application to the server
- inspect and transform the server's responses on their way back to the application
- Multiple interceptors form a forward-and-backward chain of request/response handlers
- Although interceptors are capable of mutating requests and responses, the HttpRequest and HttpResponse instance properties are readonly, rendering them largely immutable.
  - clone() method's hash argument allows you to mutate specific properties of the request while copying the others.

Syntax

```typescript
import { Injectable } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest
} from '@angular/common/http';

import { Observable } from 'rxjs';

/** Pass untouched request through to the next request handler. */
@Injectable()
export class NoopInterceptor implements HttpInterceptor {
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {
    return next.handle(req);
  }
}
```

- The **intercept** method transforms a request into an Observable that eventually returns the HTTP response
- The **next** object represents the next interceptor in the chain of interceptors.
  - The final next in the chain is the HttpClient backend handler that sends the request to the server and receives the server's response.

```typescript
export abstract class HttpHandler {
  abstract handle(req: HttpRequest<any>): Observable<HttpEvent<any>>;
}
```

- the handle() method transforms an HTTP request into an Observable of HttpEvents which ultimately include the server's response.
  - The intercept() method could inspect that observable and alter it before returning it to the caller

### Injection

- Because interceptors are (optional) dependencies of the HttpClient service, you must provide them in the same injector (or a parent of the injector) that provides HttpClient.
- Interceptors provided after DI creates the HttpClient are ignored.

```typescript
{ provide: HTTP_INTERCEPTORS, useClass: NoopInterceptor, multi: true },
```

multi: true option. This required setting tells Angular that HTTP_INTERCEPTORS is a token for a multiprovider that injects an array of values, rather than a single value.

### HttpEvents

intercept() and handle() methods return observables of HttpEvent<any>

- interceptors work at a lower level than those HttpClient methods.

  - A single HTTP request can generate multiple events, including upload and download progress events.
  - The HttpResponse class itself is actually an event, whose type is HttpEventType.HttpResponseEvent.

    <link rel="stylesheet" type="text/css" media="all" href="../markdown.css" />
