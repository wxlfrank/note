# Jasmine

## Asynchronous Work

### What is an **asynchronous component**?

### 3 ways managing asynchronous work
1. callbacks
```javascript
beforeEach(function(done) {
  setTimeout(function() {
    // do some stuff
    done();
  }, 100);
});
```
the function passed to Jasmine took an argument (traditionally called **done**), Jasmine will pass a function to be invoked when asynchronous work has been completed.
2. Promises
```javascript
beforeEach(function() {
  return new Promise(function(resolve, reject) {

    // do some stuff

    resolve();
  });
});
```
3. **async** keywords

#### Apply to
1. **beforeEach**
2. **afterEach**
3. **beforeAll**
4. **afterAll**
5. **it**

#### Apply NOT
**describe** can’t be executed asynchronously, because your suite should be statically defined
