# Android

## Fundamentals

### Development

- Android apps can be written using `Kotlin`, `Java`, and `C++`.
- `The Android SDK tools` compile your `code` along with any `data` and `resource` files into an `APK`, an Android package 
  - an archive file with an `.apk` suffix
  - contains all the contents of an Android app
  - Android-powered devices use to install the app

### Runtime Security

Each Android app lives in its own security `sandbox`

- The Android operating system is a `multi-user Linux system` in which `each app is a different user`.
  - the system assigns each app a `unique` Linux `user ID`
    - the ID is used only by the system and is unknown to the app
  - The system sets permissions for all the files in an app so that only the user ID assigned to that app can access them.
  - Each process has its own `virtual machine` (VM)
    - every app runs in its own Linux process
    - The Android system starts the process when any of the app's components need to be executed
    - shuts down the process when it's no longer needed or when the system must recover memory for other apps.
  - The Android system implements the principle of `least privilege`
    - each app, by default, has access only to the components that it requires to do its work and no more
    - share data and access system services
      - share the same Linux user ID, in which case they are able to access each other's files
        - To conserve system resources, apps with the same user ID can also arrange to `run in the same Linux process` and `share the same VM`. 
        - The apps must also be signed with `the same certificate`.

## Concepts

- App `components` are the essential building blocks of an Android app
  - each component is `an entry point` through which the system or a user can enter your app
  - Each screen in your app that is not `the main entry point` (all screens that are not the "home" screen) should provide `navigation` so the user can return to the logical parent screen in the app hierarchy by tapping `the Up button` in the app bar.
  - Activities
    - interacting with the user
    - represents `a single screen` with `a user interface`
    - activities work together to form a cohesive user experience, each one is independent of the others.
  - Services
    - runs in the background to perform long-running operations or to perform work for remote processes.
    - A service does not provide a user interface
    - two kinds
      - `Started services` tell the system to keep them running until their work is completed. 
      - `Bound services` run because some other app (or the system) has said that it wants to make use of the service. 
        - This is basically the service providing an API to another process.
  - Broadcast receivers
    - enables the system to deliver `events` to the app outside of a regular user flow, allowing the app to respond to `system-wide broadcast announcements`
    - broadcast receivers don't display a user interface, they may create `a status bar notification` to alert the user when a broadcast event occurs.
  - Content providers

## UI

The user interface for an Android app is built using a hierarchy of

- `layouts` (`ViewGroup` objects)
  - containers that control how their child views are positioned on the screen
  - `ConstraintLayout` is a layout that defines the position for each view based on `constraints to sibling views and the parent layout`.
- `widgets` (`View` objects)
  - UI components such as buttons and text boxes

- most of your UI is defined in XML files

An `Intent` is an object that provides `runtime binding` between separate components, such as two activities

- The `Intent` constructor takes two parameters:
  - A `Context` (this is used because the `Activity` class is a subclass of `Context`)
  - The `Class` of the app component to which the system should deliver the `Intent` (in this case, the activity that should be started).
- The `putExtra()` method adds the EditText's value to the intent. 
  - An `Intent` can carry data types as `key-value pairs` called `extras`.
- The `startActivity()` method starts an instance of the start activity specified by the `Intent`
