# C

## Keywords

### internal

- Internal types or members are accessible only within files in the same **assembly**
- A `protected internal` member is accessible from the current assembly or from types that are derived from the containing class.

## suger syntax

### using statement

The using statement

- calls the `Dispose` method on the object in the correct way
- causes the object itself to go out of scope as soon as `Dispose` is called.
- Within the using block, the object is `read-only` and can't be modified or reassigned.
- If the object implements `IAsyncDisposable`, the using statement calls the `DisposeAsync` and awaits the returned `Task`

### Null-conditional operators ?. and ?[]

- If a evaluates to null, the result of `a?.x` or `a?[x]` is null.
- If a evaluates to non-null, the result of `a?.x` or `a?[x]` is the same as the result of `a.x` or `a[x]`, respectively
