# Media RSS

> - RDF Site Summary (Resource Description Framework) (RSS 0.91, RSS 1.0)
> - Rich Site Summary (RSS 0.9, RSS 1.0)
> - Really Simple Syndication (RSS 2.0)

## Namespace

```xml
<rss version="2.0" xmlns:media="http://search.yahoo.com/mrss/">
```

## Concepts

### group

#### Description

- grouping of [content](#content) elements that are effectively the same content, yet different representations

#### Structure

```css
item > group*
```

### content

#### Description

- The sequence of these items implies the order of presentation
- Media objects that are not the same [content](#content) should not be included in the same [group](#group) element

#### Property

- url?
  - if not included, [player](#player) must be given
- fileSize?
- type?: MIME type
- medium?: image | audio | video | document | executable
  - simplifies decision making on the reader side,
  - flushes out any ambiguities between MIME type and object type
- isDefault?
  - the default object that should be used for the [group](#group)
  - There should only be one default object per [group](#group)
- expression?: sample | full (default) | nonstop
  - if the object is a sample or the full version of the object, or even if it is a continuous stream
- bitrate?
  - the kilobits per second rate of media
- framerate?
  - the number of frames per second for the media object
  - FPS
- samplingrate?
  - the number of samples per second taken to create the media object
  - thousands of samples per second (kHz)
- channels?
  - number of audio channels in the media object
- duration?
  - the number of seconds the media object plays
- height?
- width?
- lang?

#### Structure

```css
item > content*,
group > content*
```

### thumbnails

#### Description

- Allows particular images to be used as representative images for the media object.
- If multiple thumbnails are included, and time coding is not at play, it is assumed that the images are in order of importance.
  - time coding : A timecode (alternatively, time code) is a sequence of numeric codes generated at regular intervals by a timing synchronization system

#### Property

- url
- height
- width
- time: Time code
  - Normal Play Time (NPT)
    - H:M:S.h (npt-hhmmss) or S.h (npt-sec), where H=hours, M=minutes, S=second and h=fractions of a second
  - SMPTE (Society of Motion Picture and Television Engineers)
    - hour:minute:second:frame

### player

#### Description

- Allows the media object to be accessed through a web browser media player console.
- This element is required only if a direct media url attribute is not specified in the [content](#content) element.

#### Property

- url
  - the URL of the player console that plays the media
- height
- width

### text

#### Description

- inclusion of a text transcript, closed captioning or lyrics of the media content

#### Property

- type: plain | html
- lang
- start
- end

### embed

#### Description

- include player-specific embed code
- key-value pairs

### status

#### Description

- specify the status of a media object

#### Property

- state: active | blocked | deleted
- reason: string | URL

## Proxy

- Browse material is derived media resources attached to an asset entry in order to facilitate browsing and previewing the media content described by the asset entry. In the broadcast industry such browse material is often referred to as "proxy".
