# Viz Artist

## Overview

### Components and Interaction

- `Viz Engine`
  - automatically start
  - render the graphics
- `Graphic Hub`
  - configurable: UI has a `Config` tab to open configuration editor
  - database solution
  - operation modes
    - Localhost
    - 5/4 Free: five simultaneous connections
    - Multiuser (dongle required)
    - Main Server (License Required)
      - provides a mirror service to a running Replication server
    - Replication server (License Required)
      - in case that main server is unavailable
      - every transaction on the primary server is immediately mirrored on a Replication Server.
- `Vizrt Licensing Service`
  - optional
  - handles the communication between our software protection server and your local computer
  - The `online License Activation` is a part of installing the free edition of Viz Artist.

### UI

- `Server Panel` shows an overview of all available items in the connected Graphic Hub database
  - `file tabs` to filter the type of files currently visible in the selected folder
- `Scene Tree` is the logical visualization of all the properties in a `Scene`
  - The Scene Tree consists of `Containers` that hold properties. The properties can be
    - `Geometries`
    - `Images`
    - `Materials`
    - `Texts`
    - `Transparency functions`
    - `Key functions`
  - Scenes are root items that hold other items.
- `Scene Editor` shows graphics
  - change between cameras
  - view your graphics in wireframe
  - enable safe and title frame
  - preview the key
  - control your animation with the timeline buttons
- `Plugin` (Builtin)
  - add geometry to the scene
  - add additional functionality to containers
  - add different kinds of shaders
  - add media assets
  - types:
    - Geometry Plugins
    - Container Plugins
    - Shaders
    - Scene Plugins
    - Media Assets

### Data

- Input
  - non-builtin (available items are shown in `Server Panel`)
    - `geometric`
    - `Material`
    - `Material Advanced`
    - `Material definition`
    - `Image`
    - `Font`
    - `Audio`
    - `Video`
    - `Substance`

- `Archive`
  - collected assets exported from Viz Artist

### Workflow

1. Viz Artist Login Graphic Hub
2. Server Tree